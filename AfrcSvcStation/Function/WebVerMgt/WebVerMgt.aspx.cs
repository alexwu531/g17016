﻿using AfrcSvcStation.App_Code;
using AfrcSvcStation.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.FunctionOld.SiteMgt
{
    public partial class WebVerMgt : System.Web.UI.Page
    {
        WebMessage Msg = new WebMessage();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }


        /// <summary>
        /// 參數清單綁定
        /// </summary>
        public void BindData()

        {
            try
            {
                using (AFRCEntities db = new AFRCEntities())
                {

                    var rr = from w in db.T_Web
                             //join t in db.T_Dept on w.Dept equals t.sn.ToString()
                             where (!string.IsNullOrEmpty(txtWebName.Text) ? w.WebName.Contains(txtWebName.Text) : true) && (ddlStatus.SelectedValue.ToString() != "0" ? w.Status.ToString() == ddlStatus.SelectedValue : true)


                             select new
                             {
                                 wd = w.WebID,
                                 de = w.Dept,
                                 we = w.WebName,
                                 //ca = t.caption,
                                 ty = w.Type,
                                 st = w.Status,
                                 url = w.WebUrl
                             };


                    AspNetPager1.AlwaysShow = true;
                    AspNetPager1.RecordCount = rr.Count();
                    gvUnitList.DataSource = rr.OrderBy(o => o.we).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize).Take(AspNetPager1.PageSize).ToList();
                    gvUnitList.DataBind();
                }
            }
            catch (Exception ex)
            {

            }

        }


        /// <summary>
        /// 分頁事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            gvUnitList.ShowFooter = false;
            BindData();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //新增參數
            if (e.CommandName == "Add")
            {
                UserInfo so = (UserInfo)Session["UserData"];
                TextBox txtWebName_s = gvUnitList.FooterRow.FindControl("txtWebName_s") as TextBox;
                TextBox txtWeburl_s = gvUnitList.FooterRow.FindControl("txtWeburl_s") as TextBox;
                TextBox txtcaption = gvUnitList.FooterRow.FindControl("txtType") as TextBox;
                TextBox txtcaption_s = gvUnitList.FooterRow.FindControl("txtStatus") as TextBox;
                DropDownList ddlDept = gvUnitList.FooterRow.FindControl("ddlDept") as DropDownList;

                AFRCEntities db = new AFRCEntities();

                try
                {
                    int intDept = Convert.ToInt32(ddlDept.SelectedValue.ToString());

                    T_Web Dept = new T_Web

                    {
                        WebName = txtWebName_s.Text,
                        WebUrl = txtWeburl_s.Text,
                        Type = int.Parse(txtcaption.Text),
                        Status = int.Parse(txtcaption_s.Text),
                        Dept = intDept.ToString()


                    };
                    db.T_Web.Add(Dept);

                    db.SaveChanges();


                    gvUnitList.ShowFooter = false;
                    BindData();
                    Msg.MsgBox_In_Ajax("新增完成", this.Page);

                }
                catch (Exception ex)
                {
                    Msg.MsgBox_In_Ajax("新增失敗", this.Page);

                }
                finally
                {
                    db.Dispose();
                }
            }
            //取消新增
            else if (e.CommandName == "CancelAdd")
            {
                gvUnitList.ShowFooter = false;
                BindData();
            }
        }


        #region 清單各類事件
        /// <summary>
        /// 開啟編輯
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvUnitList.EditIndex = e.NewEditIndex;
            BindData();
            ScriptManager.RegisterStartupScript(this, GetType(), "spinner", @"spinner();", true);
        }
        /// <summary>
        /// 清單儲存事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            UserInfo so = (UserInfo)Session["UserData"];
            int wd = int.Parse(((HiddenField)gvUnitList.Rows[e.RowIndex].FindControl("hidde")).Value);
            String we = (((TextBox)gvUnitList.Rows[e.RowIndex].FindControl("txtWebName_s")).Text.ToString());
            String url = (((TextBox)gvUnitList.Rows[e.RowIndex].FindControl("txtWeburl_s")).Text.ToString());
            int ty = int.Parse(((TextBox)gvUnitList.Rows[e.RowIndex].FindControl("txtType")).Text.ToString());
            int st = int.Parse(((TextBox)gvUnitList.Rows[e.RowIndex].FindControl("txtStatus")).Text.ToString());
            ListBox lstDept = ((ListBox)gvUnitList.Rows[e.RowIndex].FindControl("lstbDept"));
            string de = "";
            foreach (ListItem li in lstDept.Items)
            {
                if (li.Selected == true)
                {
                    de += li.Value + ",";
                }
            }

            AFRCEntities db = new AFRCEntities();
            try
            {
                // T_Web x = db.T_Web.Where(u =>  (u.WebName.ToString() == we.ToString() ^ u.Dept == de)).FirstOrDefault();
                T_Web x = db.T_Web.Where(u => u.WebID == wd).FirstOrDefault();
                //T_Web x = db.T_Web.Where(u => u.WebName.ToString() == we.ToString()).FirstOrDefault();
                x.WebName = we;
                x.WebUrl = url;
                x.Type = ty;
                x.Status = st;
                x.Dept = de.Length > 0 ? de.Substring(0, de.Length - 1) : "2";

                db.SaveChanges();
                gvUnitList.EditIndex = -1;
                BindData();
                Msg.MsgBox_In_Ajax("更新完成", this.Page);
            }
            catch (Exception ex)
            {
                Msg.MsgBox_In_Ajax("更新失敗", this.Page);
            }
            finally
            {
                db.Dispose();
            }
        }
        /// <summary>
        /// 清單刪除事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            UserInfo so = (UserInfo)Session["UserData"];

            int wd = int.Parse(((HiddenField)gvUnitList.Rows[e.RowIndex].FindControl("hidde")).Value);


            AFRCEntities db = new AFRCEntities();
            try
            {
                T_Web Deptx = db.T_Web.Where(u => u.WebID == wd).FirstOrDefault();
                db.T_Web.Remove(Deptx);
                db.SaveChanges();

                gvUnitList.EditIndex = -1;
                BindData();
                Msg.MsgBox_In_Ajax("刪除完成", this.Page);
            }
            catch (Exception ex)
            {
                Msg.MsgBox_In_Ajax("刪除失敗", this.Page);
            }
            finally
            {
                db.Dispose();
            }
        }
        /// <summary>
        /// 清單取消編輯
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvUnitList.EditIndex = -1;
            BindData();
        }
        #endregion

        protected void cmdadd_Click(object sender, EventArgs e)
        {
            try
            {
                gvUnitList.ShowFooter = true;
                BindData();


                AFRCEntities db = new AFRCEntities();
                var captions = from cap in db.T_Dept
                               where cap.belong_sn != 0 && cap.del == 1
                               select cap;

                ListBox lstbDept = gvUnitList.FooterRow.FindControl("lstbDept") as ListBox;
                lstbDept.DataSource = captions.ToList();
                lstbDept.DataTextField = "caption";
                lstbDept.DataValueField = "sn";
                lstbDept.DataBind();
                db.Dispose();
            }
            catch (Exception ex)
            {

            }

        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void gvUnitList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                    {

                        AFRCEntities db = new AFRCEntities();


                        var captions = from cap in db.T_Dept
                                       where cap.belong_sn != 0
                                       select cap;

                        ListBox lstbDept = e.Row.FindControl("lstbDept") as ListBox;
                        lstbDept.DataSource = captions.ToList();
                        lstbDept.DataTextField = "caption";
                        lstbDept.DataValueField = "sn";
                        lstbDept.DataBind();



                        object hidde = DataBinder.Eval(e.Row.DataItem, "de");

                        string[] arr = hidde.ToString().Split(',');



                        foreach (ListItem li in lstbDept.Items)
                        {
                            foreach (string s in arr)
                            {
                                if (s == li.Value)
                                {
                                    li.Selected = true;
                                }
                            }
                        }

                        db.Dispose();

                    }

                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    AFRCEntities db = new AFRCEntities();
                    var depts = db.T_Dept.ToList();
                    Label lblDept = e.Row.FindControl("lblDept") as Label;
                    object hidde = DataBinder.Eval(e.Row.DataItem, "de");

                    string[] arr = hidde.ToString().Split(',');

                    string _Depts = "";
                    foreach (string de in arr)
                    {
                        foreach (var s in depts)
                        {
                            if (s.sn.ToString() == de)
                            {
                                _Depts += "<br>" + s.caption;
                            }
                        }
                    }

                    lblDept.Text = _Depts;

                    db.Dispose();

                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}