﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebVerMgt.aspx.cs" Inherits="AfrcSvcStation.FunctionOld.SiteMgt.WebVerMgt" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="../css/jquery-ui.custom.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
    <!-- PAGE CONTENT BEGINS -->
    <div class="col-xs-12">
        <div class="page-header">
            <h1>網站版本維護
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    新增編輯網站版本
                                </small>
            </h1>
        </div>

        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <!-- search-area  BEGINS -->
            <div class="widget-box">
                <div class="widget-header widget-header-small">
                    <h5 class="widget-title lighter">查詢條件</h5>
                </div>

                <div class="widget-body">
                    <div class="widget-main">

                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-lg-6 ">
                                <div class="input-group">
                                    <span class="input-group-addon">網站名稱
                                    </span>
                                    <asp:TextBox ID="txtWebName" runat="server" CssClass="form-control search-query" placeholder="網站名稱"></asp:TextBox>
                                </div>
                                <div class="hr"></div>
                                <div class="input-group">
                                    <span class="input-group-addon">啟用狀態
                                    </span>
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                                        <asp:ListItem Value="1">使用中</asp:ListItem>
                                        <asp:ListItem Value="2">停用</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="hr"></div>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <asp:Button ID="cmdSearch" runat="server" Text="搜尋" CssClass="btn btn-purple btn-sm" OnClick="cmdSearch_Click" />

                                    </span>
                                </div>

                                <div class="hr"></div>
                                <div class="input-group">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            <div class="hr hr-18 dotted hr-double"></div>
            <div class="dataTables_wrapper form-inline no-footer">
                <asp:UpdatePanel ID="Upp1" runat="server">
                    <ContentTemplate>
                        <div class="row">

                            <div class="pull-right">
                                <asp:Button ID="cmdadd" runat="server" Text="新增" CssClass="btn btn-info btn-sm" OnClick="cmdadd_Click" />
                            </div>
                        </div>
                        <!--列表-->

                        <asp:GridView ID="gvUnitList" runat="server" AutoGenerateColumns="False" CssClass="table  table-bordered table-hover"
                            OnRowEditing="gvUnitList_RowEditing" OnRowUpdating="gvUnitList_RowUpdating" OnRowDeleting="gvUnitList_RowDeleting" OnRowCancelingEdit="gvUnitList_RowCancelingEdit"
                            OnRowCommand="gvUnitList_RowCommand" OnRowDataBound="gvUnitList_RowDataBound">

                            <Columns>

                                <asp:TemplateField HeaderText="網站名稱" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hidde" runat="server" Value='<%#Eval("wd")%>' />
                                        <asp:Label ID="lblWebName" runat="server" Text='<%#Eval("we")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:HiddenField ID="hidde" runat="server" Value='<%#Eval("wd")%>' />
                                        <asp:TextBox ID="txtWebName_s" Width="95%" runat="server" Text='<%#Eval("we")%>'></asp:TextBox>
                                    </EditItemTemplate>

                                    <FooterTemplate>
                                        <asp:TextBox ID="txtWebName_s" Width="95%" runat="server" Text=""></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="前台網址預設" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                    <ItemTemplate>
                                   
                                        <asp:Label ID="lblWeburl" runat="server" Text='<%#Eval("url")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                       
                                        <asp:TextBox ID="txtWeburl_s" Width="95%" runat="server" Text='<%#Eval("url")%>'></asp:TextBox>
                                    </EditItemTemplate>

                                    <FooterTemplate>
                                        <asp:TextBox ID="txtWeburl_s" Width="95%" runat="server" Text=""></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="維護單位" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDept" runat="server" Text=''></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                      <%--  <asp:DropDownList ID="ddlDept" Width="95%" runat="server" >
                                        </asp:DropDownList>--%>
                                        <asp:ListBox ID="lstbDept" runat="server"  Width="95%" SelectionMode="Multiple"></asp:ListBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                      <%--  <asp:DropDownList ID="ddlDept" Width="95%" runat="server">
                                        </asp:DropDownList>--%>
                                          <asp:ListBox ID="lstbDept" runat="server"  Width="95%"></asp:ListBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="是否為主網" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                    <ItemTemplate>

                                        <asp:Label ID="lblType" runat="server" Text='<%#Eval("ty")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtType" Width="95%" runat="server" Text='<%#Eval("ty")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtType" Width="95%" runat="server" Text=""></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="啟用狀態" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                    <ItemTemplate>

                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("st")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtStatus" Width="95%" runat="server" Text='<%#Eval("st")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtStatus" Width="95%" runat="server" Text=""></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="編輯" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:Button ID="Button2" runat="server" Text="編輯" CssClass="btn btn-xs btn-info" CausesValidation="False" CommandName="Edit" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Button ID="cmdCancel" runat="server" CssClass="btn btn-xs" Text="取消" CausesValidation="False" CommandName="Cancel" />
                                        <asp:Button ID="cmdSave" runat="server" Text="儲存" CssClass="btn-success btn btn-xs" CausesValidation="True" CommandName="Update" CommandArgument="edit" />
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button ID="cmdCancel" runat="server" CssClass="btn btn-xs" Text="取消" CausesValidation="False" CommandName="CancelAdd" />
                                        <asp:Button ID="cmdSave" runat="server" Text="儲存" CssClass="btn-success btn btn-xs" CausesValidation="True" CommandName="Add" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="刪除" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Button ID="Button1" runat="server" Text="刪除" CausesValidation="False" CommandName="Delete" CssClass="btn btn-xs btn-danger" OnClientClick="return confirm('確認刪除?')" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>

                        </asp:GridView>

                        <div class="row">

                            <div class="col-xs-12">
                                <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                                        ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                                    </webdiyer:AspNetPager>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!--列表-->

                <%--      <table id="simple-table" class="table  table-bordered table-hover">
                    <thead>
                        <tr>

                            <th>群組名稱</th>
                            <th>狀態</th>
                            <th>編輯</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                               <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                                        ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                                    </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
    <script src="../js/spinbox.min.js"></script>
    <script type="text/javascript">

        function spinner() {
            $('.txtspinner').ace_spinner({ value: 1, min: 1, max: 100, step: 1, touch_spinner: true, icon_up: 'ace-icon fa fa-caret-up bigger-110', icon_down: 'ace-icon fa fa-caret-down bigger-110' })
           .closest('.ace-spinner')
           .on('changed.fu.spinbox', function () {
               //console.log($('#spinner1').val())
           });




        }
    </script>
</asp:Content>
