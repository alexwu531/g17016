﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;
using NLog;
using System.Text.RegularExpressions;

namespace AfrcSvcStation.Function.SodierRecruit
{
    public partial class RecruitCaseDetail : System.Web.UI.Page
    {
        WebMessage MsgBox = new WebMessage();
        protected UserInfo so = new UserInfo();
        protected string StrID = HttpContext.Current.Request.QueryString["StrID"];
        private static Logger logger = LogManager.GetCurrentClassLogger();
        /*  0.初始資料綁定: 未除理時預設為?，依登入帳號綁定受理單位
         *  
         *  權限：
         *  admin 可分配所有單位的受理人
         *  同單位互相分配同單位受理人 
         *  上級是否跨單位分配
         *  
         *  受理人初始為null
         *  預設則帶入0人
         *  儲存:
         *  受理人!=0儲存後則改為3 處理中
         *  
         *  勾選已結案後儲存則改為4 已結案
         *  
         *  
         *  
      */
        protected void Page_Load(object sender, EventArgs e)
        {
            //UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            if (!IsPostBack)
            {
                try
                {
                    so = (UserInfo)Session["UserData"];
                    //VisitorLog.AddAction(so.UserID, VisitorLog.ActionType.pageview.ToString(), "系統管理>使用者管理");
                }
                catch (Exception ex)
                {

                }

                BinUserGroup();//下拉選單綁定_使用者群組
                BindUserDept();//下拉選單綁定_使用者部門

                if (!string.IsNullOrEmpty(StrID))
                {//編輯
                    BindData();
                }
            }
        }

        /// <summary>
        /// 使用者資料綁定
        /// </summary>
        public void BindData()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    int StrID = Convert.ToInt32(this.StrID);

                    T_SysUser user = db.T_SysUser.FirstOrDefault(u => u.ID == StrID);

                    if (user != null)
                    {
                        //hidID.Value = user.SysUserID.ToString();
                        txtUserID.Text = user.UserID;
                        txtUserID.Enabled = false;
                        txtUserName.Text = user.UserName;
                        ddlUserGroup.SelectedValue = user.RoleID.ToString();
                        txtPSW.Text = user.UserPassword;
                        txtCHKPSW.Text = user.UserPassword;

                        ddlUserDept.SelectedValue = user.DeptID.ToString();
                        ddlUseState.SelectedValue = user.UseState.ToString();
                        txtEmail.Text = user.UserEmail;
                        txtPhone.Text = user.UserPhone;
                    }

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(user.ToString());
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }
        }

        /// <summary>
        /// 下拉選單單位資料綁定_使用者群組
        /// </summary>
        public void BinUserGroup()
        {
            using (AFRCDutyEntities db = new AFRCDutyEntities())
            {
                try
                {
                    var Group = (from G in db.T_SysGroup
                                 orderby G.Sort
                                 where G.Del == 1
                                 select G).ToList();

                    ddlUserGroup.DataSource = Group;
                    ddlUserGroup.DataTextField = "GroupName";
                    ddlUserGroup.DataValueField = "GroupID";
                    ddlUserGroup.DataBind();

                    
                    ddlUserGroup.Items.Insert(0, new ListItem("請選擇群組", "0"));

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(Group.ToString());
                    #endregion
                }
                catch (Exception ex)
                {
                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Error(ex);
                    #endregion
                }
            }
        }

        /// <summary>
        /// 下拉選單單位資料綁定_使用者部門
        /// </summary>
        public void BindUserDept()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    var Dept = (from D in db.T_SysDept
                                orderby D.sort
                                where D.del == 1 && D.belong_sn != 0
                                select D).ToList();

                    ddlUserDept.DataSource = Dept;
                    ddlUserDept.DataTextField = "caption";
                    ddlUserDept.DataValueField = "sn";
                    ddlUserDept.DataBind();

                    ddlUserDept.Items.Insert(0, new ListItem("請選擇部門", "0"));

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(Dept.ToString());
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }
        }

        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            so = (UserInfo)Session["UserData"];

            string errorMsg = "";
            if (txtUserID.Text == "")
            {
                errorMsg += "使用者帳號必填\\n";
            }
            if (txtUserName.Text == "")
            {
                errorMsg += "使用者名稱必填\\n";
            }
            if (ddlUserGroup.SelectedValue == "0")
            {
                errorMsg += "請選擇使用者群組\\n";
            }
            if (string.IsNullOrEmpty(StrID) && txtPSW.Text == "")
            {//新增才要判斷，編輯不用
                errorMsg += "密碼欄位必填\\n";
            }
            if (!string.IsNullOrEmpty(txtPSW.Text) && string.IsNullOrEmpty(txtCHKPSW.Text))
            {
                errorMsg += "確認密碼欄位必填\\n";
            }
            if (ddlUserDept.SelectedValue == "0")
            {
                errorMsg += "請選擇使用者部門\\n";
            }
            if (ddlUseState.SelectedValue == "0")
            {
                errorMsg += "請選擇帳號使用狀態\\n";
            }
            if (!string.IsNullOrEmpty(txtPSW.Text))
            {
                Regex regex = new Regex(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W).{8,16}$");
                if (!regex.IsMatch(txtPSW.Text))
                {
                    errorMsg += "密碼必須英數混合包含一個不是英數的符號，英文要有大小寫，長度8~16位\\n";
                }
            }
            if (!string.IsNullOrEmpty(txtCHKPSW.Text))
            {
                Regex regex = new Regex(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W).{8,16}$");
                if (!regex.IsMatch(txtCHKPSW.Text))
                {
                    errorMsg += "密碼必須英數混合包含一個不是英數的符號，英文要有大小寫，長度8~16位\\n";
                }
            }

            if (!string.IsNullOrEmpty(errorMsg))
            {
                MsgBox.MsgBox_In_Ajax(errorMsg, this.Page, Request.RawUrl);
                return;
            }

            //驗證密碼
            if (string.IsNullOrEmpty(DoubleCheckPWD()) && string.IsNullOrEmpty(ChkExistsUID()))
            {
                if (!string.IsNullOrEmpty(StrID))
                {
                    #region 編輯
                    try
                    {
                        int StrID = Convert.ToInt32(this.StrID);
                        AFRCDutyEntities db = new AFRCDutyEntities();
                        T_SysUser usr = db.T_SysUser.FirstOrDefault(u => u.ID == StrID);

                        if (usr != null)
                        {
                            usr.UserID = txtUserID.Text;//帳號
                            usr.UserName = txtUserName.Text;//使用者名稱
                            usr.RoleID = Convert.ToInt32(ddlUserGroup.SelectedValue.ToString());//使用者群組權限
                            if (!string.IsNullOrEmpty(txtPSW.Text) && !string.IsNullOrEmpty(txtCHKPSW.Text))
                            {
                                usr.UserPassword = txtPSW.Text;//密碼
                                usr.UserPasswordModifyTime = DateTime.UtcNow.AddHours(8);//密碼更改日期
                            }                            
                            usr.DeptID = Convert.ToInt32(ddlUserDept.SelectedValue.ToString());//部門
                            usr.UseState = ddlUseState.SelectedValue.ToString();//帳戶使用狀態
                            usr.UserEmail = txtEmail.Text.Trim();//信箱
                            usr.UserPhone = txtPhone.Text.Trim();//分機
                            usr.ModifyTime = DateTime.UtcNow.AddHours(8);
                            usr.ModifyUser = so.UserID;
                            db.SaveChanges();

                            #region ActionLog
                            string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                            //取得IP
                            if (Request.ServerVariables["HTTP_VIA"] != null)
                            {
                                tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                            }
                            GlobalDiagnosticsContext.Set("addr", tClientIP);
                            GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                            GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                            logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_SysUser", usr.ID));
                            #endregion

                            db.Dispose();
                            MsgBox.MsgBox_In_Ajax("編輯成功", this.Page, "AccountMain.aspx");
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                        logger.Error(ex);
                        #endregion

                        MsgBox.MsgBox_In_Ajax("編輯失敗", this.Page, Request.RawUrl);
                    }
                    #endregion
                }
                else
                {
                    #region 新增
                    try
                    {
                        using (AFRCDutyEntities db = new AFRCDutyEntities())
                        {
                            T_SysUser usr = new T_SysUser();
                            usr.UserID = txtUserID.Text;//帳號
                            usr.UserName = txtUserName.Text;//使用者名稱
                            usr.RoleID = Convert.ToInt32(ddlUserGroup.SelectedValue.ToString());//使用者群組權限
                            usr.UserPassword = txtPSW.Text;//密碼
                            usr.DeptID = Convert.ToInt32(ddlUserDept.SelectedValue.ToString());//部門
                            usr.UseState = ddlUseState.SelectedValue.ToString();//帳戶使用狀態
                            usr.UserEmail = txtEmail.Text.Trim();//信箱
                            usr.UserPhone = txtPhone.Text.Trim();//分機
                            usr.ModifyTime = DateTime.UtcNow.AddHours(8);
                            usr.ModifyUser = so.UserID;
                            usr.UserPasswordModifyTime = DateTime.UtcNow.AddHours(8);

                            db.T_SysUser.Add(usr);
                            db.SaveChanges();

                            #region ActionLog
                            string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                            //取得IP
                            if (Request.ServerVariables["HTTP_VIA"] != null)
                            {
                                tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                            }
                            GlobalDiagnosticsContext.Set("addr", tClientIP);
                            GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                            GlobalDiagnosticsContext.Set("action", Actios.Insert.ToString());
                            logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_SysUser", usr.ID));
                            #endregion

                            MsgBox.MsgBox_In_Ajax("新增成功", this.Page, "AccountMain.aspx");
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Insert.ToString());
                        logger.Error(ex);
                        #endregion

                        //Logger.ErrorException("新增使用者", ex);
                        MsgBox.MsgBox_In_Ajax("新增失敗", this.Page, Request.RawUrl);
                    }
                    #endregion
                }
            }
            else
            {
                MsgBox.MsgBox_In_Ajax(DoubleCheckPWD() + ChkExistsUID(), this.Page, Request.RawUrl);
            }
        }

        /// <summary>
        /// 驗證密碼兩次是否依樣
        /// </summary>
        string DoubleCheckPWD()
        {
            string retuValue = string.Empty;

            if (txtCHKPSW.Text != "")
            {
                if (txtPSW.Text != txtCHKPSW.Text)
                {
                    retuValue = "請確認密碼是否相同\\n";
                }
            }

            return retuValue;
        }

        /// <summary>
        /// 驗證資料庫是否已存在帳號
        /// </summary>
        /// <returns></returns>
        string ChkExistsUID()
        {
            string retuValue = string.Empty;
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    T_SysUser usr = db.T_SysUser.FirstOrDefault(u => u.UserID == txtUserID.Text.Trim());
                    if (usr != null && string.IsNullOrEmpty(StrID))
                    {
                        retuValue = "使用者帳號重複\\n";
                    }
                }                
            }
            catch (Exception ex)
            {

            }

            return retuValue;
        }

        protected void Unnamed_ServerClick1(object sender, EventArgs e)
        {
            Response.Redirect("AccountMain");
        }
    }
}