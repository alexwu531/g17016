﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;
using NLog;

namespace AfrcSvcStation.Function.SodierRecruit
{
    public partial class RecruitCaseMain : System.Web.UI.Page
    {
        WebMessage Msgbox = new WebMessage();
        protected string UserID = HttpContext.Current.Request.QueryString["UserID"];
        protected UserInfo so = new UserInfo();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            so = (UserInfo)Session["UserData"];
            if (!IsPostBack)
            {
                DataForBind();
            }
        }


        /*  0.初始資料綁定: 未除理時預設為?，依登入帳號綁定受理單位
         *  
         *  1.查詢:
         *  2.新增:無須新增
         *  3.修改: 修改前須跳出登入驗證視窗
         *  4.刪除:是否可刪除待商榷
         *  
         *  
         *  規則:
         *  受理時需做登入驗正，驗證後改變狀態為2 受理中
         *  
         *  CaseStatus==1(未受理)
         *  CaseStatus==2(受理中)
         *  CaseStatus==3(處理中)
         *  CaseStatus==4(已結案)
         *  
      */
        private void DataForBind()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    
                    var RecruitCaseList =
                         db.T_RecruitCase.Where(
                             c => txtUserID.Text != "" ? c.CaseNumber.Contains(txtUserID.Text) : true &&
                                  txtUserName.Text != "" ? c.CaseName.Contains(txtUserName.Text) : true &&
                                  //ddlUseState.SelectedValue.ToString() != "0" ? c.UseState == ddlUseState.SelectedValue.ToString() : true &&
                                  c.CaseStatus != 3
                                  ).ToList();
                    var RecruitCaseList_Query =
                    db.T_RecruitCase.Where(
                             c => txtUserID.Text != "" ? c.CaseNumber.Contains(txtUserID.Text) : true &&
                                  txtUserName.Text != "" ? c.CaseName.Contains(txtUserName.Text) : true &&
                                  //ddlUseState.SelectedValue.ToString() != "0" ? c.UseState == ddlUseState.SelectedValue.ToString() : true &&
                                  c.CaseStatus != 3
                              ).ToString();
                    AspNetPager1.AlwaysShow = true;
                    AspNetPager1.RecordCount = RecruitCaseList.Count();

                    repRecruitCaseView.DataSource = RecruitCaseList.OrderBy(o => o.RecruitCaseID).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize)
                                             .Take(AspNetPager1.PageSize);
                    repRecruitCaseView.DataBind();


                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(RecruitCaseList_Query);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }
        }

        protected void repRecruitCaseView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField HF_RoleID = (HiddenField)e.Item.FindControl("HiddenField_RoleID");
                HiddenField HF_UseState = (HiddenField)e.Item.FindControl("HiddenField_UseState");
                Label lblRoleID = (Label)e.Item.FindControl("lblRoleID");
                Label lblUseState = (Label)e.Item.FindControl("lblUseState");

                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    try
                    {
                        so = (UserInfo)Session["UserData"];

                        var Group = (from G in db.T_SysGroup
                                     where G.GroupID.ToString() == HF_RoleID.Value
                                     select G).SingleOrDefault();

                        lblRoleID.Text = Group.GroupName;

                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Info(Group.ToString());
                        db.Dispose();
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        lblRoleID.Text = "查無對應權限";

                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Info(ex);
                        #endregion
                    }

                    lblUseState.Text = HF_UseState.Value == "1" ? "是" : "否";
                }
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataForBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataForBind();
        }

        /// <summary>
        /// 刪除使用者
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                try
                {
                    AFRCDutyEntities db = new AFRCDutyEntities();

                    T_SysUser User = db.T_SysUser.FirstOrDefault(c => c.ID.ToString() == e.CommandArgument.ToString());
                    if (User != null)
                    {
                        User.UseState = "2";
                        User.ModifyUser = so.UserID;
                        db.SaveChanges();
                        Msgbox.MsgBox_In_Ajax("刪除完成", this.Page, Request.RawUrl);


                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                        logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_SysUser", User.ID));
                        db.Dispose();
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    Msgbox.MsgBox_In_Ajax("刪除失敗", this.Page, Request.RawUrl);

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                    logger.Error(ex);
                    #endregion
                }
            }
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("AccountDetail.aspx?StrID=" + e.CommandArgument.ToString());
        }

        protected void cmdadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AccountDetail");
        }
    }
}