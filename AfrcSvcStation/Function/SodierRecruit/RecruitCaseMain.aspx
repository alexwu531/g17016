﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RecruitCaseMain.aspx.cs" Inherits="AfrcSvcStation.Function.SodierRecruit.RecruitCaseMain" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 36px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>

    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
            <h1>預備戰士報名查詢</h1>
        </div>

        <div class="col-xs-12 col-md-10 col-md-offset-1">
                        <!-- search-area  BEGINS -->
            <div class="widget-box">
                <div class="widget-header widget-header-small">
                    <h5 class="widget-title lighter">查詢條件</h5>
                </div>

                <div class="widget-body">
                    <div class="widget-main">

                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-lg-6 ">
                                <div class="input-group">
                                    <span class="input-group-addon">召集單位
                                    </span>
                                    <asp:TextBox ID="txtUserID" runat="server" CssClass="form-control search-query" placeholder="請填入帳號"></asp:TextBox>
                                </div>

                                <div class="hr"></div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                    </span>
                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control search-query" placeholder="請填入號碼"></asp:TextBox>
                                </div>

                                <div class="hr"></div>
                                <%-- 
                                <div class="input-group">
                                    <span class="input-group-addon">帳號是否使用
                                    </span>
                                    <asp:DropDownList ID="ddlUseState" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                                        <asp:ListItem Value="1">使用中</asp:ListItem>
                                        <asp:ListItem Value="2">停用</asp:ListItem>
                                    </asp:DropDownList>
                                </div>--%>
                                <div class="hr"></div>
                                <asp:Button ID="btnSearch" runat="server" Text="查詢" CausesValidation="False" CommandName="Search" CssClass="btn btn-purple btn-sm" OnClick="btnSearch_Click" />
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            <div class="hr hr-18 dotted hr-double"></div>
            <div class="dataTables_wrapper form-inline no-footer">

                
            <asp:UpdatePanel ID="up1" runat="server">
                <ContentTemplate>

                    <div class="dataTables_wrapper form-inline no-footer">


                        <table id="simple-table" class="table  table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>個案號碼</th>
                                    <th>姓名</th>
                                    <th>身分證字號</th>
                                    <th>電話</th>
                                    <th>報名單位</th>
                                    <th>報名時間</th>
                                    <th>受理時間</th>
                                    <th>受理狀態</th>
                                    <th>受理人</th>
                                    <th>受理單位</th>
                                    <th>結案時間</th>
                                    <th>最近更新人</th>
                                    <th>最近更新時間</th>
                                    <th>編輯</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repRecruitCaseView" runat="server" OnItemDataBound="repRecruitCaseView_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                              
                                            <!--個案號碼-->
                                            <td><%#Eval("CaseNumber")%></td>
                                            <!--姓名-->
                                            <td><%#Eval("CaseName")%></td>
                                            <!--身分證字號-->
                                            <td><%#Eval("CaseIdNumber")%></td>
                                            <!--電話-->
                                            <td><%#Eval("Phone")%></td>
                                            <!--報名單位-->
                                            <td><%#Eval("UnitVacancy")%></td>
                                            <!--報名時間-->
                                            <td><%#Eval("SignDate")%></td>
                                            <!--受理時間-->
                                            <td><%#Eval("CaseAcceptDate")%></td>
                                            <!--受理狀態-->
                                            <td><%#Eval("CaseStatus")%></td>
                                            <!--受理人-->
                                            <td><%#Eval("DealerID")%></td>
                                            <!--受理單位-->
                                            <td><%#Eval("CaseAcceptDate")%></td>
                                            <!--結案時間-->
                                            <td><%#Eval("CaseClosedDate")%></td>
                                            <!--最近更新者-->
                                            <td><%#Eval("ModifiedPersonID")%></td>
                                            <!--最近更新時間-->
                                            <td><%#Eval("ModifiedDate")%></td>
                                           
                                        <td class="center"><asp:Button ID="btnEdit" runat="server" Text="編輯" CssClass="btn btn-xs btn-info" CausesValidation="False" CommandName="Edit" CommandArgument='<%#Eval("RecruitCaseID")%>' OnCommand="btnEdit_Command"/></td>                                        
<%--                                            <a class="btn btn-xs btn-warning" href='<%# ResolveUrl("GroupPowerEdit")+"?StrID="+Eval("GroupID").ToString() %>'><i class="ace-icon fa fa-key bigger-120"></i></a>--%>
                                        <td><asp:Button ID="btnDelete" runat="server" Text="刪除" CausesValidation="False" CommandName="Delete" CssClass="btn btn-xs btn-danger" CommandArgument='<%#Eval("RecruitCaseID")%>' OnCommand="btnDelete_Command" OnClientClick="return confirm('確認刪除?')"/></td>
                                        </tr>
                                    </ItemTemplate>

                                </asp:Repeater>

                            </tbody>
                        </table>

                        <div class="row">

                            <div class="col-xs-12">
                                <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                                        ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                                    </webdiyer:AspNetPager>

                                </div>
                            </div>
                        </div>

                    </div>


                    <!-- PAGE CONTENT ENDS -->
                    </div>
    <!-- /.col -->




                </ContentTemplate>

            </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <!-- #dialog-message -->
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
</asp:Content>
