﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RecruitCaseDetail.aspx.cs" Inherits="AfrcSvcStation.Function.SodierRecruit.RecruitCaseDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="page-header">
                <h1>報名詳細內容
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    報名內容
                                </small>
                </h1>
            </div>

            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal" role="form">
                <!--編號-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">編號</label>

                    <div class="col-sm-9">
                        <%--<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="txtUserID" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>

                <div class="space-4"></div>
                <!--姓名-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">姓名</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="txtUserName" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                
                <div class="space-4"></div>
                <!--身分證字號-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">身分證字號</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox1" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--電話-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">電話</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox2" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--家電-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">家電</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox3" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                  <!--個案地址-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">個案地址</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox12" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <!--Email-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Email</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox4" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--生日-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">生日</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox5" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--退伍日期-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">退伍日期</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox6" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--報名日期-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">報名日期</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox13" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--報名單位-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">報名單位</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox7" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--報名職位-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">報名職位</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox8" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--退伍階級-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">退伍階級</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox9" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <!--受理部門-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">受理部門</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox10" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <!--受理狀態-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">受理狀態</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox11" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--受理人-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">受理人</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox14" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--受理日期-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">受理日期</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox15" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--結案日期-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">結案日期</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox16" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--上次更新者-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">上次更新者</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox17" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="space-4"></div>
                <!--最近更新時間-->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">最近更新時間</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="TextBox18" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>

                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        											<button runat="server" class="btn" type="reset" onserverclick="Unnamed_ServerClick1">
                                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                                返回
                                            </button>
                        &nbsp; &nbsp; &nbsp;
                                                <button runat="server" class="btn btn-info" type="button" onserverclick="Unnamed_ServerClick">
                            <i class="ace-icon fa fa-check bigger-110"></i>
                            送出</button>
                    </div>
                </div>
            </div>

            <div class="hr hr-24"></div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
</asp:Content>
