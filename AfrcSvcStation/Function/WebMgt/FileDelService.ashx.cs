﻿using AFRCAdmin.App_Code;
using AFRCAdmin.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace AFRCAdmin.WebMgt
{
    /// <summary>
    /// FileDelService 的摘要描述
    /// </summary>
    public class FileDelService : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            string fid = HttpUtility.UrlDecode(context.Request.QueryString["FileID"].ToString());
            object Isalbum = HttpUtility.UrlDecode(context.Request.QueryString["Isalbum"]);


            int _fid = int.Parse(fid);


            //宣告返回的JSON物件
            JObject resultJO = new JObject();
            JArray JA = new JArray();
            JObject JD = new JObject();

            AFRCEntities db = new AFRCEntities();
            try
            {
                if (Isalbum != null)
                {
                    if (Isalbum.ToString() == "1")
                    {
                        T_Album f = db.T_Album.Where(o => o.FileID == _fid).FirstOrDefault();

                        //db.T_Album.Remove(f);
                        f.Del = 2;
                        db.SaveChanges();


                        resultJO = new JObject(
                           new JProperty("Result", "1"),
                           new JProperty("MSG", "刪除成功")
                               );
                    }
                }
                else
                {
                    T_ContentEXLink f = db.T_ContentEXLink.Where(o => o.ID == _fid).FirstOrDefault();

                    db.T_ContentEXLink.Remove(f);

                    db.SaveChanges();


                    resultJO = new JObject(
                       new JProperty("Result", "1"),
                       new JProperty("MSG", "刪除成功")
                           );
                }
             
            }
            catch (Exception ex)
            {

                resultJO = new JObject(
                   new JProperty("Result", "0"),
                   new JProperty("MSG", "刪除失敗")
                       );
            }
            finally
            {
                db.Dispose();
            }

            context.Response.Write(JsonConvert.SerializeObject(resultJO));









        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}