﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AFRCAdmin.WebUserControl;
using AFRCAdmin.App_Code;
using AFRCAdmin.Models;

namespace AFRCAdmin.WebMgt
{
    public partial class EditHomeManagment : System.Web.UI.Page
    {
        protected string PicName;
        protected string PicPath;
        protected string sn = HttpContext.Current.Request.QueryString["sn"];
        WebMessage MsgBox = new WebMessage();
        FileUploadPathHelper PathHelper = new FileUploadPathHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataBind();
            }
        }

        protected void DataBind()
        {
            AFRCEntities db = new AFRCEntities();
            try
            {
                int sn = Convert.ToInt32(this.sn);
                int WebID = Convert.ToInt32(GetWebID());

                var NewsList = (from N in db.news
                                where N.WebID == WebID && N.sn == sn
                                select N).FirstOrDefault();
                if (NewsList != null)
                {
                    txtCaption.Text = NewsList.caption;
                    Wucd.CNDate = Convert.ToDateTime(NewsList.d).ToShortDateString();
                    string qq = NewsList.is_show.ToString();
                    ddlIsShow.SelectedValue = NewsList.is_show.ToString() == "True" ? "1" : "0";
                    ddlIsShowMain.SelectedValue = NewsList.PublishMainPage.ToString() == "True" ? "1" : "0";
                    txtContr.Text = Server.HtmlDecode(NewsList.content);

                    string SavePath = "\\Uploads\\News\\" + NewsList.pic;
                    pic.ImageUrl = SavePath;

                    MetaData clsCMetaData = new MetaData();
                    MetaData CSmetaData = clsCMetaData.GetMetaData(NewsList.MetaData);//解析XML後把XML的值帶入欄位
                    hfThemeValue.Value = CSmetaData.Theme;
                    hfCakeValue.Value = CSmetaData.Cake;
                    hfServiceValue.Value = CSmetaData.Service;

                    WucMetaDataControl.MetaDataXML = NewsList.MetaData;
                }
                db.Dispose();
            }
            catch (Exception ex)
            {
                db.Dispose();
            }
            finally
            {
                db.Dispose();
            }
        }

        #region GetWebID
        protected string GetWebID()
        {//GetWebID
            return Session["WEBID"].ToString();
        }
        #endregion

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string checkMsg = "";
            if (string.IsNullOrEmpty(txtCaption.Text))
            {
                checkMsg += "標題欄位必填\\n";
            }
            if (string.IsNullOrEmpty(Wucd.CNDate))
            {
                checkMsg += "發佈日期欄位必填\\n";
            }
            if (ddlIsShow.SelectedValue.ToString() == "x")
            {
                checkMsg += "請選擇是否顯示\\n";
            }
            if (ddlIsShowMain.SelectedValue.ToString() == "x")
            {
                checkMsg += "請選擇是否顯示於主網\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metatitle))
            {
                checkMsg += "分類檢索標題欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metasubject))
            {
                checkMsg += "分類檢索主旨欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaCreator))
            {
                checkMsg += "分類檢索作者姓名欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaPublisher))
            {
                checkMsg += "分類檢索機關全稱欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaWucDate))
            {
                checkMsg += "分類檢索製作日期欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaIdengifier))
            {
                checkMsg += "分類檢索OID欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metatheme))
            {
                checkMsg += "分類檢索主題分類欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metacake))
            {
                checkMsg += "分類檢索施政分類欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaservice))
            {
                checkMsg += "分類檢索服務分類欄位必填\\n";
            }

            try
            {
                DateTime S = Convert.ToDateTime(Wucd.CNDate);
            }
            catch (Exception)
            {
                checkMsg += "發佈日期請書入正確日期格式";
            }

            if (!string.IsNullOrEmpty(checkMsg))
            {
                MsgBox.MsgBox_In_Ajax(checkMsg, this.Page);
                return;
            }


            if (string.IsNullOrEmpty(this.sn))
            {//新增
                #region 新增
                try
                {
                    AFRCEntities db = new AFRCEntities();


                    news NewsList = new news(); ;
                    if (NewsList != null)
                    {
                        NewsList.WebID = Convert.ToInt32(GetWebID());
                        NewsList.caption = txtCaption.Text.Trim();//標題
                        NewsList.Title = txtCaption.Text.Trim();//標題
                        NewsList.Date = Convert.ToDateTime(Wucd.CNDate);
                        NewsList.d = Convert.ToDateTime(Wucd.CNDate);
                        NewsList.is_show = ddlIsShow.SelectedValue.ToString() == "1" ? true : false;
                        NewsList.PublishMainPage = ddlIsShowMain.SelectedValue.ToString() == "1" ? true : false;
                        NewsList.content = Server.HtmlDecode(txtContr.Text);

                        if (FileUpload.HasFile)
                        {
                            string FilePath1 = PathHelper.GetUpoladPathByVer(GetWebID());
                            string SavePath = FilePath1 + "News\\" + FileUpload.FileName;
                            NewsList.pic = FileUpload.FileName;
                            NewsList.pic_path = "\\Uploads\\News\\";
                            //上傳
                            FileUpload.SaveAs(SavePath);
                        }


                        //MenuData
                        WucMetaDataControl.metatheme = (hfThemeValue.Value).ToString().TrimEnd(',');
                        WucMetaDataControl.metacake = (hfCakeValue.Value).ToString().TrimEnd(',');
                        WucMetaDataControl.metaservice = (hfServiceValue.Value).ToString().TrimEnd(',');
                        NewsList.MetaData = WucMetaDataControl.MetaDataXML;

                        db.news.Add(NewsList);
                        db.SaveChanges();
                        MsgBox.MsgBox_In_Ajax("新增成功", this.Page, "HomeManagment");
                        db.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    //Logger.ErrorException("新增使用者", ex);
                    MsgBox.MsgBox_In_Ajax("新增失敗", this.Page, Request.RawUrl);
                }
                #endregion
            }
            else
            {//編輯
                try
                {
                    AFRCEntities db = new AFRCEntities();
                    int sn = Convert.ToInt32(this.sn);
                    news NewsList = db.news.FirstOrDefault(o => o.sn == sn);

                    if (NewsList != null)
                    {
                        NewsList.caption = txtCaption.Text.Trim();//標題
                        NewsList.Title = txtCaption.Text.Trim();//標題
                        NewsList.Date = Convert.ToDateTime(Wucd.CNDate);
                        NewsList.d = Convert.ToDateTime(Wucd.CNDate);
                        NewsList.is_show = ddlIsShow.SelectedValue.ToString() == "1" ? true : false;
                        NewsList.PublishMainPage = ddlIsShowMain.SelectedValue.ToString() == "1" ? true : false;
                        NewsList.content = Server.HtmlDecode(txtContr.Text);

                        if (FileUpload.HasFile)
                        {
                            string FilePath1 = PathHelper.GetUpoladPathByVer(GetWebID());
                            string SavePath = FilePath1 + "News\\" + FileUpload.FileName;
                            NewsList.pic = FileUpload.FileName;
                            NewsList.pic_path = "\\Uploads\\News\\";
                            //上傳
                            FileUpload.SaveAs(SavePath);
                        }

                        //MenuData
                        WucMetaDataControl.metatheme = (hfThemeValue.Value).ToString().TrimEnd(',');
                        WucMetaDataControl.metacake = (hfCakeValue.Value).ToString().TrimEnd(',');
                        WucMetaDataControl.metaservice = (hfServiceValue.Value).ToString().TrimEnd(',');
                        NewsList.MetaData = WucMetaDataControl.MetaDataXML;
                        db.SaveChanges();

                        db.Dispose();
                        //ScriptManager.RegisterStartupScript(Page, GetType(), "CloseFrame", "<script>parent.CloseFrame();</script>", false);
                        MsgBox.MsgBox_In_Ajax("編輯成功", this.Page, "HomeManagment");
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("HomeManagment");
        }

        protected void btnUploadPic_Click(object sender, EventArgs e)
        {

        }
    }
}