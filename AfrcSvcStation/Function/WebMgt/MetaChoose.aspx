﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MetaChoose.aspx.cs" Inherits="AFRCAdmin.MetaChoose" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
        <script language="javascript">
        function funReturn() {
            var Obj1 = "<%=Request["ValueCID"].ToString()%>";
            var Obj2 = "<%=Request["TextCID"].ToString()%>";

            var rtnObj1 = parent.window.opener.document.getElementById(Obj1);
            var rtnObj2 = parent.window.opener.document.getElementById(Obj2);

            var rtValue = document.getElementById("hfRtValue").value;
            var rtText = document.getElementById("hfRtText").value;
            rtnObj1.value = rtValue
            rtnObj2.textContent = rtText

	        window.close();
	    }
    </script>    
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="SM1" runat="server">
            </asp:ScriptManager>
            <br />
            <asp:HiddenField ID="hfRtValue" runat="server"  ClientIDMode="Static"/>
            <asp:HiddenField ID="hfRtText" runat="server"  ClientIDMode="Static"/>
            <br />
            <asp:TreeView ID="TreeView1" runat="server"></asp:TreeView>
            <asp:Button ID="Button1" runat="server" Text="確認" OnClick="Button1_Click" Style="height: 21px" />
        </div>
    </form>
</body>
</html>
