﻿using AFRCAdmin.App_Code;
using AFRCAdmin.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace AFRCAdmin.WebMgt
{
    /// <summary>
    /// FileUploadService 的摘要描述
    /// </summary>
    public class FileUploadService : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            string webid = HttpUtility.UrlDecode(context.Request.QueryString["webid"].ToString());
            string menuid = HttpUtility.UrlDecode(context.Request.QueryString["menuid"].ToString());
            string ismenu = HttpUtility.UrlDecode(context.Request.QueryString["ismenu"].ToString());

            int _webid = int.Parse(webid);
            int _menuid = int.Parse(menuid);
            int _ismenu = int.Parse(ismenu);

            //宣告返回的JSON物件
            JObject resultJO = new JObject();
            JArray JA = new JArray();
            JObject JD = new JObject();


            //跑所有檔案
            for (int i = 0; i < context.Request.Files.Count; i++)
            {
                try
                {
                    //取得檔案物件
                    HttpPostedFile f = context.Request.Files[i];

                    if (!string.IsNullOrEmpty(System.IO.Path.GetFileName(f.FileName)))
                    {
                        AFRCEntities db = new AFRCEntities();

                        try
                        {
                            FileUploadPathHelper FileUploadPathHelper = new FileUploadPathHelper();

                            string rootpath = FileUploadPathHelper.GetUpoladPathByVer(_webid.ToString()) + @"Files\";

                            string fullpath = FileUploadPathHelper.GetFullPathByMenuID(_menuid, rootpath);

                            string savepath = fullpath + @"\" + f.FileName;

                            f.SaveAs(rootpath + savepath);

                            int sort = 0;
                            var vsort = db.T_ContentEXLink.Where(o => o.ParentID == _menuid && o.WebID == _webid).Select(o => o.Sort).ToArray().Max();
                            if (vsort == null)
                            {
                                sort = 1;
                            }
                            else
                            {
                                sort = Convert.ToInt32(vsort);
                                sort = sort + 1;
                            }


                            string webfilepath = db.T_Web.Where(w => w.WebID == _webid).Select(s => s.WebUrl).FirstOrDefault();

                            string rtPath = webfilepath + @"Uploads\Files\" + savepath;


                            T_ContentEXLink Content = new T_ContentEXLink
                            {
                                isMenu = _ismenu,
                                Link = savepath,
                                ParentID = _menuid,
                                Sort = sort,
                                Title = Path.GetFileNameWithoutExtension(f.FileName),
                                Type = 2,
                                WebID = _webid

                            };
                            db.T_ContentEXLink.Add(Content);

                            db.SaveChanges();

                            JD = new JObject(
                                    new JProperty("FileName", f.FileName),
                                       new JProperty("ID", Content.ID),
                                    new JProperty("FilePath", rtPath)

                        );
                            JA.Add(JD);

                            resultJO = new JObject(
                         new JProperty("Result", "1"),
                         new JProperty("Data", JA)
                         );

                        }
                        catch (Exception ex)
                        {
                            //上傳失敗
                            resultJO = new JObject(
                               new JProperty("Result", "0"),
                               new JProperty("MSG", "")
                                   );
                        }
                        finally
                        {
                            db.Dispose();
                        }

                    }
                    else
                    {
                        //上傳失敗
                        resultJO = new JObject(
                           new JProperty("Result", "0"),
                           new JProperty("MSG", "")
                               );
                    }

                }
                catch (Exception ex)
                {
                    resultJO = new JObject(
                           new JProperty("Result", "0"),
                           new JProperty("MSG", "")
                                 );
                }
            }
            context.Response.Write(JsonConvert.SerializeObject(resultJO));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}