﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AFRCAdmin.App_Code;
using AFRCAdmin.Models;
using AFRCAdmin.WebUserControl;
using NLog;

namespace AFRCAdmin.WebMgt
{
    public partial class ListContentManagment : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected UserInfo so = new UserInfo();
        protected int MenuLevel;
        protected string MenuID = HttpContext.Current.Request.QueryString["MenuID"];
        protected string MenuType = HttpContext.Current.Request.QueryString["MenuType"];
        protected string ListID = HttpContext.Current.Request.QueryString["ListID"];
        protected string Type = HttpContext.Current.Request.QueryString["Type"];
        protected string IsMenu;
        WebMessage MsgBox = new WebMessage();

        protected void Page_Load(object sender, EventArgs e)
        {
            UserInfo so = (UserInfo)Session["UserData"];
            hfWebID.Value = "1";
            hfMenuID.Value = MenuID;
            hfIsMenu.Value = "2";
            hfMenuType.Value = MenuType;
            hfListID.Value = ListID;
            hfType.Value = Type;

            #region 判斷是否具審核權限
            if (so.UserGroup.ToString() == "1")
            {
                divAudit.Style.Add("display", "block");
            }
            #endregion


            if (Type == "1")
            {//HTML編輯
                content1.Style.Add("display", "block");
                content_index.Style.Add("display", "block");
                rdbMenuType1.Checked = true;
            }
            //if (MenuType == "7")
            //{//簡易編輯
            //    content7.Style.Add("display", "block");
            //    rdbMenuType7.Checked = true;
            //}
            if (Type == "2")
            {
                content2.Style.Add("display", "block");
                rdbMenuType2.Checked = true;
            }
            //if (MenuType == "5")
            //{//自定列表
            //    content4.Style.Add("display", "block");
            //    rdbMenuType5.Checked = true;
            //    hfIsMenu.Value = "1";
            //}
            //if (MenuType == "6")
            //{
            //    content6.Style.Add("display", "block");
            //    rdbMenuType6.Checked = true;
            //}
            //if (MenuType == "1")
            //{
            //    content1.Style.Add("display", "block");
            //    rdbMenuType1.Checked = true;
            //}

            if (!IsPostBack)
            {
                DataBind();
                BindSonDept();
                BindMenuList();
            }
        }

        private void DataBind()
        {
            AFRCEntities db = new AFRCEntities();
            try
            {
                int MenuID = Convert.ToInt32(this.MenuID);
                int MenuType = Convert.ToInt32(this.MenuType);
                int ListID = Convert.ToInt32(this.ListID);
                int Type = Convert.ToInt32(this.Type);

                var List = (from L in db.T_List
                            where L.ListID == ListID
                            select L).FirstOrDefault();

                if (List != null)
                {
                    //選單名稱
                    txtMenuName.Text = List.Title;

                    /*內容區塊*/
                    switch (Type)
                    {
                        case 1:
                            //內容-HTML編輯
                            txtContr.Text = Server.HtmlDecode(List.Content);

                            MetaData clsCMetaData = new MetaData();
                            MetaData CSmetaData = clsCMetaData.GetMetaData(List.MetaData);//解析XML後把XML的值帶入欄位
                            hfThemeValue.Value = CSmetaData.Theme;
                            hfCakeValue.Value = CSmetaData.Cake;
                            hfServiceValue.Value = CSmetaData.Service;

                            WucMetaDataControl.MetaDataXML = List.MetaData;
                            break;
                        //case 7:
                        //    //內容-簡易編輯
                        //    EasyContentBind();
                        //    break;
                        case 2:
                            //內容-自訂連結
                            txtMenuUrl.Value = List.Url;
                            break;
                        //case 1:
                        //    //同某選單
                        //    rdblstMenuName.SelectedValue = Menu.MenuLink.ToString();
                        //    break;
                    }
                    /*內容區塊*/

                    //開啟方式
                    //switch (Menu.Target)
                    //{
                    //    case 3:
                    //        rdbTarget3.Checked = true;
                    //        break;
                    //    case 4:
                    //        rdbTarget4.Checked = true;
                    //        break;
                    //    case 1:
                    //        rdbTarget1.Checked = true;
                    //        break;
                    //    case 2:
                    //        rdbTarget2.Checked = true;
                    //        break;
                    //}

                    //發佈單位
                    ddlSonDept.SelectedValue = List.DepID.ToString(); ;

                    //刊登
                    switch (List.Publish)
                    {
                        case 1:
                            chkPublic1.Checked = true;
                            break;
                        case 2:
                            chkPublic2.Checked = true;
                            break;
                        case 3:
                            chkPublic1.Checked = true;
                            chkPublic2.Checked = true;
                            break;
                    }

                    //審核
                    switch (List.Audit)
                    {
                        case 1:
                            chkAudit.Checked = true;
                            break;
                    }

                    //編輯資訊
                    string[] EditInfo = new string[4];
                    EditInfo = GetEditInfo(ListID);
                    lblCreateUser.Text = EditInfo[0];
                    lblCreateTime.Text = EditInfo[1];
                    lblModifyUser.Text = EditInfo[2];
                    lblModifyTime.Text = EditInfo[3];

                    #region ActionLog
                    string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(string.Format("資料表:T_List;主鍵{0};單元名稱:{1}", List.ListID.ToString(), List.Title));
                    #endregion
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

        /// <summary>
        /// 回傳選單編輯相關資訊
        /// </summary>
        /// <param name="intMenuID"></param>
        /// <returns></returns>
        private string[] GetEditInfo(int intMenuID)
        {
            string[] rtValue = new string[4];

            AFRCEntities db = new AFRCEntities();
            try
            {
                var Info = (from L in db.T_List
                            where L.ListID == intMenuID
                            select L).FirstOrDefault();
                if (Info != null)
                {
                    rtValue[0] = IDtoName(Info.CreatePersonID);
                    rtValue[1] = Info.CreateDate.ToString();
                    rtValue[2] = IDtoName(Info.EditPersonID);
                    rtValue[3] = Info.EditDate.ToString();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                db.Dispose();
            }
            finally
            {
                db.Dispose();
            }

            return rtValue;
        }

        /// <summary>
        /// 使用者唯一值轉使用者名稱
        /// </summary>
        /// <param name="intID">User IdentityID</param>
        /// <returns></returns>
        private string IDtoName(int intID)
        {
            string rtValue = "";

            AFRCEntities db = new AFRCEntities();
            try
            {
                var User = (from U in db.T_SysUser
                            where U.ID == intID
                            select U.UserName).FirstOrDefault();
                if (User != null)
                {
                    rtValue = User;
                }
            }
            catch (Exception ex)
            {
                db.Dispose();
            }
            finally
            {
                db.Dispose();
            }

            return rtValue;
        }

        /// <summary>
        /// 第二層部門綁定
        /// </summary>
        public void BindSonDept()
        {
            try
            {
                using (AFRCEntities db = new AFRCEntities())
                {
                    var Dept = (from D in db.T_Dept
                                orderby D.sort
                                where D.del == 1 && D.sn != 0 && D.belong_sn != 0
                                select D).ToList();

                    ddlSonDept.DataSource = Dept;
                    ddlSonDept.DataTextField = "caption";
                    ddlSonDept.DataValueField = "sn";
                    ddlSonDept.DataBind();

                    ddlSonDept.Items.Insert(0, new ListItem("請選擇部門", "0"));
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 選單名稱綁定
        /// </summary>
        public void BindMenuList()
        {
            try
            {
                using (AFRCEntities db = new AFRCEntities())
                {
                    int MenuID = Convert.ToInt32(this.MenuID);
                    int _WebID = Convert.ToInt32(Session["WEBID"].ToString());
                    var MenuLevel = (from M in db.T_Menu
                                     where M.MenuID == MenuID && M.WebID== _WebID
                                     select M.MenuLevel).FirstOrDefault();

                    var lst = (from M in db.T_Menu
                               where M.MenuLevel == MenuLevel
                               select M).ToList();

                    rdblstMenuName.DataSource = lst;
                    rdblstMenuName.DataTextField = "MenuName";
                    rdblstMenuName.DataValueField = "MenuID";
                    rdblstMenuName.DataBind();

                    ddlSonDept.Items.Insert(0, new ListItem("請選擇部門", "0"));
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {//編輯
            try
            {
                UserInfo so = (UserInfo)Session["UserData"];
                int ListID = Convert.ToInt32(this.ListID);
                int Type = Convert.ToInt32(this.Type);

                AFRCEntities db = new AFRCEntities();
                T_List List = db.T_List.FirstOrDefault(u => u.ListID == ListID);

                if (List != null)
                {
                    List.Title = txtMenuName.Text;//選單名稱
                    List.EditDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                    List.EditPersonID = int.Parse(so.UserPKID);
                    List.WorkYear = DateTime.Now.Year; //int.Parse(WorkYears.SelectedValue);
                    //List.WorkDate = WorkDate.Text;
                    List.DepID = int.Parse(ddlSonDept.SelectedValue);
                    List.Type = Convert.ToInt32(Type);//選單類別

                    switch (Type)
                    {
                        case 1://HTML編輯
                            List.Content = Server.HtmlDecode(txtContr.Text);

                            //WucMetaDataControl.metatheme = (hfThemeValue.Value).ToString().TrimEnd(',');
                            //WucMetaDataControl.metacake = (hfCakeValue.Value).ToString().TrimEnd(',');
                            //WucMetaDataControl.metaservice = (hfServiceValue.Value).ToString().TrimEnd(',');
                            List.MetaData = WucMetaDataControl.MetaDataXML;

                            List.BakContent = "";
                            break;
                        //case 7://簡易編輯

                        //    break;
                        case 2://自訂連結
                            List.Url = txtMenuUrl.Value.ToString();
                            break;
                        //case 1://同某選單
                        //    Menu.MenuLink = Convert.ToInt32(rdblstMenuName.SelectedValue.ToString());
                        //    break;
                    }

                    //int target = 0;
                    //if (rdbTarget3.Checked)
                    //{//本頁切換 ( 不含選單外框 ) 
                    //    target = 3;
                    //}
                    //if (rdbTarget4.Checked)
                    //{//另開視窗 ( 不含選單外框 )
                    //    target = 4;
                    //}
                    //if (rdbTarget1.Checked)
                    //{//本頁切換 ( 包含選單外框 )
                    //    target = 1;
                    //}
                    //if (rdbTarget2.Checked)
                    //{//自訂列表
                    //    target = 2;
                    //}
                    //Menu.Target = target;//使用者群組權限

                    int PublishCount = 0;
                    if (chkPublic1.Checked)
                    {
                        PublishCount = 1;
                    }
                    if (chkPublic2.Checked)
                    {
                        PublishCount = 2;
                    }
                    if (chkPublic1.Checked && chkPublic2.Checked)
                    {
                        PublishCount = 3;
                    }

                    List.Publish = PublishCount;//刊登
                    List.Audit = chkAudit.Checked ? 1 : 2;//審核狀態
                    so = (UserInfo)Session["UserData"];
                    db.SaveChanges();
                    #region ActionLog
                    string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                    logger.Info(string.Format("資料表:T_List;主鍵{0};單元名稱:{1}", List.ListID.ToString(), List.Title));
                    #endregion
                    db.Dispose();
                    MsgBox.MsgBox_In_Ajax("編輯成功", this.Page);
                    ScriptManager.RegisterStartupScript(Page, GetType(), "CloseFrame", "<script>parent.CloseFrame();</script>", false);
                }
            }
            catch (Exception ex)
            {
                MsgBox.MsgBox_In_Ajax("編輯失敗", this.Page);
            }
        }

        /// <summary>
        /// 取得簡易編輯列表
        /// </summary>
        private void EasyContentBind()
        {
            AFRCEntities db = new AFRCEntities();

            try
            {
                var ezlist = (from ez in db.T_EasyContent
                              select new { ez.ID, ez.ParentID }).ToList();

                repEasyContent.DataSource = ezlist;
                repEasyContent.DataBind();
            }
            catch (Exception ex)
            {

            }
        }

        protected void repEasyContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField HfEzcontentID = (HiddenField)e.Item.FindControl("hfEzcontentID");
                WucSimpleEdit WucEz = (WucSimpleEdit)e.Item.FindControl("WucSimpleEdit");

                AFRCEntities db = new AFRCEntities();
                try
                {
                    int ParentID = Convert.ToInt32(this.MenuID);
                    int EzID = Convert.ToInt32(HfEzcontentID.Value);

                    var ezlst = (from ez in db.T_EasyContent
                                 where ez.ParentID == ParentID && ez.ID == EzID
                                 select ez.Content).FirstOrDefault();
                    if (ezlst != null)
                    {
                        WucEz.ezcontent = ezlst;
                        db.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    db.Dispose();
                }
                finally
                {
                    db.Dispose();
                }
            }
        }

        protected void btnAddEzContent_Click(object sender, EventArgs e)
        {
            #region 新增
            try
            {
                using (AFRCEntities db = new AFRCEntities())
                {
                    int MenuID = Convert.ToInt32(this.MenuID);

                    T_EasyContent ez = new T_EasyContent();
                    ez.ParentID = Convert.ToInt32(this.MenuID);
                    ez.CreateDate = DateTime.UtcNow.AddHours(8);
                    ez.EditDate = DateTime.UtcNow.AddHours(8);
                    ez.EditPerson = Convert.ToInt32(so.UserPKID);
                    ez.Content = txtEzContent.Value;

                    int sort = 0;
                    var vsort = db.T_EasyContent.Where(o => o.ParentID == MenuID).Select(o => o.sort).ToArray().Max();
                    if (vsort == null)
                    {
                        sort = 1;
                    }
                    else
                    {
                        sort = Convert.ToInt32(vsort) + 1;
                    }
                    ez.sort = sort;
                    ez.Type = 1;
                    //ez.UserName = txtUserName.Text;//使用者名稱
                    //ez.RoleID = Convert.ToInt32(ddlUserGroup.SelectedValue.ToString());//使用者群組權限
                    //ez.UserPassword = txtPSW.Text;//密碼
                    //ez.DeptID = Convert.ToInt32(ddlUserDept.SelectedValue.ToString());//部門
                    //ez.UseState = ddlUseState.SelectedValue.ToString();//帳戶使用狀態
                    //ez.UserEmail = txtEmail.Text.Trim();//信箱
                    //ez.UserPhone = txtPhone.Text.Trim();//分機
                    //ez.ModifyTime = DateTime.UtcNow.AddHours(8);
                    //ez.ModifyUser = so.UserID;

                    db.T_EasyContent.Add(ez);
                    db.SaveChanges();

                    MsgBox.MsgBox_In_Ajax("新增成功", this.Page);
                    EasyContentBind();
                }
            }
            catch (Exception ex)
            {
                //Logger.ErrorException("新增使用者", ex);
                MsgBox.MsgBox_In_Ajax("新增失敗", this.Page, Request.RawUrl);
            }
            #endregion
        }

        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                try
                {
                    AFRCEntities db = new AFRCEntities();

                    int ID = Convert.ToInt32(e.CommandArgument.ToString());
                    T_EasyContent User = db.T_EasyContent.FirstOrDefault(c => c.ID.ToString() == e.CommandArgument.ToString());
                    if (User != null)
                    {
                        db.T_EasyContent.RemoveRange(db.T_EasyContent.Where(c => c.ID == ID));
                        db.SaveChanges();
                        MsgBox.MsgBox_In_Ajax("刪除完成", this.Page, Request.RawUrl);
                    }
                }
                catch (Exception ex)
                {
                    MsgBox.MsgBox_In_Ajax("刪除失敗", this.Page, Request.RawUrl);
                }
            }
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {

        }

        protected void repEasyContent_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                try
                {
                    UserInfo so = (UserInfo)Session["UserData"];
                    int ID = Convert.ToInt32(e.CommandArgument);

                    using (AFRCEntities db = new AFRCEntities())
                    {
                        WucSimpleEdit WucEz = (WucSimpleEdit)e.Item.FindControl("WucSimpleEdit");

                        int gid = int.Parse(e.CommandArgument.ToString());
                        //修改
                        T_EasyContent ez = db.T_EasyContent.Where(c => c.ID == ID).FirstOrDefault();

                        ez.EditPerson = Convert.ToInt32(so.UserPKID);
                        ez.EditDate = DateTime.UtcNow.AddHours(8);
                        ez.Content = WucEz.ezcontent;
                        db.SaveChanges();
                        MsgBox.MsgBox_In_Ajax("修改完成", this.Page);
                    }
                }
                catch (Exception ex)
                {
                }

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, GetType(), "CloseFrame", "<script>parent.CloseFrame(); parent.location.reload();</script>", false);
        }
    }
}