﻿using AFRCAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using AFRCAdmin.App_Code;
namespace AFRCAdmin.WebMgt
{
    /// <summary>
    /// Bulkfileupload 的摘要描述
    /// </summary>
    public class Bulkfileupload : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            System.Collections.Specialized.NameValueCollection nvc = context.Request.Form;

            FileUploadPathHelper fhelper = new FileUploadPathHelper();


            int webid = int.Parse(HttpContext.Current.Session["WEBID"].ToString());

            string folderPath = fhelper.GetUpoladPathByVer(webid.ToString()) + @"Album\";

            for (int i = 0; i < context.Request.Files.Count; i++)
            {
                HttpPostedFile f = context.Request.Files[i];

                string Rename = System.DateTime.Now.ToString("HmsyyyyMMdd_") + System.IO.Path.GetFileName(f.FileName);

                if (!string.IsNullOrEmpty(System.IO.Path.GetFileName(f.FileName)))
                {
                    string FileDes = string.Empty;
                    int FileSort = 0;
                    foreach (string key in nvc)
                    {
                        try
                        {
                            string keyname = key.Split('-')[0];
                            int keyId = Convert.ToInt32(key.Split('-')[1].ToString());
                            var keyvalue = nvc[key];
                            if (keyId == i)
                            {
                                if (keyname == "fileDes")
                                {
                                    FileDes = keyvalue;
                                }
                                if (keyname == "sort")
                                {
                                    FileSort = Convert.ToInt32(keyvalue);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //錯誤
                        }


                    }
                    AFRCEntities db = new AFRCEntities();
                    try
                    {
                        /*判斷檔案是否存在*/
                        if (System.IO.File.Exists(folderPath + System.IO.Path.GetFileName(f.FileName)))
                        {
                            f.SaveAs(folderPath + Rename);
                            UserInfo so = (UserInfo)HttpContext.Current.Session["UserData"];

                            T_Album Album = new T_Album();
                            Album.Sort = Convert.ToInt32(FileSort.ToString());
                            Album.WebID = webid;

                            Album.Size = f.ContentLength;
                            Album.FileName = Rename;
                            Album.FileInfo = FileDes;
                            Album.CreateTime = DateTime.UtcNow.AddHours(8);
                            Album.CreateUser = int.Parse(so.UserPKID);
                            Album.Del = 1;
                            db.T_Album.Add(Album);
                            db.SaveChanges();
                        }
                        else
                        {
                            f.SaveAs(folderPath + System.IO.Path.GetFileName(f.FileName));
                            UserInfo so = (UserInfo)HttpContext.Current.Session["UserData"];

                            T_Album Album = new T_Album();
                            Album.Sort = Convert.ToInt32(FileSort.ToString());
                            Album.WebID = webid;

                            Album.Size = f.ContentLength;
                            Album.FileName = f.FileName;
                            Album.FileInfo = FileDes;
                            Album.CreateTime = DateTime.UtcNow.AddHours(8);
                            Album.CreateUser = int.Parse(so.UserPKID);
                            Album.Del = 1;
                            db.T_Album.Add(Album);
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        //錯誤
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}