﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AFRCAdmin.Models;

namespace AFRCAdmin
{
    public partial class MetaChoose : System.Web.UI.Page
    {
        TreeNode FatherNode;
        TreeNode SonNode;
        string Type = HttpContext.Current.Request.QueryString["Type"];
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ////取得資料
                //DataTable dt = null;//請自己加入來源

                ////LINQ查詢  
                AFRCEntities db = new AFRCEntities();
                List<classify_search> lst = new List<classify_search>();
                if (Type == "Theme")
                {
                    lst = (from T in db.classify_search
                           where T.classify_search_code == "Theme"
                           select T).OrderBy(o => o.sort).ToList();
                }
                if (Type == "Cake")
                {
                    lst = (from T in db.classify_search
                           where T.classify_search_code == "Cake"
                           select T).OrderBy(o => o.sort).ToList();
                }
                if (Type == "Service")
                {
                    lst = (from T in db.classify_search
                           where T.classify_search_code == "Service"
                           select T).OrderBy(o => o.sort).ToList();
                }


                List<classify_search> clf = new List<classify_search>();
                GetClassify(null, clf, lst, 0);
                TreeView1.ShowCheckBoxes = TreeNodeTypes.All;
                TreeView1.CollapseAll();
            }
        }


        private void GetClassify(TreeNode node, List<classify_search> lstNewclf, List<classify_search> lstOldclf, int sn)
        {
            lstOldclf.Where(o => o.belong_sn == sn).OrderBy(o => o.sort).ToList().ForEach(x =>
            {
                lstNewclf.Add(x);

                if (x.belong_sn == 0)
                {
                    FatherNode = new TreeNode();//父
                    FatherNode.Text = x.caption.ToString() + "(" +x.id.ToString()+ ")"; //父的顯示名稱
                    FatherNode.Value = x.id.ToString();
                    //FatherNode.SelectAction = TreeNodeSelectAction.None;
                    node = FatherNode;
                    TreeView1.Nodes.Add(node);//將節點加入TreeView  
                }
                else
                {
                    SonNode = new TreeNode();//子
                    SonNode.Text = x.caption.ToString() + "(" + x.id.ToString() + ")"; //父的顯示名稱
                    SonNode.Value = x.id.ToString();
                    //SonNode.SelectAction = TreeNodeSelectAction.None;
                    node = SonNode;
                    FatherNode.ChildNodes.Add(SonNode);//將節點加入TreeView   
                }

                if (lstOldclf.Where(o => o.belong_sn == x.sn).Any())
                {
                    GetClassify(node, lstNewclf, lstOldclf, x.sn);
                }
            });
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string themeValue = "";
                string themeText = "";

                List<string[]> al = new List<string[]>();            

                if (TreeView1.CheckedNodes.Count > 0)
                {
                    foreach (TreeNode node in TreeView1.CheckedNodes)
                    {
                        al.Add(new string[] { node.Value, node.Text });
                    }
                }

                themeValue = "";
                themeText = "";
                foreach (var item in al)
                {
                    themeValue += item[0] + ",";
                    themeText += item[1] + ",";
                }
                hfRtValue.Value = themeValue.TrimEnd(',');
                hfRtText.Value = themeText.TrimEnd(',');



                //Button1.Attributes.Add("OnClick", "funReturn()");
                ScriptManager.RegisterStartupScript(this, GetType(), "return", "funReturn();", true);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
}