﻿using AFRCAdmin.App_Code;
using AFRCAdmin.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
namespace AFRCAdmin.WebMgt
{
    /// <summary>
    /// GetFilesService 的摘要描述
    /// </summary>
    public class GetFilesService : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            string webid = HttpUtility.UrlDecode(context.Request.QueryString["webid"].ToString());
            string menuid = HttpUtility.UrlDecode(context.Request.QueryString["menuid"].ToString());
            string ismenu = HttpUtility.UrlDecode(context.Request.QueryString["ismenu"].ToString());


            int _webid = int.Parse(webid);
            int _menuid = int.Parse(menuid);
            int _ismenu = int.Parse(ismenu);




            //宣告返回的JSON物件
            JObject resultJO = new JObject();
            JArray JA = new JArray();
            JObject JD = new JObject();

            AFRCEntities db = new AFRCEntities();
            FileUploadPathHelper FileUploadPathHelper = new FileUploadPathHelper();
            try
           {
                var files = db.T_ContentEXLink.Where(c => c.ParentID == _menuid && c.WebID == _webid && c.isMenu == _ismenu && c.Type == 2).ToList();

                foreach (var item in files)
                {
                    JD = new JObject(
                                  new JProperty("FileName", item.Title),
                                      new JProperty("ID", item.ID),
                                  new JProperty("FilePath", FileUploadPathHelper.GetFilePathByVer(_webid.ToString()) + @"\Uploads\Files\" + item.Link)

                      );
                    JA.Add(JD);


                }
                resultJO = new JObject(
                new JProperty("Result", "1"),
                new JProperty("Data", JA)
                );

            }
            catch (Exception ex)
            {
                resultJO = new JObject(
                            new JProperty("Result", "0"),
                            new JProperty("MSG", ex.ToString())
                                );
            }
            finally
            {
                db.Dispose();
            }
            context.Response.Write(JsonConvert.SerializeObject(resultJO));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}