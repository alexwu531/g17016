﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlbumManagment.aspx.cs" Inherits="AFRCAdmin.WebMgt.AlbumManagment" %>

<%@ Register Src="~/WebUserControl/WucAlbum.ascx" TagPrefix="uc1" TagName="WucAlbum" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>後備指揮部後台</title>
    <%: Styles.Render("~/bundles/masterstyles") %>
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link href="../css/jquery-ui.min.css" rel="stylesheet" />
    <style type="text/css">
        #rdblstMenuName tr td {
            padding-right: 10px;
        }
    </style>

</head>
<body>
    <script src="../js/jquery-1.11.3.min.js"></script>
        
    <form id="form1" runat="server" class="form-horizontal">
                 <asp:ScriptManager ID="Sc1" runat="server"></asp:ScriptManager>
        <div class="main-content">
            <div class="main-content-inner">

                <div class="page-content">


                    <div class="page-header">
                        <h1>相簿編輯
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    相簿編輯與上傳
                                </small>
                        </h1>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <uc1:WucAlbum runat="server" id="WucAlbum" />

                        </div>
                    </div>
                </div>

            </div>

        </div>

    </form>
    <script src="../js/jquery-2.1.4.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script>
        //數字選擇器
        function Initspinner() {

            $('.sort').spinner({
                min: 0,
                max: 1000000,
                create: function (event, ui) {
                    //add custom classes and icons
                    $(this)
                    .next().addClass('btn btn-success').html('<i class="ace-icon fa fa-plus"></i>')
                    .next().addClass('btn btn-danger').html('<i class="ace-icon fa fa-minus"></i>')

                    //larger buttons on touch devices
                    if ('touchstart' in document.documentElement)
                        $(this).closest('.ui-spinner').addClass('ui-spinner-touch');
                }
            });
            $('.sort').unbind("keypress");
            $(".sort").bind("keydown", function (event) {
                event.preventDefault();
            });
            $(".txtint").spinner({
                min: 0,
                max: 1000000
            });
            $(".txtint").unbind("keypress");


            $(".txtint").bind("keydown", function (event) {
                event.preventDefault();
            });
        }
    </script>
</body>
</html>
