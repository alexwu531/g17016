﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AFRCAdmin.SiteMgt.Default" %>

<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script src="../Scripts/ckeditor/ckeditor.js"></script>
  
    <asp:TextBox ID="txtContr" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
    <script>

        CKEDITOR.replace('txtContr',
{
    filebrowserBrowseUrl: '/Scripts/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl: '/Scripts/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl: '/Scripts/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl: '/Scripts/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl: '/Scripts/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl: '/Scripts/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash'
});
    </script>
</asp:Content>
