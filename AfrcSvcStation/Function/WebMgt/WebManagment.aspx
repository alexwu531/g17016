﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebManagment.aspx.cs" Inherits="AFRCAdmin.WebMgt.WebManagment" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <%: Styles.Render("~/bundles/webmgtstyles") %>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hfSessionRoleID" ClientIDMode="Static" runat="server" />

    <div class="col-xs-12">
        <div class="page-header">
            <h1>選單管理
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    網站選單階層與內容維護
                                </small>
            </h1>
        </div>
        <!-- div.table-responsive -->

        <!-- div.dataTables_borderWrap -->

        <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
            <div class="row">
                <div class="col-xs-6">
                    <div class="dataTables_length" id="dynamic-table_length">
                        <asp:DropDownList ID="ddlMenuList" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlMenuList_SelectedIndexChanged"></asp:DropDownList>

                        <a href="javascript:return false;" class="btn btn-xs btn-info" onclick="addnewrow(this,'dynamic-table');" id="cmdadd_0">新增主選單</a>
                    </div>
                </div>


            </div>
            <table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="dynamic-table_info">
                <thead>
                    <tr>
                        <th align="center">選單ID</th>
                        <th align="center">選單名稱</th>
                        <th align="center">刊登狀態</th>
                        <th align="center">新增下層選單</th>
                        <th align="center">刪除選單</th>
                    </tr>
                </thead>

                <tbody>
                    <asp:Repeater ID="repMenuList" runat="server" OnItemDataBound="repMenuList_ItemDataBound" OnItemCommand="repMenuList_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <%#Eval("MenuID") %>
                                    <asp:HiddenField ID="hfMenuID" runat="server" Value='<%#Eval("MenuID")%>' />
                                </td>
                                <td><a href='<%#ResolveUrl("ContentManagment") + "?MenuID="+Eval("MenuID") + "&MenuType=" + Eval("MenuType")+"&WebID="+GetWebid()+"&ismenu=1"%>' class="iframe"><%#LayerCheck(Convert.ToInt32(Eval("MenuID")))%><%#Eval("MenuName")%></a></td>

                                <td align="center"><%#LayerStatus(Eval("Publish"))%></td>


                                <td class="center">

                                    <a href="javascript:return false;" onclick="addnewrow(this,'dynamic-table');" class="btn btn-xs btn-info" id='<%#"cmdadd_"+Eval("MenuID")%>'>+
                                    </a>
                                </td>
                                <td class="center">
                                    <asp:Button ID="cmddel" runat="server" CommandName="Del" CommandArgument='<%#Eval("MenuID") %>' CssClass="btn btn_del" Text="刪除" OnClientClick='<%#"return  ChkChild("+Eval("MenuID")+");" %>' ></asp:Button>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>


        </div>
    </div>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
    <%: Scripts.Render("~/bundles/webmgtjs") %>

    <script type="text/javascript" charset="utf-8">
        var UserGroup = document.getElementById("hfSessionRoleID");
        var btnaddrow = document.getElementById("cmdadd_0");

        if (UserGroup.value != "1") {
            btnaddrow.style.display = "none";
        }
    </script>
    <script>
        function ChkChild(va)
        {
            var rt = gotoCheck(va);
            return rt;
        }



        function gotoCheck(obj)
        {
            var rt = false;
        
            var formData = JSON.stringify({ mid: obj });
            $.ajax({
                type: "POST",
                url: 'WebManagment.aspx/CheckChild',
                data: formData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,

                success: function (msg) {
                    var msgobj = msg.d[0];
                    if (msgobj.length != 0) {
                        if (msgobj.HasChild == "1")
                        {
                            rt =confirm('已經有子階層是否繼續刪除?');
                        }
                        else {
                            rt = confirm('繼續刪除?');
                        }
                
                    }
                    else {
                        rt = confirm('繼續刪除?');
                    }

                }
            });

            return rt;
        }
  
    </script>
    <script>
        $(document).ready(function () {

            $(".iframe").colorbox({
                iframe: true, width: "100%", height: "100%"
            });

            //$(".callbacks").colorbox({
            //    onOpen: function () { alert('onOpen: colorbox is about to open'); },
            //    onLoad: function () { alert('onLoad: colorbox has started to load the targeted content'); },
            //    onComplete: function () { alert('onComplete: colorbox has displayed the loaded content'); },
            //    onCleanup: function () { alert('onCleanup: colorbox has begun the close process'); },
            //    onClosed: function () { alert('onClosed: colorbox has completely closed'); }
            //});

        });

        function CloseFrame() {
            $.colorbox.close();
        }

    </script>

</asp:Content>




