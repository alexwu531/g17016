﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListContentManagment.aspx.cs" Inherits="AFRCAdmin.WebMgt.ListContentManagment" ValidateRequest="false" %>

<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>
<%@ Register Src="~/WebUserControl/WucSimpleEdit.ascx" TagPrefix="uc1" TagName="WucSimpleEdit" %>
<%@ Register Src="~/WebUserControl/WucDate.ascx" TagPrefix="uc1" TagName="WucDate" %>
<%@ Register Src="~/WebUserControl/WucMetaDataControl.ascx" TagPrefix="uc1" TagName="WucMetaDataControl" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>後備指揮部後台</title>
    <%: Styles.Render("~/bundles/masterstyles") %>
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/bootstrap-multiselect.css" type="text/css"/>

    <style type="text/css">
        #rdblstMenuName tr td {
            padding-right: 10px;
        }
    </style>
</head>

<body>
    <form id="form1" runat="server" class="form-horizontal">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:HiddenField ID="hfWebID" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hfMenuID" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hfMenuType" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hfListID" runat="server" />
        <asp:HiddenField ID="hfType" runat="server" />
        <asp:HiddenField ID="hfIsMenu" ClientIDMode="Static" runat="server" />

        <asp:HiddenField ID="hfThemeValue" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hfCakeValue" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hfServiceValue" ClientIDMode="Static" runat="server" />
        <div class="main-container ace-save-state" id="main-container">


            <div class="main-content">
                <div class="main-content-inner">

                    <div class="page-content">


                        <div class="page-header">
                            <h1>選單編輯
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    編輯選單屬性與內容樣式
                                </small>
                            </h1>
                        </div>
                        <!-- /.page-header -->

                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">選單名稱</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtMenuName" runat="server" CssClass="col-xs-10 col-sm-5" placeholder=""></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">選單類型</label>

                                    <div class="col-sm-9">
                                        <div class="widget-box">
                                            <div class="widget-header">
                                                <h4 class="widget-title">選單類型</h4>

                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div>
                                                        <div class="control-group row">

                                                            <div class="radio col-sm-3">
                                                                <i class="fa fa-pencil-square-o ace-icon green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio" id="rdbMenuType1" type="radio" class="ace" value="1" runat="server">
                                                                    <span class="lbl">HTML編輯</span>
                                                                </label>
                                                            </div>

                                                            <%--                                                            <div class="radio col-sm-3">
                                                                <i class="fa fa-pencil-square-o ace-icon green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio" id="rdbMenuType7" type="radio" class="ace" value="7" runat="server">
                                                                    <span class="lbl">簡易編輯</span>
                                                                </label>
                                                            </div>--%>

                                                            <div class="radio col-sm-3">
                                                                <i class="fa fa-link green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio" id="rdbMenuType2" type="radio" class="ace" value="2" runat="server">
                                                                    <span class="lbl">自訂連結</span>
                                                                </label>
                                                            </div>
                                                            <%-- <div class="radio col-sm-3">
                                                                <i class="fa fa-list-ul green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio" id="rdbMenuType5" type="radio" class="ace" value="5" runat="server">
                                                                    <span class="lbl">自訂列表</span>
                                                                </label>
                                                            </div>
                                                            <div class="radio col-sm-3">
                                                                <i class="fa fa-list-ul green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio" id="rdbMenuType9" type="radio" class="ace" value="9" runat="server">
                                                                    <span class="lbl">自訂單元</span>
                                                                </label>
                                                            </div>
                                                            <div class="radio col-sm-3">
                                                                <i class="fa fa-cloud-download green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio" id="rdbMenuType6" type="radio" class="ace" value="6" runat="server">
                                                                    <span class="lbl">單檔下載</span>
                                                                </label>
                                                            </div>
                                                            <div class="radio col-sm-3">
                                                                <i class="fa fa-picture-o green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio" id="rdbMenuType8" type="radio" class="ace" value="8" runat="server">
                                                                    <span class="lbl">多圖分頁</span>
                                                                </label>
                                                            </div>
                                                            <div class="radio col-sm-3">
                                                                <i class="fa fa-columns green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio" id="rdbMenuType2" type="radio" class="ace" value="2" runat="server">
                                                                    <span class="lbl">等同下層第一個選單</span>
                                                                </label>
                                                            </div>
                                                            <div class="radio col-sm-3">
                                                                <i class="fa fa-columns green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio" id="rdbMenuType1" type="radio" class="ace" value="1" runat="server">
                                                                    <span class="lbl">同某選單</span>
                                                                </label>
                                                            </div>--%>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group" id="content1" style="display: none" runat="server">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">內容</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtContr" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group" id="content7" style="display: none" runat="server">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">簡易編輯</label>


                                    <div class="col-xs-12 col-sm-9 widget-container-col ui-sortable" id="widget-container-col-2">
                                        <div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-2">


                                            <asp:UpdatePanel ID="up1" runat="server">
                                                <ContentTemplate>

                                                    <div class="dataTables_wrapper form-inline no-footer">


                                                        <table id="simple-table" class="table  table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>段落內容</th>
                                                                    <th>上傳圖片預覽</th>
                                                                    <th>編輯</th>
                                                                    <th>刪除</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <asp:Repeater ID="repEasyContent" runat="server" OnItemDataBound="repEasyContent_ItemDataBound" OnItemCommand="repEasyContent_ItemCommand">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:HiddenField ID="hfEzcontentID" runat="server" Value='<%#Eval("ID")%>' />
                                                                                <uc1:WucSimpleEdit runat="server" ID="WucSimpleEdit" />
                                                                            </td>

                                                                            <td></td>

                                                                            <td class="center">
                                                                                <asp:Button ID="btnEdit" runat="server" Text="編輯" CssClass="btn btn-xs btn-info" CausesValidation="False" CommandName="Edit" CommandArgument='<%#Eval("ID")%>' OnCommand="btnEdit_Command" />
                                                                            </td>

                                                                            <td>
                                                                                <asp:Button ID="btnDelete" runat="server" Text="刪除" CausesValidation="False" CommandName="Delete" CssClass="btn btn-xs btn-danger" CommandArgument='<%#Eval("ID")%>' OnCommand="btnDelete_Command" OnClientClick="return confirm('確認刪除?')" />
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>

                                                                </asp:Repeater>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- PAGE CONTENT ENDS -->
                                                    </div>
    <!-- /.col -->




                                                    <%--Layer2Start     <div class="widget-main">--%>
                                                    <div class="col-sm-6">

                                                        <div class="widget-box widget-color-blue">
                                                            <div class="widget-header widget-header-small"></div>

                                                            <div class="widget-body">
                                                                <div class="widget-main no-padding">
                                                                    <div class="md-editor active" id="1476873027631">

                                                                        <textarea name="content" id="txtEzContent" data-provide="markdown" data-iconlibrary="fa" rows="10" class="md-input" style="resize: none;" runat="server"></textarea>
                                                                        <div class="md-fullscreen-controls"><a href="#" class="exit-fullscreen" title="Exit fullscreen"><span class="fa fa-compress"></span></a></div>
                                                                        <div class="md-fullscreen-controls"><a href="#" class="exit-fullscreen" title="Exit fullscreen"><span class="fa fa-compress"></span></a></div>
                                                                    </div>
                                                                </div>

                                                                <div class="widget-toolbox padding-4 clearfix">
                                                                    <div class="btn-group pull-right">
                                                                        <asp:Button ID="btnAddEzContent" runat="server" Text="新增段落" CssClass="btn btn-sm btn-purple" OnClick="btnAddEzContent_Click" />
                                                                        <%--        <%--<button class="btn btn-sm btn-purple" runat="server">
                                                                    <i class="ace-icon fa fa-floppy-o bigger-125"></i>
                                                                    新增段落
                                                                </button>--%>
                                                                    </div>

                                                                    <div class="btn-group pull-right">
                                                                        <button class="btn btn-sm btn-purple">
                                                                            <%--<i class="ace-icon fa fa-floppy-o bigger-125"></i>--%>
                                                                上傳圖片
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%--Layer2End  </div>--%>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="content2" style="display: none" runat="server">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">自訂連結</label>

                                    <div class="widget-main">

                                        <div class="col-sm-5">
                                            <label for="form-field-9">請輸入網址，或連結位置：</label>
                                            <input type="text" class="form-control limited" id="txtMenuUrl" runat="server"></input>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="content6_old" style="display: none" runat="server">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">單檔下載</label>

                                    <%--  <div class="col-sm-9">
                                        <div class="widget-box" id="file-widget-box" style="max-width: 400px">
                                            <div class="widget-header">
                                                <h4 class="widget-title">檔案上傳</h4>
                                                <div class="widget-toolbar">
                                                    <a href="javascript:" id="btnaddfile">
                                                        <i class="ace-icon fa fa-plus-circle green"></i>
                                                    </a>
                                                    <a href="javascript:" id="btnupfile">
                                                        <i class="fa fa-upload ace-icon blue"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div>
                                                        <div class="control-group row" id="uploaddiv">
                                                            <div class="col-sm-12">
                                                                <input type="file" id="id-input-file-2">
                                                            </div>
                                                        </div>

                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>


                                    <div class="col-sm-9">
                                        <div class="col-sm-4">
                                            <label class="ace-file-input">
                                                <input type="file" id="id-input-file-1"><span class="ace-file-container" data-title="選擇"><span class="ace-file-name" data-title="未選取檔案..."><i class=" ace-icon fa fa-upload"></i></span></span><a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="content1_old" style="display: none" runat="server">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">同某選單</label>

                                    <div class="col-sm-9">
                                        <%--                                        <div class="control-group">
                                            <asp:Literal ID="ltrMenuList" runat="server"></asp:Literal>
                                        </div>--%>
                                        <%--                                        <div class="dataTables_wrapper form-inline no-footer">
                                            <table id="simple-table" class="table  table-bordered table-hover">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="repUserView" runat="server" OnItemDataBound="repMenuList_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:RadioButton ID="rdbMenuName" runat="server" />
                                                                    <asp:HiddenField ID="hfMenuID" runat="server" Value='<%#Eval("MenuID")%>' />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblUseState" runat="server" Text='<%#Eval("UseState") %>'></asp:Label>
                                                                    <asp:HiddenField ID="HiddenField_UseState" runat="server" Value='<%#Eval("UseState")%>' />
                                                                </td>
                                                                <td class="center">
                                                                    <asp:Button ID="btnEdit" runat="server" Text="編輯" CssClass="btn btn-xs btn-info" CausesValidation="False" CommandName="Edit" CommandArgument='<%#Eval("ID")%>' OnCommand="btnEdit_Command" /></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                        </div>--%>

                                        <asp:RadioButtonList ID="rdblstMenuName" runat="server" RepeatColumns="8" RepeatDirection="Horizontal" CellPadding="10" CellSpacing="10"></asp:RadioButtonList>

                                    </div>
                                </div>

                                <div class="form-group" id="content_index" style="display: none" runat="server">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">分類檢索</label>

                                    <div class="col-sm-9">

                                        <a class="btn btn-pink btn-xs" onclick="ShowHideMetaDataContent()">
                                            <i class="ace-icon fa fa-search-plus bigger-200"></i>
                                            點擊縮放
                                        </a>

                                        <div id="MetaDataContent" style="display: none">
                                            <uc1:WucMetaDataControl runat="server" ID="WucMetaDataControl" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">相關連結</label>

                                    <div class="col-sm-9">
                                        <span class="input-icon">
                                            <asp:TextBox ID="txtlinkname" runat="server"></asp:TextBox>
                                            <i class="ace-icon fa fa-font blue"></i>
                                        </span>

                                        <span class="input-icon input-icon-right">
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                            <i class="ace-icon fa fa-link green"></i>
                                        </span>

                                        <button class="btn  btn-grey btn-xs radius-4">
                                            <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                                            新增									
                                        </button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">相關檔案</label>

                                    <div class="col-sm-9">
                                        <div class="widget-box" id="file-widget-box" style="max-width: 400px">
                                            <div class="widget-header">
                                                <h4 class="widget-title">檔案上傳</h4>
                                                <div class="widget-toolbar">
                                                    <a href="javascript:" id="btnaddfile">
                                                        <i class="ace-icon fa fa-plus-circle green"></i>
                                                    </a>
                                                    <a href="javascript:" id="btnupfile">
                                                        <i class="fa fa-upload ace-icon blue"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div>
                                                        <div class="control-group row" id="uploaddiv">
                                                            <div class="col-sm-12">
                                                                <input type="file" id="id-input-file-2">
                                                            </div>
                                                        </div>

                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">開啟方式</label>

                                    <div class="col-sm-9">
                                        <div class="widget-box">
                                            <div class="widget-header">
                                                <h4 class="widget-title">選單類型</h4>

                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div>
                                                        <div class="control-group row">

                                                            <div class="radio col-sm-6">
                                                                <i class="fa fa-pencil-square-o ace-icon green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio2" type="radio" class="ace" runat="server" id="rdbTarget3" value="3">
                                                                    <span class="lbl">本頁切換 ( 不含選單外框 )</span>
                                                                </label>
                                                            </div>

                                                            <div class="radio col-sm-6">
                                                                <i class="fa fa-pencil-square-o ace-icon green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio2" type="radio" class="ace" runat="server" id="rdbTarget4" value="4">
                                                                    <span class="lbl">另開視窗 ( 不含選單外框 )</span>
                                                                </label>
                                                            </div>

                                                            <div class="radio col-sm-6">
                                                                <i class="fa fa-link green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio2" type="radio" class="ace" runat="server" id="rdbTarget1" value="1">
                                                                    <span class="lbl">本頁切換 ( 包含選單外框 ) </span>
                                                                </label>
                                                            </div>
                                                            <div class="radio col-sm-6">
                                                                <i class="fa fa-list-ul green fa-lg"></i>
                                                                <label>
                                                                    <input name="form-field-radio2" type="radio" class="ace" runat="server" id="rdbTarget2" value="2">
                                                                    <span class="lbl">自訂列表</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">發布單位</label>
                                    <div class="col-xs-12 col-sm-3">
                                        <div>
                                            <asp:DropDownList ID="ddlSonDept" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">刊登</label>

                                        <div class="col-xs-12 col-sm-3">
                                            <div class="control-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="form-field-checkbox" id="chkPublic1" class="ace" type="checkbox" runat="server">
                                                        <span class="lbl">刊登</span>
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <label>
                                                        <input name="form-field-checkbox" id="chkPublic2" class="ace" type="checkbox" runat="server">
                                                        <span class="lbl">不刊登於主選單(階層一) </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <label class="col-sm-1 control-label no-padding-right" for="form-field-1">審核</label>
                                   <div class="col-xs-12 col-sm-3" runat="server" id="divAudit" style="display: none">
                                            <div class="control-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="form-field-checkbox" id="chkAudit" class="ace" type="checkbox" runat="server">
                                                        <span class="lbl">審核</span>
                                                    </label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-9 col-sm-offset-2">
                                        <div class="profile-user-info profile-user-info-striped">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name col-sm-3">創建人員</div>

                                                <div class="profile-info-value col-sm-4">
                                                    <asp:Label ID="lblCreateUser" runat="server" Text="Label">皓展</asp:Label>
                                                    <%--<span class="editable editable-click" id="CreateUser" runat ="server">皓展</span>--%>
                                                </div>

                                                <div class="profile-info-name col-sm-3">上次更新人員</div>

                                                <div class="profile-info-value col-sm-4">
                                                    <asp:Label ID="lblModifyUser" runat="server" Text="Label">系統維護員</asp:Label>
                                                    <%--<span class="editable editable-click" id="LastModifyUser">系統維護員</span>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-9 col-sm-offset-2">
                                        <div class="profile-user-info profile-user-info-striped">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name col-sm-3">創建時間</div>

                                                <div class="profile-info-value col-sm-4">
                                                    <asp:Label ID="lblCreateTime" runat="server" Text="Label">2014/6/26 上午 10:04:43</asp:Label>
                                                    <%--<span class="editable editable-click" id="username">2014/6/26 上午 10:04:43</span>--%>
                                                </div>

                                                <div class="profile-info-name col-sm-3">上次更新時間</div>

                                                <div class="profile-info-value col-sm-4">
                                                    <asp:Label ID="lblModifyTime" runat="server" Text="Label">2014/6/26 上午 10:04:43</asp:Label>
                                                    <%--<span class="editable editable-click" id="username">2014/6/26 上午 10:04:43</span>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group center">
                              <%--      <asp:Button ID="Button1" CssClass="btn btn-info" runat="server" Text="預覽" />&nbsp;&nbsp;&nbsp;--%>
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-info" runat="server" Text="儲存" OnClick="btnSubmit_Click" ClientIDMode="Static" />&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnCancel" CssClass="btn btn-info" runat="server" Text="取消" OnClick="btnCancel_Click" />
                                </div>

                                <div class="hr hr-18 dotted hr-double"></div>


                                <!-- PAGE CONTENT ENDS -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.page-content -->
                </div>
            </div>
            <!-- /.main-content -->




        </div>
        <!-- /.main-container -->
    </form>

    <!-- basic scripts -->
    <%: Scripts.Render("~/bundles/masterjs") %>
    <script src="../Scripts/ckeditor/ckeditor.js"></script>
    <script src="../js/multiple-fileupload.js"></script>
    <script src="../js/bootstrap-multiselect.min.js"></script>

    <script>

        CKEDITOR.replace('txtContr',
{
    height: '400px',
    filebrowserBrowseUrl: '../Scripts/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl: '../Scripts/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl: '../Scripts/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl: '../Scripts/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl: '../Scripts/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl: '../Scripts/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash'
});
    </script>

    <script type="text/javascript" charset="utf-8">
        jQuery(function () {
            var Files = new MultipleFileUpload('uploaddiv');
            Files.Init('id-input-file-2', 'btnaddfile', 'btnupfile', 'file-widget-box');
            jQuery(".datepicker").datepicker({ dateFormat: "yy-mm-dd" });
        });
    </script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('input:radio[name=form-field-radio]').change(function () {

                //var items = new Array("content1", "content3", "content6");
                var MenuID = document.getElementById("hfMenuID").value;
                var MenuType = document.getElementById("hfMenuType").value;
                var WebID = document.getElementById("hfWebID").value;
                var ListID = document.getElementById("hfListID").value;
                var Type = document.getElementById("hfType").value;
                //for (var i = 0; i < items.length; i++) {
                //    document.getElementById(items[i]).style.display = "none";
                //}

                if (this.value == '1') {//HTML編輯
                    window.location = "ListContentManagment?MenuID=" + MenuID + "&MenuType=" + MenuType + "&WebID=" + WebID + "&ListID=" + ListID + "&Type=1" + "&ismenu=1";
                    //HidCheck(1);
                    //document.getElementById("content1").style.display = "block";
                }
                //if (this.value == '7') {
                //    window.location = "ListContentManagment?MenuID=" + MenuID + "&MenuType=7&ListID=" + ListID + "&Type=" + Type;
                //    //HidCheck(1);
                //    //document.getElementById("content1").style.display = "block";
                //}
                if (this.value == '2') {
                    window.location = "ListContentManagment?MenuID=" + MenuID + "&MenuType=" + MenuType + "&WebID=" + WebID + "&ListID=" + ListID + "&Type=2" + "&ismenu=1";
                    //document.getElementById("content3").style.display = "block";
                }
                //if (this.value == '6') {
                //    window.location = "ListContentManagment?MenuID=" + MenuID + "&MenuType=6&ListID=" + ListID + "&Type=" + Type;
                //    //document.getElementById("content6").style.display = "block";
                //}
                //if (this.value == '111111') {
                //    window.location = "ListContentManagment?MenuID=" + MenuID + "&MenuType=1&ListID=" + ListID + "&Type=" + Type;
                //    //document.getElementById("content6").style.display = "block";
                //}
            })

            //get dropdownlist
            $('.multiselect').multiselect({
                enableFiltering: true,
                enableHTML: true,
                buttonClass: 'btn btn-white btn-primary',
                templates: {
                    button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
                    ul: '<ul class="multiselect-container dropdown-menu"></ul>',
                    filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                    filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
                    li: '<li><a tabindex="0"><label></label></a></li>',
                    divider: '<li class="multiselect-item divider"></li>',
                    liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
                },

            });

            //set dropdownlist_theme
            var theme = document.getElementById("hfThemeValue").value;
            var str1 = theme.split(",");
            $('#ddlMetaTheme').multiselect('select', str1);

            var cake = document.getElementById("hfCakeValue").value;
            var str2 = cake.split(",");
            $('#ddlMetaCake').multiselect('select', str2);

            var service = document.getElementById("hfServiceValue").value;
            var str3 = service.split(",");
            $('#ddlMetaService').multiselect('select', str3);
        });
    </script>

        <script type="text/javascript">
        function ShowHideMetaDataContent() {
            if (document.getElementById("MetaDataContent").style.display == "block") {
                document.getElementById("MetaDataContent").style.display = "none";
            }
            else {
                document.getElementById("MetaDataContent").style.display = "block";
            }
        }
    </script>

    <script type="text/javascript">
    $('#btnSubmit').click(function () {

        var selected1 = $("#ddlMetaTheme option:selected");    
        var ThemeValue = "";
        var hfThemeValue = document.getElementById("hfThemeValue");
        selected1.each(function () {
            ThemeValue += $(this).val() + ",";
        });        
        hfThemeValue.value = ThemeValue;

        var selected2 = $("#ddlMetaCake option:selected");
        var CakeValue = "";
        var hfCakeValue = document.getElementById("hfCakeValue");
        selected2.each(function () {
            CakeValue += $(this).val() + ",";
        });
        hfCakeValue.value = CakeValue;

        var selected3 = $("#ddlMetaService option:selected");
        var ServiceValue = "";
        var hfServiceValue = document.getElementById("hfServiceValue");
        selected3.each(function () {
            ServiceValue += $(this).val() + ",";
        });
        hfServiceValue.value = ServiceValue;

        //alert("theme:" + ThemeValue + "cake:" + CakeValue + "service:" + ServiceValue);
    });
</script>
</body>
</html>
