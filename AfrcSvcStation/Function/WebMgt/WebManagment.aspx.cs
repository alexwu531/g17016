﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AFRCAdmin.Models;
using AFRCAdmin.App_Code;
using System.Data;
using System.Web.Services;
using System.IO;

namespace AFRCAdmin.WebMgt
{
    public partial class WebManagment : System.Web.UI.Page
    {
        WebMessage MsgBox = new WebMessage();
        protected UserInfo so = new UserInfo();
        protected string StrID = HttpContext.Current.Request.QueryString["StrID"];

        protected void Page_Load(object sender, EventArgs e)
        {
            so = (UserInfo)Session["UserData"];
            //UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            if (!IsPostBack)
            {
                try
                {
                    hfSessionRoleID.Value = so.UserGroup;
                    //VisitorLog.AddAction(so.UserID, VisitorLog.ActionType.pageview.ToString(), "系統管理>使用者管理");
                    BindMenuList();
                    BindData();
                }
                catch (Exception ex)
                {

                }



            }
        }

        protected string GetWebid()
        {
            return Session["WEBID"].ToString();
        }


        private void BindData()
        {


            AFRCEntities db = new AFRCEntities();
            try
            {
                int pmid = ddlMenuList.SelectedValue != "0" ? Convert.ToInt32(ddlMenuList.SelectedValue) : 0;
                int webid = (int)Session["WEBID"];
                var MenuList = db.T_Menu.Where(o => o.WebID == webid && ((o.ParentMenuID == pmid || o.MenuID == pmid ) || pmid == 0 || o.MenuLevel == 3)).ToList();
                List<T_Menu> Menu = new List<T_Menu>();
                GetMenuList(Menu, MenuList, 0);

                repMenuList.DataSource = Menu;
                repMenuList.DataBind();

            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Dispose();
            }


        }




        public void GetMenuList(List<T_Menu> Menulst, List<T_Menu> OldMenulst, int MenuId)
        {
            if (so.UserID == "admin")
            {
                OldMenulst.Where(o => o.ParentMenuID == MenuId).OrderBy(o => o.Sort).ToList().ForEach(x =>
                {
                    Menulst.Add(x);
                    if (OldMenulst.Where(o => o.ParentMenuID == x.MenuID).Any())
                    {
                        GetMenuList(Menulst, OldMenulst, x.MenuID);
                    }
                });
            }
            else
            {
                int DeptID = int.Parse(so.UserDept);
                OldMenulst.Where(o => o.ParentMenuID == MenuId && o.DepID == DeptID).OrderBy(o => o.Sort).ToList().ForEach(x =>
                {
                    Menulst.Add(x);
                    if (OldMenulst.Where(o => o.ParentMenuID == x.MenuID).Any())
                    {
                        GetMenuList(Menulst, OldMenulst, x.MenuID);
                    }
                });
            }
        }

        public string LayerCheck(int intMenuID)
        {
            string rtValue = "";

            try
            {
                using (AFRCEntities db = new AFRCEntities())
                {
                    T_Menu list = (from M in db.T_Menu
                                   where M.MenuID == intMenuID
                                   select M).FirstOrDefault();
                    if (list != null)
                    {
                        switch (list.MenuLevel)
                        {
                            case 1:
                                rtValue = "|_";
                                break;
                            case 2:
                                rtValue = "　　||__";
                                break;
                            case 3:
                                rtValue = "　　　　|||___";
                                break;
                            case 4:
                                rtValue = "　　　　       ||||___";
                                break;
                            case 5:
                                rtValue = "　　　　             |||||___";
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return rtValue;
        }


        public string LayerStatus(object status)
        {
            string rtValue = "";
            try
            {
                if (status != null)
                {
                    switch (status.ToString())
                    {
                        case "1":
                            rtValue = "<span class='label label-sm label-success'>刊登</span>";
                            break;
                        case "2":
                            rtValue = "<span class='label label-sm label-inverse'>不刊登</span>";
                            break;
                        case "3":
                            rtValue = "<span class='label label-sm label-warning'>不刊登於主選單</span>";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return rtValue;
        }

        /// <summary>
        /// 下拉選單單位資料綁定_使用者群組
        /// </summary>
        public void BindMenuList()
        {

            using (AFRCEntities db = new AFRCEntities())
            {
                try
                {
                    int webid = (int)Session["WEBID"];
                    int UserDept = int.Parse(so.UserDept);

                    List<T_Menu> Menu = new List<T_Menu>();
                    if (so.UserID == "admin")
                    {
                        Menu = (from M in db.T_Menu
                                where M.MenuLevel == 1 && M.WebID == webid
                                orderby M.Sort
                                select M).ToList();
                    }
                    else
                    {
                        //string strWebID = "";
                        //var Webid = db.T_Web.Where(w => w.Dept.Contains(so.UserDept)).ToList();
                        //foreach (var item in Webid)
                        //{
                        //    strWebID += item.WebID.ToString() + ",";
                        //}
                        //strWebID = strWebID.TrimEnd(',');
                        //string[] aryWebID = strWebID.Split(',');


                        Menu = (from M in db.T_Menu
                                where M.MenuLevel == 1 && M.WebID == webid && M.DepID == UserDept
                                orderby M.Sort
                                select M).ToList();
                    }

                    ddlMenuList.DataSource = Menu;
                    ddlMenuList.DataTextField = "MenuName";
                    ddlMenuList.DataValueField = "MenuID";
                    ddlMenuList.DataBind();

                    ddlMenuList.Items.Insert(0, new ListItem("請選擇主選單", "0"));
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void repMenuList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //HiddenField hfMenuID = (HiddenField)e.Item.FindControl("hfMenuID");
                //DropDownList ddlPublish = (DropDownList)e.Item.FindControl("ddlPublish");

                //ddlPublish.Items.Insert(0, new ListItem("顯示", "1"));
                //ddlPublish.Items.Insert(1, new ListItem("不顯示", "2"));
                //ddlPublish.Items.Insert(2, new ListItem("不顯示於主選單", "3"));

                //int MenuID = Convert.ToInt32(hfMenuID.Value);

                //try
                //{
                //    using (AFRCEntities db = new AFRCEntities())
                //    {
                //        var list = (from M in db.T_Menu
                //                    where M.MenuID == MenuID
                //                    select M).FirstOrDefault();

                //        if (list != null)
                //        {
                //            string Publish = list.Publish.ToString();
                //            ddlPublish.SelectedValue = Publish;
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{

                //}
            }
        }



        [WebMethod(EnableSession = true)]
        public static List<MenuModel> AddChild(string mid)
        {
            List<MenuModel> MenuModels = new List<MenuModel>();
            try
            {
                using (AFRCEntities db = new AFRCEntities())
                {
                    int _mid = int.Parse(mid);
                    int webid = (int)HttpContext.Current.Session["WEBID"];
                    UserInfo so = (UserInfo)HttpContext.Current.Session["UserData"];
                    if (so != null)
                    {
                        List<T_Menu> MenuList = db.T_Menu.ToList();
                        T_Menu menu = new T_Menu();
                        if (_mid == 0)
                        {
                            int MaxSort = 0;

                            if (MenuList.Where(m => m.ParentMenuID == _mid).Select(o => o.Sort).Any())
                            {
                                MaxSort = MenuList.Where(m => m.ParentMenuID == _mid).Select(o => o.Sort).ToArray().Max();
                            }

                            int NewSort = MaxSort + 1;
                            menu = new T_Menu
                            {
                                MenuName = "新選單",
                                ParentMenuID = _mid,
                                MenuLevel = 1,
                                CreateDate = DateTime.UtcNow.AddHours(8),
                                CreatePersonID = int.Parse(so.UserPKID),
                                EditDate = DateTime.UtcNow.AddHours(8),
                                EditPersonID = int.Parse(so.UserPKID),
                                Sort = 1,
                                Publish = 0,
                                Audit = 0,
                                WebID = webid,
                                Class = 1

                            };
                        }
                        else
                        {
                            int MaxSort = 0;
                            int ChildLevel = MenuList.Where(m => m.MenuID == _mid).FirstOrDefault().MenuLevel + 1;
                            if (MenuList.Where(m => m.ParentMenuID == _mid).Select(o => o.Sort).Any())
                            {
                                MaxSort = MenuList.Where(m => m.ParentMenuID == _mid).Select(o => o.Sort).ToArray().Max();
                            }

                            int NewSort = MaxSort + 1;
                            menu = new T_Menu
                            {
                                MenuName = "新選單",
                                ParentMenuID = _mid,
                                MenuLevel = ChildLevel,
                                CreateDate = DateTime.UtcNow.AddHours(8),
                                CreatePersonID = int.Parse(so.UserPKID),
                                EditDate = DateTime.UtcNow.AddHours(8),
                                EditPersonID = int.Parse(so.UserPKID),
                                Sort = NewSort,
                                Publish = 0,
                                Audit = 0,
                                WebID = webid

                            };
                        }


                        db.T_Menu.Add(menu);

                        db.SaveChanges();

                        string layer = "";

                        switch (menu.MenuLevel)
                        {
                            case 1:
                                layer = "|_";
                                break;
                            case 2:
                                layer = "　　||__";
                                break;
                            case 3:
                                layer = "　　　　|||___";
                                break;
                            case 4:
                                layer = "　　　　       |||___";
                                break;
                            case 5:
                                layer = "　　　　             |||___";
                                break;
                        }
                        string type = menu.MenuType.HasValue ? menu.MenuType.ToString() : "";
                        MenuModels.Add(new MenuModel { MenuID = menu.MenuID.ToString(), MenuLevel = menu.MenuLevel.ToString(), MenuNane = layer + menu.MenuName, MenuType = type, WebID = webid.ToString() });
                    }
                    else
                    {

                    }

                }
            }
            catch (Exception ex)
            {

            }

            return MenuModels;
        }



        [WebMethod(EnableSession = true)]
        public static List<DelObj> CheckChild(string mid)
        {
            List<DelObj> DelObjs = new List<DelObj>();
            try
            {
                bool hasany = false;
                int _mid = int.Parse(mid);
                using (AFRCEntities db = new AFRCEntities())
                {
                    hasany = db.T_Menu.Where(o => o.ParentMenuID == _mid).Any() || db.T_List.Where(o => o.ParentMenuID == _mid).Any();

                }
                if (hasany)
                {
                    DelObjs.Add(new DelObj {HasChild = "1" });
                }
                else
                {
                    DelObjs.Add(new DelObj { HasChild = "0" });
                }
            }
            catch
            {

            }

            return DelObjs;
        }





        protected void ddlMenuList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }


        protected void repMenuList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Del")
            {
                try
                {
                    int _mid = int.Parse(e.CommandArgument.ToString());
                    using (AFRCEntities db = new AFRCEntities())
                    {
                        T_Menu menu = db.T_Menu.Where(m => m.MenuID == _mid).FirstOrDefault();

                        db.T_Menu.Remove(menu);

                        db.SaveChanges();

                        MsgBox.MsgBox_In_Ajax("刪除完成", this.Page, Request.RawUrl);
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
    }

    public class DelObj
    {
        public string HasChild { get; set; }
    }
}