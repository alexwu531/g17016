﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AfrcSvcStation.Function.Account.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>後備指揮部登入畫面</title>
    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- css ---------------------------------------------------------------->
    <%:Styles.Render("~/bundles/loginstyles") %>

    <style>
        body, .blue {
            font-family: "微軟正黑體",Arial;
        }
    </style>
</head>

<body class="login-layout light-login">
    <div class="main-container">
        <div class="main-content">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="login-container">
                        <div class="center">
                            <img src="../images/logo.png" width="50%" alt="後備指揮部">
                            <h4 class="blue" id="id-company-text">後備指揮部網站後端管理平台</h4>
                        </div>

                        <div class="space-6"></div>

                        <div class="position-relative">
                            <div id="login-box" class="login-box visible widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header blue lighter bigger">
                                            <i class="ace-icon fa fa-coffee green"></i>
                                            請輸入帳號密碼
                                        </h4>

                                        <div class="space-6"></div>

                                     <form runat="server">
                                            <fieldset>
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" class="form-control" placeholder="請輸入帳號"  runat="server" id="txtacc"/>
                                                      
                                                        <i class="ace-icon fa fa-user"></i>
                                                    </span>
                                                </label>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="password" class="form-control" placeholder="請輸入密碼"  runat="server" id="txtpwd"/>
                                                        <i class="ace-icon fa fa-lock"></i>
                                                    </span>
                                                </label>

                                                <div class="space"></div>

                                                <div class="clearfix">
                                                  <%--  <label class="inline">
                                                        <input type="checkbox" class="ace" />
                                                        <span class="lbl">記住我的帳號</span>
                                                    </label>--%>
                                                    <asp:LinkButton ID="cmdLogin" runat="server" OnClick="cmdLogin_Click" CssClass="width-35 pull-right btn btn-sm btn-primary">
                                                            	<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">登入</span>
                                                    </asp:LinkButton>

                                                </div>

                                                <div class="space-4"></div>
                                            </fieldset>
                                        </form>





                                    </div>
                                    <!-- /.widget-main -->

                                  <%--  <div class="toolbar clearfix">
                                        <div>
                                            <a href="#" data-target="#forgot-box" class="forgot-password-link">
                                                <i class="ace-icon fa fa-arrow-left"></i>
                                                忘記密碼
                                            </a>
                                        </div>

                                    
                                    </div>--%>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.login-box -->

              
                            <!-- /.forgot-box -->

                       
                            <!-- /.signup-box -->
                        </div>
                        <!-- /.position-relative -->


                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.main-content -->
    </div>
    <!-- /.main-container -->

    <!-- basic scripts -->

    <!--[if !IE]> -->
    <%--<script src="~/js/jquery-2.1.4.min.js"></script>--%>

    <!-- <![endif]-->
    <!------------------ BundleJavascript ------------------>
    <%: Scripts.Render("~/bundles/loginjs") %>
    <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->

    <!-- inline scripts related to this page -->

</body>
</html>
