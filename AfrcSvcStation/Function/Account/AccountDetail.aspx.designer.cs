﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     變更這個檔案可能會導致不正確的行為，而且如果已重新產生
//     程式碼，則會遺失變更。
// </自動產生的>
//------------------------------------------------------------------------------

namespace AfrcSvcStation.Function.Account {
    
    
    public partial class AccountAddEdit {
        
        /// <summary>
        /// txtUserID 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtUserID;
        
        /// <summary>
        /// txtUserName 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtUserName;
        
        /// <summary>
        /// ddlUserGroup 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlUserGroup;
        
        /// <summary>
        /// txtPSW 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtPSW;
        
        /// <summary>
        /// txtCHKPSW 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtCHKPSW;
        
        /// <summary>
        /// ddlUserDept 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlUserDept;
        
        /// <summary>
        /// ddlUseState 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlUseState;
        
        /// <summary>
        /// txtEmail 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtEmail;
        
        /// <summary>
        /// txtPhone 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtPhone;
    }
}
