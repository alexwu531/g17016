﻿using AfrcSvcStation.App_Code;
using AfrcSvcStation.ModelsForStation;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.Function.Account
{
    public partial class GroupEdit : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        WebMessage Msg = new WebMessage();
        UserInfo so = new UserInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DataForBind();
            }
        }
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataForBind();
        }
        /// <summary>
        /// 資料繫節
        /// </summary>
        private void DataForBind()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {

                    var lst = db.T_SysGroup.Where(o => o.Del == 1);

                    repGroupsView.DataSource = lst.OrderBy(o => o.Sort).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize).Take(AspNetPager1.PageSize).ToList();
                    AspNetPager1.AlwaysShow = true;
                    AspNetPager1.RecordCount = lst.Count();
                    repGroupsView.DataBind();

                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(lst.ToString());
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }



        }

        /// <summary>
        /// 新增群組
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdsave_Click(object sender, EventArgs e)
        {
            try
            {
                UserInfo so = (UserInfo)Session["UserData"];


                string GroupName = txtgroupname.Text;
                txtgroupname.Text = "";

                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {

                    //新增
                    T_SysGroup group = new T_SysGroup
                    {
                        GroupName = GroupName,
                        Status = 1,
                        Del = 1,
                        Sort = 1,
                        CreateTime = DateTime.UtcNow.AddHours(8),
                        CreateUser = Convert.ToInt32(so.UserPKID)

                    };
                    db.T_SysGroup.Add(group);

                    db.SaveChanges();
                    DataForBind();
                    Msg.MsgBox_In_Ajax("新增完成", this.Page);

                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Insert.ToString());
                    logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_SysGroup", group.GroupID));
                    db.Dispose();
                    #endregion

                }
            }
            catch (Exception ex)
            {

                txtgroupname.Text = "";

                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Insert.ToString());
                logger.Error(ex);
                #endregion
            }
        }

        protected void repGroupsView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                try
                {
                    UserInfo so = (UserInfo)Session["UserData"];
                    using (AFRCDutyEntities db = new AFRCDutyEntities())
                    {
                        TextBox txtgname = e.Item.FindControl("txtgroupname") as TextBox;
                        int gid = int.Parse(e.CommandArgument.ToString());
                        //修改
                        T_SysGroup group = db.T_SysGroup.Where(u => u.GroupID == gid).FirstOrDefault();

                        group.GroupName = txtgname.Text;
                        group.ModifyTime = DateTime.UtcNow.AddHours(8);
                        group.ModifyUser = int.Parse(so.UserPKID);
                        db.SaveChanges();
                        Msg.MsgBox_In_Ajax("修改完成", this.Page);

                        #region ActionLog
                        so = (UserInfo)Session["UserData"];
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                        logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_SysGroup", group.GroupID));
                        db.Dispose();
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                    logger.Error(ex);
                    #endregion
                }

            }

            if (e.CommandName == "Del")
            {
                try
                {
                    UserInfo so = (UserInfo)Session["UserData"];
                    using (AFRCDutyEntities db = new AFRCDutyEntities())
                    {
                        TextBox txtgname = e.Item.FindControl("txtgroupname") as TextBox;
                        int gid = int.Parse(e.CommandArgument.ToString());
                        //修改
                        T_SysGroup group = db.T_SysGroup.Where(u => u.GroupID == gid).FirstOrDefault();

                        group.Del = 2;
                        group.ModifyTime = DateTime.UtcNow.AddHours(8);
                        group.ModifyUser = int.Parse(so.UserPKID);
                        db.SaveChanges();
                        DataForBind();
                        Msg.MsgBox_In_Ajax("刪除完成", this.Page);

                        #region ActionLog
                        so = (UserInfo)Session["UserData"];
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                        logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_SysGroup", group.GroupID));
                        db.Dispose();
                        #endregion
                    }
                }
                catch (Exception ex)
                {

                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                    logger.Error(ex);
                    #endregion
                }

            }
        }




        [WebMethod(EnableSession = true)]
        public static string SetGeoupStatu(string GroupId, string Status)
        {
            string rt = "";
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    int gid = int.Parse(GroupId);

                    UserInfo so = (UserInfo)HttpContext.Current.Session["UserData"];
                    if (so != null)
                    {
                        //修改
                        T_SysGroup group = db.T_SysGroup.Where(u => u.GroupID == gid).FirstOrDefault();

                        int _status = int.Parse(Status);

                        group.Status = _status;
                        group.ModifyUser = int.Parse(so.UserPKID);
                        group.ModifyTime = DateTime.UtcNow.AddHours(8);

                        db.SaveChanges();
                        rt = "更新完成";

                    }
                    else
                    {
                        rt = "更新失敗";
                    }

                }
            }
            catch (Exception ex)
            {
                rt = "更新失敗";
            }

            return rt;
        }
    }
}