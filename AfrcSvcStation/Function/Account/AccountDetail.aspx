﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AccountDetail.aspx.cs" Inherits="AfrcSvcStation.Function.Account.AccountAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="page-header">
                <h1>帳戶編輯
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    帳戶編輯
                                </small>
                </h1>
            </div>

            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">使用者帳號</label>

                    <div class="col-sm-9">
                        <%--<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="txtUserID" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">使用者名稱</label>

                    <div class="col-sm-9">
<%--                        <input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="txtUserName" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">使用者群組</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlUserGroup" runat="server" Style="width: 30%;">
                            <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                            <asp:ListItem Value="1">使用中</asp:ListItem>
                            <asp:ListItem Value="2">停用</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2">密碼</label>

                    <div class="col-sm-9">
                        <%--<input type="password" id="form-field-2" placeholder="" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="txtPSW" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2">確認密碼</label>

                    <div class="col-sm-9">
                        <%--<input type="password" id="form-field-2" placeholder="" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="txtCHKPSW" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">部門</label>
                    <div class="col-sm-9">
                    <asp:DropDownList ID="ddlUserDept" runat="server" CssClass="form-control" Style="width: 30%;">
                        <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                        <asp:ListItem Value="1">使用中</asp:ListItem>
                        <asp:ListItem Value="2">停用</asp:ListItem>
                    </asp:DropDownList>
                    </div>
                </div>
                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">帳號使用狀態</label>
                    <div class="col-sm-9">
                    <asp:DropDownList ID="ddlUseState" runat="server" CssClass="form-control" Style="width: 30%;">
                        <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                        <asp:ListItem Value="1">使用中</asp:ListItem>
                        <asp:ListItem Value="2">停用</asp:ListItem>
                    </asp:DropDownList>
                    </div>
                </div>
                <div class="space-4"></div>


                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2">電子郵件</label>

                    <div class="col-sm-9">
                        <%--<input type="password" id="form-field-2" placeholder="" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="txtEmail" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server" MaxLength="60"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2">聯絡電話</label>

                    <div class="col-sm-9">
                        <%--<input type="password" id="form-field-2" placeholder="" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="txtPhone" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server" MaxLength="20"></asp:TextBox>
                    </div>
                </div>

                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        											<button runat="server" class="btn" type="reset" onserverclick="Unnamed_ServerClick1">
                                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                                返回
                                            </button>
                        &nbsp; &nbsp; &nbsp;
                                                <button runat="server" class="btn btn-info" type="button" onserverclick="Unnamed_ServerClick">
                            <i class="ace-icon fa fa-check bigger-110"></i>
                            送出</button>
                    </div>
                </div>
            </div>

            <div class="hr hr-24"></div>
    </div>
        </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
</asp:Content>
