﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;
using NLog;

namespace AfrcSvcStation.Function.Account
{
    public partial class GroupCompentenceEdit : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        WebMessage Msgbox = new WebMessage();
        protected string strID = HttpContext.Current.Request.QueryString["StrID"];
        protected UserInfo so = new UserInfo();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    so = (UserInfo)Session["UserData"];


                }
                catch (Exception ex)
                {
                    //Logger.ErrorException("Log紀錄", ex);
                }

                try
                {
                    if (!string.IsNullOrEmpty(strID))
                    {

                        AFRCDutyEntities db = new AFRCDutyEntities();
                        var dt = (from _dt in db.T_SysGroup
                                  where _dt.Del == 1 && _dt.GroupID.ToString() == strID
                                  select _dt).SingleOrDefault();
                        if (dt != null)
                        {
                            TxtGroupName.Text = dt.GroupName;
                        }
                        else
                        {
                            Msgbox.MsgBox_In_Ajax("查無群組權限資料", this.Page, "GroupPowerEdit.aspx");
                        }

                        #region ActionLog
                        so = (UserInfo)Session["UserData"];
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Info(dt.ToString());
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Error(ex);
                    #endregion
                }

                DataForBind();
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataForBind();
        }

        /// <summary>
        /// 資料繫節
        /// </summary>
        private void DataForBind()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    //var MenuList = db.T_SysMenu.ToList();
                    var MenuList = db.T_SysMenu.Where(c => c.MenuLevel == 1).OrderBy(o => o.Sort).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize).Take(AspNetPager1.PageSize).ToList();

                    repGroupsView.DataSource = MenuList;
                    AspNetPager1.AlwaysShow = true;
                    AspNetPager1.RecordCount = MenuList.Count();
                    repGroupsView.DataBind();

                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(MenuList.ToString());
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }
        }

        protected void repGroupsView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //T_SysMenu drv = (T_SysMenu)e.Item.DataItem;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox ChkBox = (CheckBox)e.Item.FindControl("CheckBox1");
                CheckBoxList ChkList = (CheckBoxList)e.Item.FindControl("ChkPowerItem");
                HiddenField hidMainMenuCode = (HiddenField)e.Item.FindControl("hidMainMenuCode");

                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    try
                    {
                        var ChkListSource2 = from M in db.T_SysMenu
                                             where M.ParentMenuID.ToString() == hidMainMenuCode.Value
                                             orderby M.Sort
                                             select M;
                        ChkList.DataSource = ChkListSource2.ToList();
                        ChkList.DataTextField = "MenuName";
                        ChkList.DataValueField = "MenuID";
                        ChkList.DataBind();

                        #region ActionLog
                        so = (UserInfo)Session["UserData"];
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Info(ChkListSource2.ToString());
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        #region ActionLog
                        so = (UserInfo)Session["UserData"];
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Error(ex);
                        #endregion
                    }
                }


                try
                {
                    if (!string.IsNullOrEmpty(strID))
                    {
                        using (AFRCDutyEntities db = new AFRCDutyEntities())
                        {
                            var dt = (from _dt in db.T_SysGroup
                                      where _dt.Del == 1 && _dt.GroupID.ToString() == strID
                                      select _dt).SingleOrDefault();

                            if (dt != null)
                            {
                                string[] tempPower = dt.FunctionID.Split(',');
                                //Father
                                foreach (string strMain in tempPower)
                                {
                                    if (strMain == hidMainMenuCode.Value)
                                        ChkBox.Checked = true;
                                }
                                //Son
                                for (int i = 0; i < ChkList.Items.Count; i++)
                                {
                                    foreach (string str in tempPower)
                                    {
                                        if (str == ChkList.Items[i].Value)
                                        {
                                            ChkList.Items[i].Selected = true;
                                        }

                                    }
                                }
                            }

                            #region ActionLog
                            so = (UserInfo)Session["UserData"];
                            string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                            //取得IP
                            if (Request.ServerVariables["HTTP_VIA"] != null)
                            {
                                tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                            }
                            GlobalDiagnosticsContext.Set("addr", tClientIP);
                            GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                            GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                            logger.Info(dt.ToString());
                            #endregion
                        }
                    }
                }
                catch (Exception ex)
                {
                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Error(ex);
                    #endregion
                }
            }
        }


        /// <summary>
        /// 新增或編輯權限事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            string PowerID = "";
            for (int i = 0; i < repGroupsView.Items.Count; i++)
            {
                CheckBoxList ChkList = (CheckBoxList)repGroupsView.Items[i].FindControl("ChkPowerItem");
                CheckBox ChkBox = (CheckBox)repGroupsView.Items[i].FindControl("CheckBox1");
                HiddenField hidMainMenuCode = (HiddenField)repGroupsView.Items[i].FindControl("hidMainMenuCode");
                if (ChkBox.Checked == true)
                {
                    PowerID += (PowerID == "") ? hidMainMenuCode.Value : "," + hidMainMenuCode.Value;
                }
                for (int j = 0; j < ChkList.Items.Count; j++)
                {

                    if (ChkList.Items[j].Selected == true)
                        PowerID += (PowerID == "") ? ChkList.Items[j].Value : "," + ChkList.Items[j].Value;
                }
            }

            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                T_SysGroup group = db.T_SysGroup.SingleOrDefault(u => u.GroupID.ToString() == strID);

                //group.GroupName = TxtGroupName.Text.Trim();
                group.FunctionID = PowerID;
                group.ModifyTime = DateTime.UtcNow.AddHours(8);
                group.ModifyUser = Convert.ToInt32(so.UserID);
                db.SaveChanges();
                try
                {
                    so = (UserInfo)Session["UserData"];
                    //VisitorLog.AddAction(so.UserID, VisitorLog.ActionType.edit.ToString(), "系統管理>群組權限管理");
                }
                catch (Exception ex)
                {
                    //Logger.ErrorException("Log紀錄", ex);
                }
                Msgbox.MsgBox_In_Ajax("編輯成功", this.Page, "GroupEdit.aspx");

                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_SysGroup", group.GroupID));
                db.Dispose();
                #endregion
            }
            catch (Exception ex)
            {
                //Logger.ErrorException("編輯群組", ex);
                //Msgbox.MsgBox(Resources.CommonRes.UpdateError, this.Page, "gmbgrma.aspx");

                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                logger.Error(ex);
                db.Dispose();
                #endregion
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("GroupEdit.aspx");
        }
    }
}