﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;
using NLog;

namespace AfrcSvcStation.Function.Account
{
    public partial class AccountMain : System.Web.UI.Page
    {
        WebMessage Msgbox = new WebMessage();
        protected string UserID = HttpContext.Current.Request.QueryString["UserID"];
        protected UserInfo so = new UserInfo();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            so = (UserInfo)Session["UserData"];
            if (!IsPostBack)
            {
                DataForBind();
            }
        }

        private void DataForBind()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {

                    var UserList =
                         db.T_SysUser.Where(
                             c => txtUserID.Text != "" ? c.UserID.Contains(txtUserID.Text) : true &&
                                  txtUserName.Text != "" ? c.UserName.Contains(txtUserName.Text) : true &&
                                  ddlUseState.SelectedValue.ToString() != "0" ? c.UseState == ddlUseState.SelectedValue.ToString() : true &&
                                  c.UseState == "1"
                                  ).ToList();
                    var UserList_Query =
                     db.T_SysUser.Where(
                         c => txtUserID.Text != "" ? c.UserID.Contains(txtUserID.Text) : true &&
                              txtUserName.Text != "" ? c.UserName.Contains(txtUserName.Text) : true &&
                              ddlUseState.SelectedValue.ToString() != "0" ? c.UseState == ddlUseState.SelectedValue.ToString() : true &&
                              c.UseState == "1"
                              ).ToString();
                    AspNetPager1.AlwaysShow = true;
                    AspNetPager1.RecordCount = UserList.Count();

                    repUserView.DataSource = UserList.OrderBy(o => o.ID).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize)
                                             .Take(AspNetPager1.PageSize);
                    repUserView.DataBind();


                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(UserList_Query);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }
        }

        protected void repUserView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField HF_RoleID = (HiddenField)e.Item.FindControl("HiddenField_RoleID");
                HiddenField HF_UseState = (HiddenField)e.Item.FindControl("HiddenField_UseState");
                Label lblRoleID = (Label)e.Item.FindControl("lblRoleID");
                Label lblUseState = (Label)e.Item.FindControl("lblUseState");

                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    try
                    {
                        so = (UserInfo)Session["UserData"];

                        var Group = (from G in db.T_SysGroup
                                     where G.GroupID.ToString() == HF_RoleID.Value
                                     select G).SingleOrDefault();

                        lblRoleID.Text = Group.GroupName;

                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Info(Group.ToString());
                        db.Dispose();
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        lblRoleID.Text = "查無對應權限";

                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Info(ex);
                        #endregion
                    }

                    lblUseState.Text = HF_UseState.Value == "1" ? "是" : "否";
                }
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataForBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataForBind();
        }

        /// <summary>
        /// 刪除使用者
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                try
                {
                    AFRCDutyEntities db = new AFRCDutyEntities();

                    T_SysUser User = db.T_SysUser.FirstOrDefault(c => c.ID.ToString() == e.CommandArgument.ToString());
                    if (User != null)
                    {
                        User.UseState = "2";
                        User.ModifyUser = so.UserID;
                        db.SaveChanges();
                        Msgbox.MsgBox_In_Ajax("刪除完成", this.Page, Request.RawUrl);


                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                        logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_SysUser", User.ID));
                        db.Dispose();
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    Msgbox.MsgBox_In_Ajax("刪除失敗", this.Page, Request.RawUrl);

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                    logger.Error(ex);
                    #endregion
                }
            }
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("AccountDetail.aspx?StrID=" + e.CommandArgument.ToString());
        }

        protected void cmdadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AccountDetail");
        }
    }
}