﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GroupPowerEdit.aspx.cs" Inherits="AfrcSvcStation.Function.Account.GroupCompentenceEdit" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 24px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
            <h1>群組權限維護
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    群組使用權限設定
                                </small>
            </h1>
        </div>
        <div class="row col-xs-12 col-lg-6">

            <div class="dataTables_wrapper form-inline no-footer">
                <div class="control-group">
                    <label class="control-label">
                        群組名稱:</label>
                    <div class="controls">
                        <asp:TextBox ID="TxtGroupName" runat="server" Enabled="false" CssClass="span5"></asp:TextBox>&nbsp;&nbsp;<span
                            class="label label-warning"></span>
                    </div>
                </div>

                <table id="simple-table" class="table  table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>群組名稱</th>
                            <th>功能列表</th>
                        </tr>
                    </thead>

                    <tbody>
                        <asp:Repeater ID="repGroupsView" runat="server" OnItemDataBound="repGroupsView_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <th width="120" class="specalt" align="right">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Text='<%#Eval("MenuName") %>' />
                                        <asp:HiddenField ID="hidMainMenuCode" runat="server" Value='<%#Eval("MenuID") %>' />
                                    </th>
                                    <td>
                                        <asp:CheckBoxList ID="ChkPowerItem" runat="server" RepeatDirection="Horizontal" CssClass="chtb">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                                ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>



                <div class="form-actions">
                    <input id="Button2" type="button" name="Submit" value="返回" runat="server" class="btn" onserverclick="Cancel_Click" />
                    <input id="Button1" type="button" name="Submit" value="儲存" class="btn-success btn" runat="server" onserverclick="Save_Click" />
                </div>

            </div>


            <!-- PAGE CONTENT ENDS -->
        </div>
        <!-- /.col -->
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
</asp:Content>
