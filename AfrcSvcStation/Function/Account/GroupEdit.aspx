﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GroupEdit.aspx.cs" Inherits="AfrcSvcStation.Function.Account.GroupEdit" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="../css/jquery-ui.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
            <h1>群組維護
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    新增編輯群組名稱 & 群組使用權限設定
                                </small>
            </h1>
        </div>
        <div class="row col-xs-12 col-lg-6">
            <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="up1" runat="server">
                <ContentTemplate>

                    <div class="dataTables_wrapper form-inline no-footer">

                        <div class="row">

                            <asp:TextBox ID="txtgroupname" runat="server" placeholder="輸入群組名稱" CssClass="col-xs-5 col-sm-5"></asp:TextBox>
                            <asp:Button ID="cmdsave" runat="server" Text="新增" CssClass="btn btn-info btn-sm" OnClick="cmdsave_Click" OnClientClick="CloseDialog()" />

                        </div>


                        <table id="simple-table" class="table  table-bordered table-hover">
                            <thead>
                                <tr>

                                    <th>群組名稱</th>
                                    <th>狀態</th>
                                    <th>編輯</th>

                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repGroupsView" runat="server" OnItemCommand="repGroupsView_ItemCommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="center">

                                                <asp:TextBox ID="txtgroupname" runat="server" Text='<%#Eval("GroupName") %>'></asp:TextBox>
                                            </td>
                                            <td class="center">
                                                <label>
                                                    <input name="switch-field-1" class="ace ace-switch ace-switch-4 btn-rotate checkswitch" value='<%#Eval("GroupID").ToString()%>' type="checkbox" <%#Eval("Status").ToString()=="1" ? "checked":"" %>>
                                                    <span class="lbl" data-lbl="開&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;關"></span>
                                                </label>
                                            </td>
                                            <td class="center">

                                                <asp:LinkButton ID="lcmdedit" CommandName="Edit" CommandArgument='<%#Eval("GroupID").ToString()%>' CssClass="btn btn-xs btn-info" runat="server">    <i class="ace-icon fa fa-save bigger-120"></i></asp:LinkButton>
                                                <a class="btn btn-xs btn-warning" href='<%# ResolveUrl("GroupPowerEdit")+"?StrID="+Eval("GroupID").ToString() %>'><i class="ace-icon fa fa-key bigger-120"></i></a>
                                                <asp:LinkButton ID="LinkButton1" CommandName="Del" CommandArgument='<%#Eval("GroupID").ToString()%>' CssClass="btn btn-xs btn-danger" runat="server">   <i class="ace-icon fa fa-trash-o bigger-120"></i></asp:LinkButton>

                                            </td>
                                        </tr>
                                    </ItemTemplate>

                                </asp:Repeater>

                            </tbody>
                        </table>
                        <div class="row">

                            <div class="col-xs-12">
                                <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                                        ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                                    </webdiyer:AspNetPager>

                                </div>
                            </div>
                        </div>

                    </div>


                    <!-- PAGE CONTENT ENDS -->
                    </div>
    <!-- /.col -->




                </ContentTemplate>

            </asp:UpdatePanel>
        </div>
        <!-- #dialog-message -->
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
    <!-- page specific plugin scripts -->
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/jquery.ui.touch-punch.min.js"></script>
    <script>
        $(document).ready(function () {



            $('.checkswitch').change(function () {

                var status = '1';
                var gid = $(this).val();
                if ($(this).is(":checked")) {


                    status = '1';

                }
                else {
                    status = '2';
                }

                var formData = JSON.stringify({ GroupId: gid, Status: status });
                $.ajax({
                    type: "POST",
                    url: '<%= ResolveUrl("GroupEdit.aspx/SetGeoupStatu") %>',
                    data: formData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: "true",
                    cache: "false",

                    success: function (msg) {

                        alert(msg.d);

                    },
                    Error: function (x, e) {


                    }
                });


            });
        });
    </script>
</asp:Content>
