﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Optimization;
using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;
using NLog;
namespace AfrcSvcStation.Function.Account
{
    public partial class Login : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        WebMessage Msg = new WebMessage();
        UserInfo UserData = new UserInfo();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //第一次請求
                Session.Abandon();
                Session.Clear();
            }
            else
            {
                //postback

            }
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //檢查欄位空白
                string AccountStr = txtacc.Value;
                string PwdStr = txtpwd.Value;

                //登入驗證

                AFRCDutyEntities db = new AFRCDutyEntities();
                var Valid = db.IsValidLogin(AccountStr, PwdStr).FirstOrDefault();
                if (Valid != null)
                {
                    if (Convert.ToInt32(Valid) == 1)
                    {
                        //驗證通過
                        //確認帳號與密碼正確後取出使用者資料
                        var AcDt = (from ac in db.T_SysUser
                                    where (ac.UserID == AccountStr.Replace("'", "''")) && (ac.UserPassword == PwdStr.Replace("'", "''"))
                                    select ac).SingleOrDefault();
                        db.Dispose();
                        //登入作業
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        //將登入者資料存入 UserData物件至session
                        UserData.IP = tClientIP;
                        UserData.UserID = AcDt.UserID.ToString();
                        UserData.UserPKID = AcDt.ID.ToString();
                        UserData.UserName = AcDt.UserName;
                        UserData.UserGroup = AcDt.RoleID.ToString();
                        UserData.UserDept = AcDt.DeptID.ToString();
                        Session["UserData"] = UserData;

                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", UserData.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Login.ToString());
                        logger.Info("登入");

                        TimeSpan ts = DateTime.Now - Convert.ToDateTime(AcDt.UserPasswordModifyTime);
                        int DayDiffer = ts.Days;
                        if (UserData.UserGroup == "1")
                        {
                            //管理員預設進入主網
                            Session["WEBID"] = "1";
                            if (DayDiffer > 31)
                            {
                                Msg.MsgBox_In_Ajax("密碼已經超過30天未變更，請記得更換", this.Page, "../../Default.aspx?webid=" + Common.Encrypt("1"));                                                   
                            }
                            else
                            {
                                Response.Redirect("~/Default.aspx?webid=" + Common.Encrypt("1"), false);
                            }
                        }
                        else
                        {
                            //非管理員進入自己的後台
                            db = new AFRCDutyEntities();
                            //取得所有該單位可維護的WEB 並取第一個
                          //  var Webid = db.T_Web.Where(w => w.Dept.Contains(AcDt.DeptID.ToString())).FirstOrDefault();
                            db.Dispose();

                          // if (Webid != null){
                                //自己的後台
                                //Session["WEBID"] = Webid.WebID.ToString();
                                if (DayDiffer > 31)
                                {
                                    Msg.MsgBox_In_Ajax("密碼已經超過30天未變更，請記得更換", this.Page, "../../Default.aspx?webid=" + Common.Encrypt("1"));
                                }
                                else
                                {
                                    Response.Redirect("~/Default.aspx?webid=" + Common.Encrypt("1"), false);
                                }
                         //   }
                            
                            
                        }

                    }
                    else
                    {
                        //驗證沒過
                    }
                }
                else
                {
                    //沒驗證過
                }


            }
            catch (Exception ex)
            {
                #region ActionLog
                //so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                //GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Login.ToString());
                logger.Error(ex);
                #endregion
            }
        }


        /// <summary>
        /// 新增
        /// </summary>
        public void Insert()
        {

            using (AFRCDutyEntities db = new AFRCDutyEntities())
            {
                T_SysUser usr = new T_SysUser
                {
                    UserID = "1231232312",
                    UserEmail = "13123213",
                    DeptID = 1,
                    UseState = "12321311"
                };
                db.T_SysUser.Add(usr);

                db.SaveChanges();

            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        public void Update()
        {
            using (AFRCDutyEntities db = new AFRCDutyEntities())
            {
                T_SysUser usr = db.T_SysUser.Where(u => u.UserID == "tretet").FirstOrDefault();

                usr.UserName = "修改姓名";
                usr.UserEmail = "";
                db.SaveChanges();
            }
        }
        /// <summary>
        /// 刪除
        /// </summary>
        public void Delete()
        {
            using (AFRCDutyEntities db = new AFRCDutyEntities())
            {
                T_SysUser usr = db.T_SysUser.Where(u => u.UserID == "").FirstOrDefault();

                db.T_SysUser.Remove(usr);

                db.SaveChanges();
            }
        }
    }
}