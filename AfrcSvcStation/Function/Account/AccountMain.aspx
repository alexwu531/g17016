﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AccountMain.aspx.cs" Inherits="AfrcSvcStation.Function.Account.AccountMain" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 36px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>

    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
            <h1>帳戶維護
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    新增編輯帳戶
                                </small>
            </h1>
        </div>

        <div class="col-xs-12 col-md-10 col-md-offset-1">
                        <!-- search-area  BEGINS -->
            <div class="widget-box">
                <div class="widget-header widget-header-small">
                    <h5 class="widget-title lighter">查詢條件</h5>
                </div>

                <div class="widget-body">
                    <div class="widget-main">

                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-lg-6 ">
                                <div class="input-group">
                                    <span class="input-group-addon">帳戶
                                    </span>
                                    <asp:TextBox ID="txtUserID" runat="server" CssClass="form-control search-query" placeholder="請填入帳號"></asp:TextBox>
                                </div>

                                <div class="hr"></div>

                                <div class="input-group">
                                    <span class="input-group-addon">使用者名稱
                                    </span>
                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control search-query" placeholder="請填入使用者名稱"></asp:TextBox>
                                </div>

                                <div class="hr"></div>

                                <div class="input-group">
                                    <span class="input-group-addon">帳號是否使用
                                    </span>
                                    <asp:DropDownList ID="ddlUseState" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                                        <asp:ListItem Value="1">使用中</asp:ListItem>
                                        <asp:ListItem Value="2">停用</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="hr"></div>
                                <asp:Button ID="btnSearch" runat="server" Text="查詢" CausesValidation="False" CommandName="Search" CssClass="btn btn-purple btn-sm" OnClick="btnSearch_Click" />
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            <div class="hr hr-18 dotted hr-double"></div>
            <div class="dataTables_wrapper form-inline no-footer">

                <div class="row">
                    <div class="pull-right">
                        <asp:Button ID="cmdadd" runat="server" Text="新增" CssClass="btn btn-info btn-sm" OnClick="cmdadd_Click" />
                    </div>
                </div>
            <asp:UpdatePanel ID="up1" runat="server">
                <ContentTemplate>

                    <div class="dataTables_wrapper form-inline no-footer">


                        <table id="simple-table" class="table  table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>帳戶</th>
                                    <th>使用者名稱</th>
                                    <th>密碼</th>
                                    <th>權限</th>
                                    <th>使用中</th>
                                    <th>編輯</th>
                                    <th>刪除</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repUserView" runat="server" OnItemDataBound="repUserView_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                        <td><%#Eval("UserID") %></td>
                                        <td><%#Eval("UserName")%></td>
                                        <td><%#Eval("UserPassword")%></td>
                                        <td>
                                            <asp:Label ID="lblRoleID" runat="server" Text='<%#Eval("RoleID")%>'></asp:Label>                                            
                                            <asp:HiddenField ID="HiddenField_RoleID" runat="server" value='<%#Eval("RoleID")%>'/>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblUseState" runat="server" Text='<%#Eval("UseState") %>'></asp:Label>                                            
                                            <asp:HiddenField ID="HiddenField_UseState" runat="server" value='<%#Eval("UseState")%>'/>
                                        </td>
                                        <td class="center"><asp:Button ID="btnEdit" runat="server" Text="編輯" CssClass="btn btn-xs btn-info" CausesValidation="False" CommandName="Edit" CommandArgument='<%#Eval("ID")%>' OnCommand="btnEdit_Command"/></td>                                        
<%--                                            <a class="btn btn-xs btn-warning" href='<%# ResolveUrl("GroupPowerEdit")+"?StrID="+Eval("GroupID").ToString() %>'><i class="ace-icon fa fa-key bigger-120"></i></a>--%>
                                        <td><asp:Button ID="btnDelete" runat="server" Text="刪除" CausesValidation="False" CommandName="Delete" CssClass="btn btn-xs btn-danger" CommandArgument='<%#Eval("ID")%>' OnCommand="btnDelete_Command" OnClientClick="return confirm('確認刪除?')"/></td>
                                        </tr>
                                    </ItemTemplate>

                                </asp:Repeater>

                            </tbody>
                        </table>

                        <div class="row">

                            <div class="col-xs-12">
                                <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                                        ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                                    </webdiyer:AspNetPager>

                                </div>
                            </div>
                        </div>

                    </div>


                    <!-- PAGE CONTENT ENDS -->
                    </div>
    <!-- /.col -->




                </ContentTemplate>

            </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <!-- #dialog-message -->
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
</asp:Content>
