﻿using AfrcSvcStation.App_Code;
using AfrcSvcStation.ModelsForStation;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.Function.Account
{
    public partial class DeptEdit : System.Web.UI.Page
    {
        WebMessage Msg = new WebMessage();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        UserInfo so =  new UserInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }


        /// <summary>
        /// 參數清單綁定
        /// </summary>
        public void BindData()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {

                    var rr = from ff in db.T_SysDept
                             where ff.del == 1 && (!string.IsNullOrEmpty(txtword.Text) ? ff.caption.Contains(txtword.Text) : true)
                             select new
                             {
                                 sn = ff.sn,
                                 belong_sn = ff.belong_sn,
                                 caption = ff.caption,
                                 caption_s = ff.caption_s,
                                 sort = ff.sort,
                                 belong_name = db.T_SysDept.Where(s => s.sn == ff.belong_sn).FirstOrDefault().caption,
                                 islocal = ff.IsLocal == true ? true : false

                             };
                    AspNetPager1.AlwaysShow = true;
                    AspNetPager1.RecordCount = rr.Count();
                    gvUnitList.DataSource = rr.OrderBy(o => o.sort).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize).Take(AspNetPager1.PageSize).ToList();
                    gvUnitList.DataBind();


                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(rr.ToString());
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }

        }


        /// <summary>
        /// 分頁事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            gvUnitList.ShowFooter = false;
            BindData();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //新增參數
            if (e.CommandName == "Add")
            {
                UserInfo so = (UserInfo)Session["UserData"];
                TextBox txtSort = gvUnitList.FooterRow.FindControl("txtSort") as TextBox;
                TextBox txtcaption = gvUnitList.FooterRow.FindControl("txtDeptName") as TextBox;
                TextBox txtcaption_s = gvUnitList.FooterRow.FindControl("txtDeptName_s") as TextBox;
                CheckBox chbIsLocal = gvUnitList.FooterRow.FindControl("chbisLocal") as CheckBox;
                DropDownList ddlbelongsn = gvUnitList.FooterRow.FindControl("ddlbelongsn") as DropDownList;

                AFRCDutyEntities db = new AFRCDutyEntities();

                try
                {
                    T_SysDept Dept = new T_SysDept
                    {
                        sort = int.Parse(txtSort.Text),
                        belong_sn = int.Parse(ddlbelongsn.SelectedValue),
                        caption = txtcaption.Text,
                        caption_s = txtcaption_s.Text,
                        del = 1,
                        update_user = int.Parse(so.UserPKID),
                        update_time = DateTime.UtcNow.AddHours(8),
                        IsLocal = (chbIsLocal.Text == "True" ? true : false)

                    };
                    db.T_SysDept.Add(Dept);

                    db.SaveChanges();


                    gvUnitList.ShowFooter = false;
                    BindData();
                    Msg.MsgBox_In_Ajax("新增完成", this.Page);

                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Insert.ToString());
                    logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_Dept", Dept.sn));
                    db.Dispose();
                    #endregion
                }
                catch (Exception ex)
                {
                    Msg.MsgBox_In_Ajax("新增失敗", this.Page);

                    #region ActionLog
                    so = (UserInfo)Session["UserData"];
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Insert.ToString());
                    logger.Error(ex);                   
                    #endregion
                }
                finally
                {
                    db.Dispose();
                }
            }
            //取消新增
            else if (e.CommandName == "CancelAdd")
            {
                gvUnitList.ShowFooter = false;
                BindData();
            }
        }


        #region 清單各類事件
        /// <summary>
        /// 開啟編輯
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvUnitList.EditIndex = e.NewEditIndex;
            BindData();
            ScriptManager.RegisterStartupScript(this, GetType(), "spinner", @"spinner();", true);
        }
        /// <summary>
        /// 清單儲存事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            UserInfo so = (UserInfo)Session["UserData"];
            int sn = int.Parse(((HiddenField)gvUnitList.Rows[e.RowIndex].FindControl("hidID")).Value);
            int Sort = int.Parse(((TextBox)gvUnitList.Rows[e.RowIndex].FindControl("txtSort")).Text.ToString());
            string caption = ((TextBox)gvUnitList.Rows[e.RowIndex].FindControl("txtDeptName")).Text.ToString();
            string caption_s = ((TextBox)gvUnitList.Rows[e.RowIndex].FindControl("txtDeptName_s")).Text.ToString();
            int belongsn = int.Parse(((DropDownList)gvUnitList.Rows[e.RowIndex].FindControl("ddlbelongsn")).SelectedValue.ToString());
            string isLocal = ((CheckBox)gvUnitList.Rows[e.RowIndex].FindControl("chbisLocal")).Checked.ToString();
            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                T_SysDept Dept = db.T_SysDept.Where(u => u.sn == sn).FirstOrDefault();
                Dept.sort = Sort;
                Dept.caption = caption;
                Dept.caption_s = caption_s;
                Dept.belong_sn = belongsn;
                Dept.update_time = DateTime.UtcNow.AddHours(8);
                Dept.update_user = int.Parse(so.UserPKID);
                Dept.IsLocal = isLocal=="True"?true:false ;

                db.SaveChanges();

                gvUnitList.EditIndex = -1;
                BindData();
                Msg.MsgBox_In_Ajax("更新完成", this.Page);
                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_Dept", Dept.sn));
                db.Dispose();
                #endregion
            }
            catch (Exception ex)
            {
                Msg.MsgBox_In_Ajax("更新失敗", this.Page);

                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                logger.Error(ex);
                #endregion
            }
            finally
            {
                db.Dispose();
            }
        }
        /// <summary>
        /// 清單刪除事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            UserInfo so = (UserInfo)Session["UserData"];
            int sn = int.Parse(((HiddenField)gvUnitList.Rows[e.RowIndex].FindControl("hidID")).Value);


            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                T_SysDept Dept = db.T_SysDept.Where(u => u.sn == sn).FirstOrDefault();
                Dept.del = 2;
                db.SaveChanges();

                gvUnitList.EditIndex = -1;
                BindData();
                Msg.MsgBox_In_Ajax("刪除完成", this.Page);

                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_Dept", Dept.sn));
                db.Dispose();
                #endregion
            }
            catch (Exception ex)
            {
                Msg.MsgBox_In_Ajax("刪除失敗", this.Page);

                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                logger.Info(ex);
                db.Dispose();
                #endregion
            }
            finally
            {
                db.Dispose();
            }
        }
        /// <summary>
        /// 清單取消編輯
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUnitList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvUnitList.EditIndex = -1;
            BindData();
        }
        #endregion

        protected void cmdadd_Click(object sender, EventArgs e)
        {
            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                gvUnitList.ShowFooter = true;
                BindData();

                ScriptManager.RegisterStartupScript(this, GetType(), "tobottom", "window.scrollTo(0,document.body.scrollHeight-10);spinner();", true);
                gvUnitList.FooterRow.FindControl("txtSort").Focus();

                var captions = from cap in db.T_SysDept
                               where cap.belong_sn == 0
                               select cap;

                DropDownList ddlbelongsn = gvUnitList.FooterRow.FindControl("ddlbelongsn") as DropDownList;
                ddlbelongsn.DataSource = captions.ToList();
                ddlbelongsn.DataTextField = "caption";
                ddlbelongsn.DataValueField = "sn";
                ddlbelongsn.DataBind();
                db.Dispose();

                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(captions.ToList().ToString());
                db.Dispose();
                #endregion
            }
            catch (Exception ex)
            {
                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                logger.Error(ex);
                db.Dispose();
                #endregion
            }
            finally
            {
                db.Dispose();
            }
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void gvUnitList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                    {
                        DropDownList ddList = (DropDownList)e.Row.FindControl("ddlbelongsn");

                        var captions = from cap in db.T_SysDept
                                       where cap.belong_sn == 0
                                       select cap;

                        ddList.DataSource = captions.ToList();
                        ddList.DataTextField = "caption";
                        ddList.DataValueField = "sn";
                        ddList.DataBind();



                        DataRowView dr = e.Row.DataItem as DataRowView;
                        object belong_sn = DataBinder.Eval(e.Row.DataItem, "belong_sn");

                        ddList.SelectedValue = belong_sn.ToString();

                        #region ActionLog
                        so = (UserInfo)Session["UserData"];
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Error(captions.ToList().ToString());
                        db.Dispose();
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                so = (UserInfo)Session["UserData"];
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                db.Dispose();
                #endregion
            }
            finally
            {
                db.Dispose();
            }
        }
    }
}