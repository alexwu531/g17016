﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClickCounterReport.aspx.cs" Inherits="AfrcSvcStation.Function.StatisticReport.WebSiteCounterReport" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../../WebUserControl/WucDate.ascx" TagName="WucDate" TagPrefix="uc1" %>
<%@ Register Src="~/WebUserControl/WucDate.ascx" TagPrefix="uc2" TagName="WucDate" %>
<%@ Register Src="~/WebUserControl/WucDateRange.ascx" TagPrefix="uc1" TagName="WucDateRange" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 
    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
    
    
    
            <div class="col-xs-12">
        <div class="page-header">
            <h1>計數器統計查詢
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    計數器統計報表
                                </small>
            </h1>
        </div>
    <div class="col-xs-12 col-md-10 col-md-offset-1">
                        <!-- search-area  BEGINS -->
            <div class="widget-box">
                <div class="widget-header widget-header-small">
                    <h5 class="widget-title lighter">查詢條件</h5>
                </div>

                <div class="widget-body">
                    <div class="widget-main">

                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-lg-6 ">
                              


                                       <div class="hr"></div>
                                <div class="input-group">
                                    <span class="input-group-addon">時間範圍
                                    </span>
                                    <%--<uc2:WucDate runat="server" ID="WucDate1" placeholder="請選擇開始時間" />--%>
                                    <uc1:WucDateRange runat="server" ID="WucDateRange1" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-exchange"></i>
                                    </span>
                                    <%--<uc2:WucDate runat="server" ID="WucDate2" placeholder="請選擇結束時間" />--%>
                                    <uc1:WucDateRange runat="server" ID="WucDateRange2" />
                                </div>
                                <div class="hr"></div>

                                <div class="input-group">
                                    <span class="input-group-addon">查詢是否成功是否使用
                                    </span>
                                    <asp:DropDownList ID="ddlQueryState" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                                        <asp:ListItem Value="1">成功</asp:ListItem>
                                        <asp:ListItem Value="2">失敗</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="hr"></div>
                                <%--<asp:Button ID="btnSearch" runat="server" Text="查詢" CausesValidation="False" CommandName="Search" CssClass="btn btn-purple btn-sm" OnClick="btnSearch_Click" />--%>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            <div class="hr hr-18 dotted hr-double"></div>
            <div class="dataTables_wrapper form-inline no-footer">
                <rsweb:ReportViewer ID="ReportViewer1" Width="100%" runat="server"></rsweb:ReportViewer>

                </div>
            </div>
        <!-- #dialog-message -->
                </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
</asp:Content>
