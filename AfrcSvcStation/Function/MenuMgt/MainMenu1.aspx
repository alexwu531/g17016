﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MainMenu1.aspx.cs" Inherits="AfrcSvcStation.Function.MenuMgt.MainMenu1" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="../css/jquery-ui.custom.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
    <!-- PAGE CONTENT BEGINS -->
    <div class="col-xs-12">
        <div class="page-header">
            <h1>選單管理
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    新增選單維護
                                </small>
            </h1>
        </div>

        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <!-- search-area  BEGINS -->
            <div class="widget-box">
                <div class="widget-header widget-header-small">
                    <h5 class="widget-title lighter">查詢條件</h5>
                </div>

                <div class="widget-body">
                    <div class="widget-main">

                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-lg-6 ">
                                <div class="input-group">
                                    <span class="input-group-addon">關鍵字
                                    </span>
                                    <asp:TextBox ID="txtword" runat="server" CssClass="form-control search-query" placeholder="選單名稱"></asp:TextBox>
                                    <span class="input-group-btn">
                                        <asp:Button ID="cmdSearch" runat="server" Text="搜尋" CssClass="btn btn-purple btn-sm" OnClick="cmdSearch_Click" />

                                    </span>
                                </div>

                                <div class="hr"></div>
                                <div class="input-group">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            <div class="hr hr-18 dotted hr-double"></div>
            <div class="dataTables_wrapper form-inline no-footer">
                <asp:UpdatePanel ID="Upp1" runat="server">
                    <ContentTemplate>
                        <div class="row">

                            <div class="pull-right">
                                <asp:Button ID="cmdadd" runat="server" Text="新增" CssClass="btn btn-info btn-sm" OnClick="cmdadd_Click" />
                            </div>
                        </div>
                        <!--列表-->

                        <asp:GridView ID="gvMenuList" runat="server" AutoGenerateColumns="False" CssClass="table  table-bordered table-hover"
                            OnRowEditing="gvMenuList_RowEditing" OnRowUpdating="gvMenuList_RowUpdating" OnRowDeleting="gvMenuList_RowDeleting" OnRowCancelingEdit="gvMenuList_RowCancelingEdit"
                            OnRowCommand="gvMenuList_RowCommand" OnRowDataBound="gvMenuList_RowDataBound">

                            <Columns>
                                 <!--排序-->
                                <asp:TemplateField HeaderText="排序" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hidID" runat="server" Value='<%#Eval("MenuID")%>' />
                                        <asp:Label ID="lblSort" runat="server" Text='<%#Eval("Sort")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:HiddenField ID="hidID" runat="server" Value='<%#Eval("MenuID")%>' />

                                        <div class="ace-spinner middle touch-spinner" style="width: 125px;">
                                            <div class="input-group">

                                                <asp:TextBox ID="txtSort" runat="server" CssClass="spinbox-input form-control text-center txtspinner" Text='<%#Eval("Sort")%>'></asp:TextBox>
                                            </div>
                                        </div>
                                    </EditItemTemplate>

                                    <FooterTemplate>
                                        <asp:TextBox ID="txtSort" runat="server" CssClass="spinbox-input form-control text-center txtspinner" Text='<%#Eval("Sort")%>'></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
   
                                <!--選單名稱-->
                                <asp:TemplateField HeaderText="選單名稱" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblMuneName" runat="server" Text='<%#Eval("MenuName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text='<%#Eval("MenuName")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text=""></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <!--是否需要聲明-->
                                <asp:TemplateField HeaderText="是否需要聲明">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNotice" runat="server" Text='<%#Eval("NoticeID")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>

                                                  <asp:DropDownList ID="lblNotice" Width="95%" runat="server" >
                                                    <asp:ListItem Selected="True" Value="0">無聲明</asp:ListItem>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                  <asp:DropDownList ID="lblNotice" Width="95%" runat="server" >
                                          
                                                </asp:DropDownList>
                                            </FooterTemplate>
                               </asp:TemplateField>

                              <!--新增者-->                 
                                <asp:TemplateField HeaderText="選單名稱" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblMuneName" runat="server" Text='<%#Eval("MenuName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text='<%#Eval("MenuName")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text=""></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                                             
                                <!--新增時間-->                 
                                <asp:TemplateField HeaderText="選單名稱" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblMuneName" runat="server" Text='<%#Eval("MenuName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text='<%#Eval("MenuName")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text=""></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                                                
                                <!--修改者-->                 
                                <asp:TemplateField HeaderText="選單名稱" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblMuneName" runat="server" Text='<%#Eval("MenuName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text='<%#Eval("MenuName")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text=""></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                                               
                                <!--修改時間-->                 
                                <asp:TemplateField HeaderText="選單名稱" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblMuneName" runat="server" Text='<%#Eval("MenuName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text='<%#Eval("MenuName")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtMenuName" Width="95%" runat="server" Text=""></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                                                
                                <!--啟用與否-->                 
                                 <asp:TemplateField HeaderText="起用與否" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="chbisLocal" runat="server" Text='<%#Eval("isLocal")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chbisLocal"  Width="95%" runat="server" Text=""></asp:CheckBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chbisLocal" Width="95%" runat="server" Text=""></asp:CheckBox>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="編輯" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Button ID="Button2" runat="server" Text="編輯" CssClass="btn btn-xs btn-info" CausesValidation="False" CommandName="Edit" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Button ID="cmdCancel" runat="server" CssClass="btn btn-xs" Text="取消" CausesValidation="False" CommandName="Cancel" />
                                        <asp:Button ID="cmdSave" runat="server" Text="儲存" CssClass="btn-success btn btn-xs" CausesValidation="True" CommandName="Update" CommandArgument="edit" />
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button ID="cmdCancel" runat="server" CssClass="btn btn-xs" Text="取消" CausesValidation="False" CommandName="CancelAdd" />
                                        <asp:Button ID="cmdSave" runat="server" Text="儲存" CssClass="btn-success btn btn-xs" CausesValidation="True" CommandName="Add" />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="刪除" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Button ID="Button1" runat="server" Text="刪除" CausesValidation="False" CommandName="Delete" CssClass="btn btn-xs btn-danger" OnClientClick="return confirm('確認刪除?')" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>

                        </asp:GridView>

                        <div class="row">

                            <div class="col-xs-12">
                                <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                                        ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                                    </webdiyer:AspNetPager>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!--列表-->

                <%--      <table id="simple-table" class="table  table-bordered table-hover">
                    <thead>
                        <tr>

                            <th>群組名稱</th>
                            <th>狀態</th>
                            <th>編輯</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                               <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                                        ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                                    </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
    <script src="../js/spinbox.min.js"></script>
    <script type="text/javascript">

        function spinner() {
            $('.txtspinner').ace_spinner({ value: 1, min: 1, max: 100, step: 1, touch_spinner: true, icon_up: 'ace-icon fa fa-caret-up bigger-110', icon_down: 'ace-icon fa fa-caret-down bigger-110' })
           .closest('.ace-spinner')
           .on('changed.fu.spinbox', function () {
               //console.log($('#spinner1').val())
           });




        }
    </script>

</asp:Content>
