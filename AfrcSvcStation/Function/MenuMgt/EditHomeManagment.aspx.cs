﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AfrcSvcStation.WebUserControl;
using AfrcSvcStation.App_Code;
using AfrcSvcStation.ModelsForStation;

namespace AfrcSvcStation.Function.MenuMgt
{
    public partial class EditHomeManagment : System.Web.UI.Page
    {
        protected UserInfo so = new UserInfo();
        protected string PicName;
        protected string PicPath;
        protected string sn = HttpContext.Current.Request.QueryString["sn"];
        WebMessage MsgBox = new WebMessage();
        //FileUploadPathHelper PathHelper = new FileUploadPathHelper();
        Common comm = new Common();

        protected void Page_Load(object sender, EventArgs e)
        {
            so = (UserInfo)Session["UserData"];
            if (!IsPostBack)
            {

                DataForBind();
                BindMenu();
                
            }           
        }

        protected void DataForBind()
        {
            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                int sn = Convert.ToInt32(this.sn);

                var NewsList = (from N in db.T_News
                            where /*N.WebID == WebID && */N.sn == sn
                            select N).FirstOrDefault();
                if (NewsList != null)
                {
                    txtNewName.Text = NewsList.caption;
                    Wucd.CNDate = NewsList.d != null ? Convert.ToDateTime(NewsList.d).ToShortDateString() : "";
                    WucDateRange1.CNDateRange = NewsList.d1 != null ? Convert.ToDateTime(NewsList.d1).ToShortDateString() : "";
                    WucDateRange2.CNDateRange = NewsList.d2 != null ? Convert.ToDateTime(NewsList.d2).ToShortDateString() : "";
                    ddlIsShow.SelectedValue = NewsList.is_show.ToString() == "True" ? "1" : "0";
                   // ddlIsShowMain.SelectedValue = NewsList.PublishMainPage.ToString() == "True" ? "1" : "0";
                    ddlMenu.SelectedValue = NewsList.MenuType.ToString();
                    txtContr.Text = Server.HtmlDecode(NewsList.content);

                    //string SavePath = "\\Uploads\\News\\" + NewsList.pic;
                    //pic.ImageUrl = SavePath;

                    //MetaData clsCMetaData = new MetaData();
                    //MetaData CSmetaData = clsCMetaData.GetMetaData(NewsList.MetaData);//解析XML後把XML的值帶入欄位
                    //hfThemeValue.Value = CSmetaData.Theme;
                    //hfCakeValue.Value = CSmetaData.Cake;
                    //hfServiceValue.Value = CSmetaData.Service;

                    //WucMetaDataControl.MetaDataXML = NewsList.MetaData;

                    so = (UserInfo)Session["UserData"];
                    lblModifyUser.Text = NewsList.update_user;
                    lblModifyTime.Text = NewsList.update_time.ToString();
                }
                else
                {//新增                    
                    Wucd.CNDate = DateTime.Now.ToShortDateString();
                    WucDateRange1.CNDateRange = DateTime.Now.ToShortDateString();                   
                   // WucMetaDataControl.MetaDataXML = null;//null則帶入預設值
                }

                db.Dispose();
            }
            catch (Exception ex)
            {
                db.Dispose();
            }
            finally
            {
                db.Dispose();
            }
        }


        /// <summary>
        /// 第二層部門綁定
        /// </summary>
        public void BindMenu()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    List<T_Menu> Menu = new List<T_Menu>();

                        Menu = (from M in db.T_Menu
                                orderby M.Sort
                                where M.Del == 1 && M.MenuID != 0 select M).ToList();
                    if (Menu!=null) {

                        ddlMenu.DataSource = Menu;
                        ddlMenu.DataTextField = "MenuName";
                        ddlMenu.DataValueField = "MenuID";
                        ddlMenu.DataBind();

                        ddlMenu.Items.Insert(0, new ListItem("請選擇選單類別", "0"));
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        #region GetWebID
        protected string GetWebID()
        {//GetWebID
            return Session["WEBID"].ToString();
        }
        #endregion

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string checkMsg = "";
            if (string.IsNullOrEmpty(txtNewName.Text))
            {
                checkMsg += "標題欄位必填\\n";
            }
            if (string.IsNullOrEmpty(Wucd.CNDate))
            {
                checkMsg += "發佈日期欄位必填\\n";
            }
            if (ddlIsShow.SelectedValue.ToString() == "x")
            {
                checkMsg += "請選擇是否顯示\\n";
            }
            /*if (ddlIsShowMain.SelectedValue.ToString() == "x")
            {
                checkMsg += "請選擇是否顯示於主網\\n";
            }*/
           /* if (string.IsNullOrEmpty(WucMetaDataControl.metatitle))
            {
                checkMsg += "分類檢索標題欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metasubject))
            {
                checkMsg += "分類檢索主旨欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaCreator))
            {
                checkMsg += "分類檢索作者姓名欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaPublisher))
            {
                checkMsg += "分類檢索機關全稱欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaWucDate))
            {
                checkMsg += "分類檢索製作日期欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaIdengifier))
            {
                checkMsg += "分類檢索OID欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metatheme))
            {
                checkMsg += "分類檢索主題分類欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metacake)/*WucMetaDataControl.metacake)
            {
                checkMsg += "分類檢索施政分類欄位必填\\n";
            }
            if (string.IsNullOrEmpty(WucMetaDataControl.metaservice)/*(WucMetaDataControl.metaservice)
            {
                checkMsg += "分類檢索服務分類欄位必填\\n";
            }*/
            
            try
            {
                if (!string.IsNullOrEmpty(Wucd.CNDate))
                {
                    DateTime S = Convert.ToDateTime(Wucd.CNDate);
                }
            }
            catch (Exception)
            {
                checkMsg += "發佈日期請輸入正確日期格式\\n";
            }

            try
            {
                if (!string.IsNullOrEmpty(WucDateRange1.CNDateRange))
                {
                    DateTime D1 = Convert.ToDateTime(WucDateRange1.CNDateRange);
                }

                if (!string.IsNullOrEmpty(WucDateRange2.CNDateRange))
                {
                    DateTime D2 = Convert.ToDateTime(WucDateRange2.CNDateRange);
                }
            }
            catch (Exception)
            {
                checkMsg += "發佈日期請輸入正確日期格式\\n";
            }

            if (!string.IsNullOrEmpty(WucDateRange1.CNDateRange) && !string.IsNullOrEmpty(WucDateRange2.CNDateRange))
            {
                DateTime D1 = Convert.ToDateTime(WucDateRange1.CNDateRange);
                DateTime D2 = Convert.ToDateTime(WucDateRange2.CNDateRange);
                if (DateTime.Compare(D1,D2) > 0)
                {
                    checkMsg = "刊登結束日期日期必須大於起始日期\\n";
                }
            }

            if (!string.IsNullOrEmpty(checkMsg))
            {
                MsgBox.MsgBox_In_Ajax(checkMsg, this.Page);
                return;
            }


            if (string.IsNullOrEmpty(this.sn))
            {//新增
                #region 新增
                try
                {
                    AFRCDutyEntities db = new AFRCDutyEntities();


                    T_News NewsList = new T_News(); ;
                    if (NewsList != null)
                    {
                        NewsList.WebID = Convert.ToInt32(GetWebID());
                        NewsList.caption = txtNewName.Text.Trim();//標題
                        NewsList.Title = txtNewName.Text.Trim();
                        if (!string.IsNullOrEmpty(Wucd.CNDate))
                        {
                            NewsList.Date = Convert.ToDateTime(Wucd.CNDate);
                        }
                        if (!string.IsNullOrEmpty(Wucd.CNDate))
                        {
                            NewsList.d = Convert.ToDateTime(Wucd.CNDate);
                        }
                        if (!string.IsNullOrEmpty(WucDateRange1.CNDateRange))
                        {
                            NewsList.d1 = Convert.ToDateTime(WucDateRange1.CNDateRange);
                        }
                        if (!string.IsNullOrEmpty(WucDateRange2.CNDateRange))
                        {
                            NewsList.d2 = Convert.ToDateTime(WucDateRange2.CNDateRange);
                        }
                        NewsList.is_show = ddlIsShow.SelectedValue.ToString() == "1" ? true : false;
                        //NewsList.PublishMainPage = ddlIsShowMain.SelectedValue.ToString() == "1" ? true : false;
                        NewsList.MenuType = int.Parse(ddlMenu.SelectedValue.ToString());
                        NewsList.content = Server.HtmlDecode(txtContr.Text);

                        so = (UserInfo)Session["UserData"];
                        NewsList.update_user = comm.GetUserName(so.UserID);
                        NewsList.update_time = comm.GetDateTimeNow();

                     /*   if (FileUpload.HasFile)
                        {
                            string FilePath1 = PathHelper.GetUpoladPathByVer(GetWebID());
                            string SavePath = FilePath1 + "News\\" + FileUpload.FileName;
                            NewsList.pic = FileUpload.FileName;
                            NewsList.pic_path = "\\Uploads\\News\\";
                            //上傳
                            FileUpload.SaveAs(SavePath);
                        }*/


                        //MenuData
                        //WucMetaDataControl.metatheme = (hfThemeValue.Value).ToString().TrimEnd(',');
                        //WucMetaDataControl.metacake = (hfCakeValue.Value).ToString().TrimEnd(',');
                        //WucMetaDataControl.metaservice = (hfServiceValue.Value).ToString().TrimEnd(',');
/*
                        CheckBox ckb1 = (CheckBox)WucMetaDataControl.FindControl("ckbGetTitle");
                        if (ckb1.Checked)
                        {
                            WucMetaDataControl.InputTitle = txtCaption.Text;
                        }
                        CheckBox ckb2 = (CheckBox)WucMetaDataControl.FindControl("ckbGetSubject");
                        if (ckb2.Checked)
                        {
                            WucMetaDataControl.InputSubject = txtCaption.Text;
                        }
                        NewsList.MetaData = WucMetaDataControl.MetaDataXML;*/
                        
                        db.T_News.Add(NewsList);
                        db.SaveChanges();
                        MsgBox.MsgBox_In_Ajax("新增成功", this.Page, "HomeManagment");
                        db.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    //Logger.ErrorException("新增使用者", ex);
                    MsgBox.MsgBox_In_Ajax("新增失敗", this.Page, Request.RawUrl);
                }
                #endregion
            }
            else
            {//編輯
                try
                {
                    AFRCDutyEntities db = new AFRCDutyEntities();
                    int sn = Convert.ToInt32(this.sn);
                    T_News NewsList = db.T_News.FirstOrDefault(o => o.sn == sn);
                   
                    if (NewsList != null)
                    {
                        NewsList.caption = txtNewName.Text.Trim();//標題
                        NewsList.Title = txtNewName.Text.Trim();
                        if (!string.IsNullOrEmpty(Wucd.CNDate))
                        {
                            NewsList.Date = Convert.ToDateTime(Wucd.CNDate);
                            NewsList.d = Convert.ToDateTime(Wucd.CNDate);
                        }
                        else
                        {
                            NewsList.Date = null;
                            NewsList.d = null;
                        }

                        if (!string.IsNullOrEmpty(WucDateRange1.CNDateRange))
                        {
                            NewsList.d1 = Convert.ToDateTime(WucDateRange1.CNDateRange);
                        }
                        else
                        {
                            NewsList.d1 = null;
                        }

                        if (!string.IsNullOrEmpty(WucDateRange2.CNDateRange))
                        {
                            NewsList.d2 = Convert.ToDateTime(WucDateRange2.CNDateRange);
                        }
                        else
                        {
                            NewsList.d2 = null;
                        }
                        NewsList.is_show = ddlIsShow.SelectedValue.ToString() == "1" ? true : false;
                       // NewsList.PublishMainPage = ddlIsShowMain.SelectedValue.ToString() == "1" ? true : false;
                        NewsList.MenuType = int.Parse(ddlMenu.SelectedValue.ToString());
                        NewsList.content = Server.HtmlDecode(txtContr.Text);

                        so = (UserInfo)Session["UserData"];
                        NewsList.update_user = comm.GetUserName(so.UserID);
                        NewsList.update_time = comm.GetDateTimeNow();

                        /*if (FileUpload.HasFile)
                        {
                            string FilePath1 = PathHelper.GetUpoladPathByVer(GetWebID());
                            string SavePath = FilePath1 + "News\\" + FileUpload.FileName;
                            NewsList.pic = FileUpload.FileName;
                            NewsList.pic_path = "\\Uploads\\News\\";
                            //上傳
                            FileUpload.SaveAs(SavePath);
                        }*/

                        //MenuData
                        //WucMetaDataControl.metatheme = (hfThemeValue.Value).ToString().TrimEnd(',');
                        //WucMetaDataControl.metacake = (hfCakeValue.Value).ToString().TrimEnd(',');
                        //WucMetaDataControl.metaservice = (hfServiceValue.Value).ToString().TrimEnd(',');
                      /*  CheckBox ckb1 = (CheckBox)WucMetaDataControl.FindControl("ckbGetTitle");
                        if (ckb1.Checked)
                        {
                            WucMetaDataControl.InputTitle = txtCaption.Text;
                        }
                        CheckBox ckb2 = (CheckBox)WucMetaDataControl.FindControl("ckbGetSubject");
                        if (ckb1.Checked)
                        {
                            WucMetaDataControl.InputSubject = txtCaption.Text;
                        }
                        NewsList.MetaData = WucMetaDataControl.MetaDataXML;*/
                        db.SaveChanges();

                        db.Dispose();
                        //ScriptManager.RegisterStartupScript(Page, GetType(), "CloseFrame", "<script>parent.CloseFrame();</script>", false);
                        MsgBox.MsgBox_In_Ajax("編輯成功", this.Page, "HomeManagment");
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("HomeManagment");
        }

        protected void btnUploadPic_Click(object sender, EventArgs e)
        {

        }
    }
}