﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;
using NLog;

namespace AfrcSvcStation.Function.MenuMgt
{
    public partial class MainMenu : System.Web.UI.Page
    {
        WebMessage Msgbox = new WebMessage();
        protected string UserID = HttpContext.Current.Request.QueryString["UserID"];
        protected UserInfo so = new UserInfo();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            so = (UserInfo)Session["UserData"];
            if (!IsPostBack)
            {
                DataForBind();
            }
        }


        /*  0.初始資料綁定: 未除理時預設為?，依登入帳號綁定受理單位
         *  
         *  1.查詢:
         *  2.新增:無須新增
         *  3.修改: 修改前須跳出登入驗證視窗
         *  4.刪除:是否可刪除待商榷
         *  
         *  
         *  規則:
         *  受理時需做登入驗正，驗證後改變狀態為2 受理中
         *  
         *  CaseStatus==1(未受理)
         *  CaseStatus==2(受理中)
         *  CaseStatus==3(處理中)
         *  CaseStatus==4(已結案)
         *  
      */
        /*初始資料綁定*/
        private void DataForBind()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {

                    var MenuList =
                         db.T_Menu.Where(
                             c => txtMenuName.Text != "" ? c.MenuName.Contains(txtMenuName.Text) : true &&
                                  ddlMenuSate.SelectedValue.ToString() != "0" ? c.Del.ToString() == ddlMenuSate.SelectedValue.ToString() : true &&
                                  c.Del == 1
                                  ).ToList();
                    var MenuList_Query =
                     db.T_Menu.Where(
                         c => txtMenuName.Text != "" ? c.MenuName.Contains(txtMenuName.Text) : true &&
                                  ddlMenuSate.SelectedValue.ToString() != "0" ? c.Del.ToString() == ddlMenuSate.SelectedValue.ToString() : true &&
                                  c.Del == 1
                              ).ToString();
                    AspNetPager1.AlwaysShow = true;
                    AspNetPager1.RecordCount = MenuList.Count();

                    repMenuView.DataSource = MenuList.OrderBy(o => o.MenuID).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize)
                                             .Take(AspNetPager1.PageSize);
                    repMenuView.DataBind();


                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(MenuList_Query);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }
        }

        /* 綁定選單值*/
        protected void repMenuView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField HF_NoticeID = (HiddenField)e.Item.FindControl("HiddenField_NoticeID");
                HiddenField HF_MenuState = (HiddenField)e.Item.FindControl("HiddenField_MenuState");
                Label lblNotcieID = (Label)e.Item.FindControl("lblNoticeID");
                Label lblMenuState = (Label)e.Item.FindControl("lblMenuState");

                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    try
                    {
                        so = (UserInfo)Session["UserData"];

                        var Notice = (from N in db.T_Notice
                                     where N.NoticeID.ToString() == HF_NoticeID.Value
                                     select N).SingleOrDefault();

                       
                        lblNotcieID.Text = HF_NoticeID.Value == "0" ? "無選單聲明" : Notice.NoticeTitle;

                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Info(Notice.ToString());
                        db.Dispose();
                        #endregion
                    }
                    catch (Exception ex)
                    {

                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                        logger.Info(ex);
                        #endregion
                    }
                    lblMenuState.Text = HF_MenuState.Value == "1" ? "啟用" : "未啟用";


                }
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataForBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataForBind();
        }

        /// <summary>
        /// 刪除使用者
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                try
                {
                    AFRCDutyEntities db = new AFRCDutyEntities();

                    T_Menu Menu = db.T_Menu.FirstOrDefault(c => c.MenuID.ToString() == e.CommandArgument.ToString());
                    if (Menu != null)
                    {
                        Menu.Del = 0;
                        Menu.ModifiedUser = so.UserID;
                        db.SaveChanges();
                        Msgbox.MsgBox_In_Ajax("刪除完成", this.Page, Request.RawUrl);


                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                        logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_SysUser", so.UserID));
                        db.Dispose();
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    Msgbox.MsgBox_In_Ajax("刪除失敗", this.Page, Request.RawUrl);

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                    logger.Error(ex);
                    #endregion
                }
            }
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("MenuDetail.aspx?StrID=" + e.CommandArgument.ToString());
        }

        protected void cmdadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("MenuDetail");
        }
    }
}