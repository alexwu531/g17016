﻿using AfrcSvcStation.App_Code;
using AfrcSvcStation.ModelsForStation;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.Function.MenuMgt
{
    public partial class MainNotice : System.Web.UI.Page
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        bool IsShow;
        WebMessage MsgBox = new WebMessage();
        protected UserInfo so = new UserInfo();
        int intWebID;
        string[] aryWebBelongDept;


        /*  同意事項管理
         *  
         *  
         *  0.初始資料綁定:
         *  
         *  1.查詢:
         *  2.新增:無須新增
         *  3.修改: 修改前須跳出登入驗證視窗
         *  4.刪除:是否可刪除待商榷
         *  
         *  
         *  規則:
         *  
         *  
         * 
         *  
      */
        protected void Page_Load(object sender, EventArgs e)
        {
            so = (UserInfo)Session["UserData"];
            if (!IsPostBack)
            {
                intWebID = int.Parse(GetWebID());
                AFRCDutyEntities db = new AFRCDutyEntities();



                //BindPostType();
                // BindUserDept();
                BindMenu();
                DataForBind();


            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataForBind();
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                try
                {
                    AFRCDutyEntities db = new AFRCDutyEntities();

                    T_Notice Notice = db.T_Notice.FirstOrDefault(c => c.NoticeID.ToString() == e.CommandArgument.ToString());
                    if (Notice != null)
                    {
                        db.T_Notice.Remove(Notice);
                        db.SaveChanges();
                        MsgBox.MsgBox_In_Ajax("刪除完成", this.Page, Request.RawUrl);

                        #region ActionLog
                        string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                        logger.Info(string.Format("資料表:T_Notice;主鍵{0}", e.CommandArgument.ToString()));
                        #endregion
                    }
                }
                catch (Exception ex)
                {


                }
            }
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("NoticeDetail?Str=" + e.CommandArgument);
        }

        protected void cmdadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("NoticeDetail");
        }

        protected string GetWebID()
        {
            return "1";
        }

        protected void DataForBind()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {

                    var NoticeList = db.T_Notice.ToList();

                    DateTime DS = !string.IsNullOrEmpty(WucDate_S.CNDateRange) ? Convert.ToDateTime(WucDate_S.CNDateRange) : Convert.ToDateTime("1911-01-01");
                    DateTime DE = !string.IsNullOrEmpty(WucDate_E.CNDateRange) ? Convert.ToDateTime(WucDate_E.CNDateRange) : Convert.ToDateTime("9999-12-31");
                    int WebID = int.Parse(GetWebID());

                    if (ddlIsShow.SelectedValue.ToString() != "x")
                    {
                        switch (ddlIsShow.SelectedValue.ToString())
                        {
                            case "1":
                                IsShow = true;
                                break;
                            case "0":
                                IsShow = false;
                                break;

                        }
                    }


                    //var NewsList_Query =
                    // db.news.Where(
                    //     c => !string.IsNullOrEmpty(WucDate_S.CNDateRange) && !string.IsNullOrEmpty(WucDate_E.CNDateRange) ? c.d >= DS && c.d <= DE : true &&
                    //          !string.IsNullOrEmpty(WucDate_S.CNDateRange) && string.IsNullOrEmpty(WucDate_E.CNDateRange) ? c.d >= DS : true &&
                    //          string.IsNullOrEmpty(WucDate_S.CNDateRange) && !string.IsNullOrEmpty(WucDate_E.CNDateRange) ? c.d <= DE : true &&
                    //          ddlUnit.SelectedValue.ToString() != "0" ? c.unit_sn == Unit : true &&
                    //          ddlIsShow.SelectedValue.ToString() != "x" ? c.is_show == IsShow : (c.is_show == true || c.is_show == false) &&
                    //          c.WebID == WebID
                    //          ).ToList();

                    List<T_Notice> NoticeList_Query = new List<T_Notice>();

                    NoticeList_Query = (from s in db.T_Notice
                                      select s).ToList();



                    AspNetPager1.AlwaysShow = true;
                    AspNetPager1.RecordCount = NoticeList_Query.Count();

                    repNewList.DataSource = NoticeList_Query.OrderByDescending(o => o.NoticeID).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize)
                                             .Take(AspNetPager1.PageSize);
                    repNewList.DataBind();


                    #region ActionLog
                    string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(string.Format("資料表:news;"));
                    #endregion
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataForBind();
        }

        /// <summary>
        /// 下拉選單單位資料綁定_選單類別
        /// </summary>
        public void BindMenu()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    List<T_Menu> Menu = new List<T_Menu>();
                    

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(Menu.ToString());
                    #endregion

                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }
        }

        //單位 代碼to中文
        public string MenuTran(int intMenuType)
        {
            string rtValue = "";

            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                var Menu = (from M in db.T_Menu
                            where M.MenuID == intMenuType
                            select M.MenuName).SingleOrDefault();
                if (Menu != null)
                {
                    rtValue = Menu.ToString();
                }
                db.Dispose();
            }
            catch (Exception ex)
            {
                db.Dispose();
            }
            finally
            {
                db.Dispose();
            }

            return rtValue;
        }

        /// <summary>
        /// 儲存首頁訊息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            AFRCDutyEntities db = new AFRCDutyEntities();

            try
            {
                /*
                    so = (UserInfo)Session["UserData"];

                    T_LiveNews livenews = new T_LiveNews();

                    livenews.LiveNewsContent = txtlivenews.Text;

                    livenews.d1 = Convert.ToDateTime(WucDateRange1.CNDateRange);

                    livenews.d2 = Convert.ToDateTime(WucDateRange2.CNDateRange);

                    livenews.CreateUser = int.Parse(so.UserPKID);

                    livenews.CreateTime = DateTime.UtcNow.AddHours(8);

                    db.T_LiveNews.Add(livenews);

                    db.SaveChanges();
                    #region ActionLog
                    string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(string.Format("資料表:T_LiveNews;主鍵{0};內容:{1}", livenews.LiveNewsID.ToString(), livenews.LiveNewsContent));
                    #endregion
                    MsgBox.MsgBox_In_Ajax("更新首頁Live訊息完成", this.Page);
                    BindLiveNews();
                    */
            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Dispose();
            }
        }

    }
}