﻿using AfrcSvcStation.App_Code;
using AfrcSvcStation.ModelsForStation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.Function.MenuMgt
{
    public partial class NoticeDetail : System.Web.UI.Page
    {
        protected UserInfo so = new UserInfo();
        protected string PicName;
        protected string PicPath;
        protected string id = HttpContext.Current.Request.QueryString["Str"];
        WebMessage MsgBox = new WebMessage();
        //FileUploadPathHelper PathHelper = new FileUploadPathHelper();
        Common comm = new Common();

        protected void Page_Load(object sender, EventArgs e)
        {
            so = (UserInfo)Session["UserData"];
            if (!IsPostBack)
            {

                DataForBind();

            }
        }

        protected void DataForBind()
        {
            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                int nid = Convert.ToInt32(this.id);

                var NoticeList = (from N in db.T_Notice
                                where /*N.WebID == WebID && */N.NoticeID == nid
                                select N).FirstOrDefault();
                so = (UserInfo)Session["UserData"];
                if (NoticeList != null)
                {
                    txtNoticeName.Text = NoticeList.NoticeTitle;
                    txtContr.Text = Server.HtmlDecode(NoticeList.NoticeContent);



                    lblCreateUser.Text = comm.GetUserName(NoticeList.CreateUser);
                    lblCreateTime.Text = NoticeList.CreateTime.ToString();
                    lblModifyUser.Text = so.UserID;
                    lblModifyTime.Text = DateTime.Now.ToShortDateString();
                }
                else
                {//新增                    
                    lblCreateUser.Text = so.UserName;
                    lblCreateTime.Text = DateTime.Now.ToShortDateString();
                    // WucMetaDataControl.MetaDataXML = null;//null則帶入預設值
                }

                db.Dispose();
            }
            catch (Exception ex)
            {
                db.Dispose();
            }
            finally
            {
                db.Dispose();
            }
        }


    

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string checkMsg = "";
            if (string.IsNullOrEmpty(txtNoticeName.Text))
            {
                checkMsg += "標題欄位必填\\n";
            }
            if (!string.IsNullOrEmpty(checkMsg))
            {
                MsgBox.MsgBox_In_Ajax(checkMsg, this.Page);
                return;
            }
            if (string.IsNullOrEmpty(this.id))
            {//新增
                #region 新增
                try
                {
                    AFRCDutyEntities db = new AFRCDutyEntities();


                    T_Notice NoticeList = new T_Notice(); ;
                    if (NoticeList != null)
                    {
                        NoticeList.NoticeTitle = txtNoticeName.Text.Trim();//標題

                        //NewsList.PublishMainPage = ddlIsShowMain.SelectedValue.ToString() == "1" ? true : false;
                        NoticeList.NoticeContent = Server.HtmlDecode(txtContr.Text);

                        so = (UserInfo)Session["UserData"];
                        NoticeList.CreateUser = comm.GetUserName(so.UserID);
                        NoticeList.CreateTime = comm.GetDateTimeNow();
                        

                        db.T_Notice.Add(NoticeList);
                        db.SaveChanges();
                        MsgBox.MsgBox_In_Ajax("新增成功", this.Page, "MainNotice");
                        db.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    //Logger.ErrorException("新增使用者", ex);
                    MsgBox.MsgBox_In_Ajax("新增失敗", this.Page, Request.RawUrl);
                }
                #endregion
            }
            else
            {//編輯
                try
                {
                    AFRCDutyEntities db = new AFRCDutyEntities();
                    int nid = Convert.ToInt32(this.id);
                    T_Notice NoticeList = db.T_Notice.FirstOrDefault(o => o.NoticeID == nid);

                    if (NoticeList != null)
                    {
                        NoticeList.NoticeTitle = txtNoticeName.Text.Trim();


                        NoticeList.NoticeContent = Server.HtmlDecode(txtContr.Text);

                        so = (UserInfo)Session["UserData"];
                        NoticeList.ModifiedUser = comm.GetUserName(so.UserID);
                        NoticeList.ModifiedTime = comm.GetDateTimeNow();

                        db.SaveChanges();

                        db.Dispose();
                        MsgBox.MsgBox_In_Ajax("編輯成功", this.Page, "MainNotice");
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("MainNotice");
        }

        protected void btnUploadPic_Click(object sender, EventArgs e)
        {

        }
    }
}