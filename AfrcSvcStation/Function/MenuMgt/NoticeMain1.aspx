﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NoticeMain1.aspx.cs" Inherits="AfrcSvcStation.Function.MenuMgt.NoticeMain1" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/WebUserControl/WucDateRange.ascx" TagPrefix="uc1" TagName="WucDateRange" %>
<%@ Register Src="~/WebUserControl/WucDate.ascx" TagPrefix="uc1" TagName="WucDate" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%: Styles.Render("~/bundles/webmgtstyles") %>
    <style type="text/css">
        .auto-style1 {
            height: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="Sc1" runat="server"></asp:ScriptManager>
    <div class="col-xs-12">
        <div class="page-header">
            <h1>選單聲明管理
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    選單聲明維護
                                </small>
            </h1>
        </div>
        
        <%--<!--div class="form-horizontal" role="form" runat="server" id="livdiv">
            <div class="form-group">

                <label class="col-sm-2 control-label no-padding-right" for="form-field-1">首頁Live訊息</label>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtlivenews" placeholder="" CssClass="col-xs-12 col-sm-12" runat="server"></asp:TextBox>
                    <div class="col-sm-5 input-group">
                        <span class="input-group-addon">發佈日期</span>
                        <uc1:WucDateRange runat="server" ID="WucDateRange1" />

                        <span class="input-group-addon">
                            <i class="fa fa-exchange"></i>
                        </span>
                        <uc1:WucDateRange runat="server" ID="WucDateRange2" />
                    </div>
                </div>
                <div class="col-sm-3 col-sm-offset-2">
                    <br />
                    <asp:Button ID="btnSave" CssClass="btn btn-info" runat="server" Text="儲存" ClientIDMode="Static" OnClick="btnSave_Click" />
                </div>
            </div>
        </div-->--%>
        <div class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right" for="form-field-1">聲明消息編輯</label>

                <div class="col-sm-12">

                    <div class="col-xs-12">
   
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <!-- search-area  BEGINS -->
                            <div class="widget-box">
                                <div class="widget-header widget-header-small">
                                    <h5 class="widget-title lighter">查詢條件</h5>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-main">

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-8 col-lg-6 ">
                                                <div class="input-group">
                                                    <span class="input-group-addon">發布日期</span>
                                                    <uc1:WucDateRange runat="server" ID="WucDate_S" />

                                                    <span class="input-group-addon">
                                                        <i class="fa fa-exchange"></i>
                                                    </span>
                                                    <uc1:WucDateRange runat="server" ID="WucDate_E" />

                                                </div>


                                                <%--                                                <div class="input-group">
                                                    <span class="input-group-addon">訊息類別
                                                    </span>
                                                    <asp:DropDownList ID="ddlMenuType" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>--%>
                                                <%--      <div class="hr"></div>--%>

                                                <div class="input-group">
                                                    <span class="input-group-addon">是否顯示
                                                    </span>
                                                    <asp:DropDownList ID="ddlIsShow" runat="server" CssClass="form-control">
                                                        <asp:ListItem Selected="True" Value="x">請選擇</asp:ListItem>
                                                        <asp:ListItem Value="1">是</asp:ListItem>
                                                        <asp:ListItem Value="0">否</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:Button ID="btnSearch" runat="server" Text="查詢" CausesValidation="False" CommandName="Search" CssClass="btn btn-purple btn-sm" OnClick="btnSearch_Click" />
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </div>

                            <div class="hr hr-18 dotted hr-double"></div>
                            <div class="dataTables_wrapper form-inline no-footer">

                                <div class="row">
                                    <div class="pull-right">
                                        <!-- 新增聲明-->
                                        <asp:Button ID="cmdadd" runat="server" Text="新增" CssClass="btn btn-info btn-sm" OnClick="cmdadd_Click" />
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="up1" runat="server">
                                    <ContentTemplate>

                                        <div class="dataTables_wrapper form-inline no-footer">


                                            <table id="simple-table" class="table  table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <!-- 新增聲明-->
  <th>標題</th>
                                          <th>發布者</th>
                                                        <th>發布日期</th>
                                                        <th>編輯</th>
                                                        <th>刪除</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="repNewList" runat="server">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Eval("NoticeTitle")%></td>
                                                                <td><%# Eval("CreateUser")%></td>
                                                                <td><%# Convert.ToDateTime(Eval("CreateTime")).ToShortDateString()%></td>
                                                                <td class="center">
                                                                    <asp:Button ID="btnEdit" runat="server" Text="編輯" CssClass="btn btn-xs btn-info" CausesValidation="False" CommandName="Edit" CommandArgument='<%#Eval("NoticeID")%>' OnCommand="btnEdit_Command" /></td>
                                                                <td class="center">
                                                                    <asp:Button ID="btnDelete" runat="server" Text="刪除" CausesValidation="False" CommandName="Delete" CssClass="btn btn-xs btn-danger" CommandArgument='<%#Eval("NoticeID")%>' OnCommand="btnDelete_Command" OnClientClick="return confirm('確認刪除?')" />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                </tbody>
                                            </table>

                                            <div class="row">

                                                <div class="col-xs-12">
                                                    <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="10" OnPageChanged="AspNetPager1_PageChanged"
                                                            ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                                                        </webdiyer:AspNetPager>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <!-- PAGE CONTENT ENDS -->
                                        </div>
    <!-- /.col -->




                                    </ContentTemplate>

                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <!-- #dialog-message -->


                </div>
            </div>
        </div>


    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
    <%: Scripts.Render("~/bundles/webmgtjs") %>

    <script>
        function OpenAlbum() {

        }

        $(document).ready(function () {

            $(".iframe").colorbox({
                iframe: true, width: "100%", height: "100%"
            });


        });

        function CloseFrame() {
            $.colorbox.close();
        }
    </script>
</asp:Content>
