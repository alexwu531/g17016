﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;
using NLog;
using System.Text.RegularExpressions;

namespace AfrcSvcStation.Function.MenuMgt
{
    public partial class MenuDetail : System.Web.UI.Page
    {
        WebMessage MsgBox = new WebMessage();
        protected UserInfo so = new UserInfo();
        protected string StrID = HttpContext.Current.Request.QueryString["StrID"];
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {
            //UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            if (!IsPostBack)
            {
                try
                {
                    so = (UserInfo)Session["UserData"];
                    //VisitorLog.AddAction(so.UserID, VisitorLog.ActionType.pageview.ToString(), "系統管理>使用者管理");
                }
                catch (Exception ex)
                {

                }

                //  BinUserGroup();//下拉選單綁定_使用者群組
                //BindUserDept();//下拉選單綁定_使用者部門
                BindNoticeGroup();
                if (!string.IsNullOrEmpty(StrID))
                {//編輯
                    BindData();
                }
            }
        }

        /// <summary>
        /// 使用者資料綁定
        /// </summary>
        public void BindData()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    int StrID = Convert.ToInt32(this.StrID);

                    T_Menu menu = db.T_Menu.FirstOrDefault(M => M.MenuID == StrID);

                    if (menu != null)
                    {
                        //hidID.Value = user.SysUserID.ToString();
                        txtMenuName.Text = menu.MenuName;
                        txtMenuUrl.Text = menu.MenuUrl;

                        ddlNoticeID.SelectedValue = menu.NoticeID.ToString() == "0" ? "無選單聲明" : menu.NoticeID.ToString();

                        ddlMenuState.SelectedValue = menu.Del.ToString();
                    }

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(menu.ToString());
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }
        }

        /// <summary>
        /// 下拉選單單位資料綁定_使用者群組
        /// </summary>
        public void BindNoticeGroup()
        {
            using (AFRCDutyEntities db = new AFRCDutyEntities())
            {
                try
                {
                    var Notice = (from N in db.T_Notice
                                  select N).ToList();

                    ddlNoticeID.DataSource = Notice;
                    ddlNoticeID.DataTextField = "NoticeTitle";
                    ddlNoticeID.DataValueField = "NoticeID";
                    ddlNoticeID.DataBind();


                    ddlNoticeID.Items.Insert(0, new ListItem("無選單聲明", "0"));

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(Notice.ToString());
                    #endregion
                }
                catch (Exception ex)
                {
                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Error(ex);
                    #endregion
                }
            }
        }
        /*
        /// <summary>
        /// 下拉選單單位資料綁定_使用者部門
        /// </summary>
        public void BindUserDept()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    var Dept = (from D in db.T_SysDept
                                orderby D.sort
                                where D.del == 1 && D.belong_sn != 0
                                select D).ToList();

                    ddlUserDept.DataSource = Dept;
                    ddlUserDept.DataTextField = "caption";
                    ddlUserDept.DataValueField = "sn";
                    ddlUserDept.DataBind();

                    ddlUserDept.Items.Insert(0, new ListItem("請選擇部門", "0"));

                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(Dept.ToString());
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //取得IP
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                GlobalDiagnosticsContext.Set("addr", tClientIP);
                GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                logger.Error(ex);
                #endregion
            }
        }*/

        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            so = (UserInfo)Session["UserData"];

            string errorMsg = "";
            if (txtMenuName.Text == "")
            {
                errorMsg += "選單名稱必填\\n";
            }
            if (txtMenuUrl.Text == "")
            {
                errorMsg += "選單連結必填\\n";
            }



            if (!string.IsNullOrEmpty(errorMsg))
            {
                MsgBox.MsgBox_In_Ajax(errorMsg, this.Page, Request.RawUrl);
                return;
            }

            if (!string.IsNullOrEmpty(StrID))
            {
                #region 編輯
                try
                {
                    int StrID = Convert.ToInt32(this.StrID);
                    AFRCDutyEntities db = new AFRCDutyEntities();
                    T_Menu menu = db.T_Menu.FirstOrDefault(M => M.MenuID == StrID);

                    if (menu != null)
                    {
                        menu.MenuName = txtMenuName.Text;//選單名稱
                        menu.NoticeID = Convert.ToInt32(ddlNoticeID.SelectedValue.ToString());//使用者群組權限
                        menu.MenuUrl = txtMenuUrl.Text;
                        menu.ModifiedUser = so.UserID;//密碼
                        menu.ModifiedTime = DateTime.UtcNow.AddHours(8);//密碼更改日期

                        menu.Del = Convert.ToInt32(ddlMenuState.SelectedValue.ToString());//帳戶使用狀態
                        db.SaveChanges();

                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                        logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_Menu", so.UserID));
                        #endregion

                        db.Dispose();
                        MsgBox.MsgBox_In_Ajax("編輯成功", this.Page, "MainMenu.aspx");
                    }
                }
                catch (Exception ex)
                {
                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Update.ToString());
                    logger.Error(ex);
                    #endregion

                    MsgBox.MsgBox_In_Ajax("編輯失敗", this.Page, Request.RawUrl);
                }
                #endregion
            }
            else
            {
                #region 新增
                try
                {
                    using (AFRCDutyEntities db = new AFRCDutyEntities())
                    {
                        T_Menu menu = new T_Menu();

                        menu.MenuName = txtMenuName.Text;//選單名稱
                        menu.NoticeID = Convert.ToInt32(ddlNoticeID.SelectedValue.ToString());
                        menu.MenuUrl = txtMenuUrl.Text;

                        menu.CreateUser = so.UserID;
                        menu.CreateTime = DateTime.UtcNow.AddHours(8);//密碼更改日期
                        menu.Del = Convert.ToInt32(ddlMenuState.SelectedValue.ToString());//帳戶使用狀態

                        db.T_Menu.Add(menu);
                        db.SaveChanges();

                        #region ActionLog
                        string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Insert.ToString());
                        logger.Info(string.Format("資料表:{0};主鍵:{1}", "T_Menu", so.UserID));
                        #endregion

                        MsgBox.MsgBox_In_Ajax("新增成功", this.Page, "MainMenu.aspx");
                    }
                }
                catch (Exception ex)
                {
                    #region ActionLog
                    string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Insert.ToString());
                    logger.Error(ex);
                    #endregion

                    //Logger.ErrorException("新增使用者", ex);
                    MsgBox.MsgBox_In_Ajax("新增失敗", this.Page, Request.RawUrl);
                }
                #endregion
            }

        }



        protected void Unnamed_ServerClick1(object sender, EventArgs e)
        {
            Response.Redirect("MainMenu");
        }
    }
}