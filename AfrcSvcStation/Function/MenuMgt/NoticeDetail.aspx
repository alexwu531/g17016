﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NoticeDetail.aspx.cs" Inherits="AfrcSvcStation.Function.MenuMgt.NoticeDetail"  ValidateRequest="False"%>
<%@ Register Src="~/WebUserControl/WucDate.ascx" TagPrefix="uc1" TagName="WucDate" %>
<%@ Register Src="~/WebUserControl/WucMetaDataControl.ascx" TagPrefix="uc1" TagName="WucMetaDataControl" %>
<%@ Register Src="~/WebUserControl/WucDateRange.ascx" TagPrefix="uc1" TagName="WucDateRange" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

 <link href="../../css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-multiselect.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hfThemeValue" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hfCakeValue" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hfServiceValue" ClientIDMode="Static" runat="server" />

    <div class="row">
        <div class="col-xs-12">
            <div class="page-header">
                <h1>同意事項編輯
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    同意事項編輯
                                </small>
                </h1>
            </div>

            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1">同意事項標題</label>

                    <div class="col-sm-9">
                        <%--<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />--%>
                        <asp:TextBox ID="txtNoticeName" placeholder="" CssClass="col-xs-10 col-sm-5" runat="server"></asp:TextBox>
                    </div>
                </div>

                <%-- <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1">發佈日期</label>
                    <div class="col-sm-4">
                        <uc1:WucDate runat="server" ID="Wucd" />
                    </div>
                </div>--%>

               <%--  <div class="form-group">
                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1">刊登日期</label>

                    <div class="col-sm-4">
                        <div class ="input-group">
                        <uc1:WucDateRange runat="server" ID="WucDateRange1" />
                        <span class="input-group-addon">
                            <i class="fa fa-exchange"></i>
                        </span>
                        <%--<uc2:WucDate runat="server" ID="WucDate2" placeholder="請選擇結束時間" />
                        <uc1:WucDateRange runat="server" ID="WucDateRange2" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1">是否顯示</label>
                    <div class="col-sm-4">
                        <asp:DropDownList ID="ddlIsShow" runat="server" CssClass="form-control">
                            <asp:ListItem Value="x">請選擇</asp:ListItem>
                            <asp:ListItem Value="1">是</asp:ListItem>
                            <asp:ListItem Selected="True" Value="0">否</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>--%>

                <%--<div class="form-group" style="display:none">
                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1">是否顯示於首頁</label>
                    <div class="col-sm-4">
                        <asp:DropDownList ID="ddlIsShowMain" runat="server" CssClass="form-control">
                            <asp:ListItem Value="x">請選擇</asp:ListItem>
                            <asp:ListItem Value="1">是</asp:ListItem>
                            <asp:ListItem Value="0" Selected="True">否</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>--%>

              <%-- <div class="form-group">
                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1">隸屬選單類別</label>
                    <div class="col-sm-4">
                        <asp:DropDownList ID="ddlMenu" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>--%>

                <!--div class="form-group" style="display:none">
                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1">圖片</label>
                    <div class="col-sm-9">
                     <%--   <asp:FileUpload ID="FileUpload" runat="server" />
                        <asp:Image ID="pic" runat="server" Width="100px" />--%>
                    </div>
                </div-->

                <div class="space-4"></div>

                <div class="form-group">
                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1">內容</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtContr" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                    </div>
                </div>

              <%--  <div class="form-group" id="content_index" runat="server">
                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1">分類檢索</label>

                    <div class="col-sm-9">
                        <div id="BakContent">
                            <uc1:WucMetaDataControl runat="server" ID="WucMetaDataControl" />
                        </div>
                    </div>
                </div>--%>

                <div class="space-4"></div>

                                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-9 col-sm-offset-1">
                                        <div class="profile-user-info profile-user-info-striped">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name col-sm-3">創建人員</div>

                                                <div class="profile-info-value col-sm-4">
                                                    <asp:Label ID="lblCreateUser" runat="server"></asp:Label>
                                                    <%--<span class="editable editable-click" id="CreateUser" runat ="server">皓展</span>--%>
                                                </div>

                                                <div class="profile-info-name col-sm-3">上次更新人員</div>

                                                <div class="profile-info-value col-sm-4">
                                                    <asp:Label ID="lblModifyUser" runat="server" Text="Label">系統維護員</asp:Label>
                                                    <%--<span class="editable editable-click" id="LastModifyUser">系統維護員</span>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-9 col-sm-offset-1">
                                        <div class="profile-user-info profile-user-info-striped">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name col-sm-3">創建時間</div>

                                                <div class="profile-info-value col-sm-4">
                                                    <asp:Label ID="lblCreateTime" runat="server"></asp:Label>
                                                    <%--<span class="editable editable-click" id="username">2014/6/26 上午 10:04:43</span>--%>
                                                </div>

                                                <div class="profile-info-name col-sm-3">上次更新時間</div>

                                                <div class="profile-info-value col-sm-4">
                                                    <asp:Label ID="lblModifyTime" runat="server" Text="Label"></asp:Label>
                                                    <%--<span class="editable editable-click" id="username">2014/6/26 上午 10:04:43</span>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <asp:Button ID="btnBack" CssClass="btn btn-info" runat="server" Text="返回" ClientIDMode="Static" OnClick="btnBack_Click" />
                        &nbsp; &nbsp; &nbsp;
                      
                        <asp:Button ID="btnSubmit" CssClass="btn btn-info" runat="server" Text="送出" ClientIDMode="Static" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>

            <div class="hr hr-24"></div>
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
    <script src='<%= ResolveUrl("~/Scripts/ckeditor/ckeditor.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/multiple-fileupload.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-multiselect.min.js") %>'></script>

    <script>

        CKEDITOR.replace('txtContr',
{
    filebrowserBrowseUrl: '../Scripts/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl: '../Scripts/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl: '../Scripts/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl: '../Scripts/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl: '../Scripts/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl: '../Scripts/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash'
});
    </script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            //get dropdownlist
            $('.multiselect').multiselect({
                enableFiltering: true,
                enableHTML: true,
                buttonClass: 'btn btn-white btn-primary',
                templates: {
                    button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
                    ul: '<ul class="multiselect-container dropdown-menu"></ul>',
                    filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                    filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
                    li: '<li><a tabindex="0"><label></label></a></li>',
                    divider: '<li class="multiselect-item divider"></li>',
                    liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
                },

            });


            //set dropdownlist_theme
            var theme = document.getElementById("hfThemeValue").value;
            var str1 = theme.split(",");
            $('#ddlMetaTheme').multiselect('select', str1)

            var cake = document.getElementById("hfCakeValue").value;
            var str2 = cake.split(",");
            $('#ddlMetaCake').multiselect('select', str2);

            var service = document.getElementById("hfServiceValue").value;
            var str3 = service.split(",");
            $('#ddlMetaService').multiselect('select', str3);
        });
    </script>

    <script type="text/javascript">
        $('#btnSubmit').click(function () {

            var selected1 = $("#ddlMetaTheme option:selected");
            var ThemeValue = "";
            var hfThemeValue = document.getElementById("hfThemeValue");
            selected1.each(function () {
                ThemeValue += $(this).val() + ",";
            });
            hfThemeValue.value = ThemeValue;

            var selected2 = $("#ddlMetaCake option:selected");
            var CakeValue = "";
            var hfCakeValue = document.getElementById("hfCakeValue");
            selected2.each(function () {
                CakeValue += $(this).val() + ",";
            });
            hfCakeValue.value = CakeValue;

            var selected3 = $("#ddlMetaService option:selected");
            var ServiceValue = "";
            var hfServiceValue = document.getElementById("hfServiceValue");
            selected3.each(function () {
                ServiceValue += $(this).val() + ",";
            });
            hfServiceValue.value = ServiceValue;

            //            alert("theme:" + ThemeValue + "cake:" + CakeValue + "service:" + ServiceValue);
        });
    </script>

</asp:Content>
