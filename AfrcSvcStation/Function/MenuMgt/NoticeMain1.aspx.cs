﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AfrcSvcStation.App_Code;
using NLog;
using AfrcSvcStation.ModelsForStation;

namespace AfrcSvcStation.Function.MenuMgt
{
    public partial class NoticeMain1 : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        bool IsShow;
        WebMessage MsgBox = new WebMessage();
        protected UserInfo so = new UserInfo();
        int intWebID;
        string[] aryWebBelongDept;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                AFRCDutyEntities db = new AFRCDutyEntities();



                //BindPostType();
                // BindUserDept();
                DataBind();

                if (Session["WEBID"].ToString() != "1")
                {
                    //  livdiv.Style.Add("display", "none");
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBind();
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                try
                {
                    AFRCDutyEntities db = new AFRCDutyEntities();

                    T_Notice Notice = db.T_Notice.FirstOrDefault(c => c.NoticeID.ToString() == e.CommandArgument.ToString());
                    if (Notice != null)
                    {
                        db.T_Notice.Remove(Notice);
                        db.SaveChanges();
                        MsgBox.MsgBox_In_Ajax("刪除完成", this.Page, Request.RawUrl);

                        #region ActionLog
                        string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                        logger.Info(string.Format("資料表:Notice;主鍵{0}", e.CommandArgument.ToString()));
                        #endregion
                    }
                }
                catch (Exception ex)
                {


                }
            }
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("NoticeDetail?sn=" + e.CommandArgument);
        }

        protected void cmdadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("NoticeDetail");
        }

        protected string GetWebID()
        {
            return "1";
        }

        protected void DataBind()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {

                    var NoticeList = db.T_Notice.ToList();

                    DateTime DS = !string.IsNullOrEmpty(WucDate_S.CNDateRange) ? Convert.ToDateTime(WucDate_S.CNDateRange) : Convert.ToDateTime("1911-01-01");
                    DateTime DE = !string.IsNullOrEmpty(WucDate_E.CNDateRange) ? Convert.ToDateTime(WucDate_E.CNDateRange) : Convert.ToDateTime("9999-12-31");
                    // int Unit = Convert.ToInt32(ddlUnit.SelectedValue);



                    //var NewsList_Query =
                    // db.news.Where(
                    //     c => !string.IsNullOrEmpty(WucDate_S.CNDateRange) && !string.IsNullOrEmpty(WucDate_E.CNDateRange) ? c.d >= DS && c.d <= DE : true &&
                    //          !string.IsNullOrEmpty(WucDate_S.CNDateRange) && string.IsNullOrEmpty(WucDate_E.CNDateRange) ? c.d >= DS : true &&
                    //          string.IsNullOrEmpty(WucDate_S.CNDateRange) && !string.IsNullOrEmpty(WucDate_E.CNDateRange) ? c.d <= DE : true &&
                    //          ddlUnit.SelectedValue.ToString() != "0" ? c.unit_sn == Unit : true &&
                    //          ddlIsShow.SelectedValue.ToString() != "x" ? c.is_show == IsShow : (c.is_show == true || c.is_show == false) &&
                    //          c.WebID == WebID
                    //          ).ToList();

                    List<T_Notice> Notice_Query = new List<T_Notice>();
                    if (intWebID == 1)
                    {
                        Notice_Query = (from s in db.T_Notice
                                          select s).ToList();



                        AspNetPager1.AlwaysShow = true;
                        AspNetPager1.RecordCount = Notice_Query.Count();

                        repNewList.DataSource = Notice_Query.OrderByDescending(o => o.NoticeID).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize)
                                                 .Take(AspNetPager1.PageSize);
                        repNewList.DataBind();

                    }
                    #region ActionLog
                    string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(string.Format("資料表:news;"));
                    #endregion
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataBind();
        }

        ////新聞訊息類別
        //protected void BindPostType()
        //{
        //    using (AFRCEntities db = new AFRCEntities())
        //    {
        //        try
        //        {
        //            var Group = (from c in db.code
        //                         join ct in db.code_type on c.code_type_sn equals ct.sn
        //                         where ct.id == "news_type"
        //                         select new { CodeTypeID = ct.id, CodeID = c.id, Caption = c.caption }).ToList();

        //            if (Group != null)
        //            {
        //                ddlNewsType.DataSource = Group;
        //                ddlNewsType.DataTextField = "Caption";
        //                ddlNewsType.DataValueField = "CodeID";
        //                ddlNewsType.DataBind();


        //                ddlNewsType.Items.Insert(0, new ListItem("請選擇群組", "0"));
        //            }
        //            //#region ActionLog
        //            //string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
        //            ////取得IP
        //            //if (Request.ServerVariables["HTTP_VIA"] != null)
        //            //{
        //            //    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        //            //}

        //            //#endregion
        //        }
        //        catch (Exception ex)
        //        {
        //            //#region ActionLog
        //            //string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
        //            ////取得IP
        //            //if (Request.ServerVariables["HTTP_VIA"] != null)
        //            //{
        //            //    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        //            //}
        //            //GlobalDiagnosticsContext.Set("addr", tClientIP);
        //            //GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
        //            //GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
        //            //logger.Error(ex);
        //            //#endregion
        //        }
        //    }
        //}

        /*
    /// <summary>
    /// 下拉選單單位資料綁定_使用者部門
    /// </summary>
    public void BindUserDept()
    {
        try
        {
            using (AFRCDutyEntities db = new AFRCDutyEntities())
            {
                List<T_Dept> Dept = new List<T_Dept>();

                if (GetWebID() == "1")
                {
                    Dept = (from D in db.T_Dept
                            orderby D.sort
                            where D.belong_sn != 0 && D.del == 1
                            select D).ToList();
                }
                else
                {
                    Dept = (from D in db.T_Dept
                            orderby D.sort
                            where D.belong_sn != 0 && aryWebBelongDept.Contains(D.sn.ToString())
                            select D).ToList();
                }

                if (Dept != null)
                {
                    ddlUnit.DataSource = Dept;
                    ddlUnit.DataTextField = "caption";
                    ddlUnit.DataValueField = "sn";
                    ddlUnit.DataBind();

                    ddlUnit.Items.Insert(0, new ListItem("請選擇部門", "0"));
                }


                //#region ActionLog
                //string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                ////取得IP
                //if (Request.ServerVariables["HTTP_VIA"] != null)
                //{
                //    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                //}
                //GlobalDiagnosticsContext.Set("addr", tClientIP);
                //GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                //GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                //logger.Info(Dept.ToString());
                //#endregion
            }
        }
        catch (Exception ex)
        {
            //#region ActionLog
            //string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
            ////取得IP
            //if (Request.ServerVariables["HTTP_VIA"] != null)
            //{
            //    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            //}
            //GlobalDiagnosticsContext.Set("addr", tClientIP);
            //GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
            //GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
            //logger.Error(ex);
            //#endregion
        }
    }
    */

        /// <summary>
        /// 儲存首頁訊息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            AFRCDutyEntities db = new AFRCDutyEntities();

            try
            {
                /*
                    so = (UserInfo)Session["UserData"];

                    T_LiveNews livenews = new T_LiveNews();

                    livenews.LiveNewsContent = txtlivenews.Text;

                    livenews.d1 = Convert.ToDateTime(WucDateRange1.CNDateRange);

                    livenews.d2 = Convert.ToDateTime(WucDateRange2.CNDateRange);

                    livenews.CreateUser = int.Parse(so.UserPKID);

                    livenews.CreateTime = DateTime.UtcNow.AddHours(8);

                    db.T_LiveNews.Add(livenews);

                    db.SaveChanges();
                    #region ActionLog
                    string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(string.Format("資料表:T_LiveNews;主鍵{0};內容:{1}", livenews.LiveNewsID.ToString(), livenews.LiveNewsContent));
                    #endregion
                    MsgBox.MsgBox_In_Ajax("更新首頁Live訊息完成", this.Page);
                    BindLiveNews();
                    */
            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Dispose();
            }
        }

    }
}