﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;

namespace AfrcSvcStation.Function.SysMgt
{
    public partial class SysLog : System.Web.UI.Page
    {
        WebMessage Msgbox = new WebMessage();
        protected string userID = HttpContext.Current.Request.QueryString["UserID"];
        protected UserInfo so = new UserInfo();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    so = (UserInfo)Session["UserData"];
                    //VisitorLog.AddAction(so.UserID, VisitorLog.ActionType.pageview.ToString(), "系統管理>群組權限管理");
                }
                catch (Exception ex)
                {
                    //Logger.ErrorException("Log紀錄", ex);

                }

                so = (UserInfo)Session["UserData"];
                BindUserDept();//下拉選單綁定_使用者部門
                BindUN(2);//下拉選單與部門綁定_使用者名稱
                BindUserName(); //下拉選單綁定_使用者名稱
                DataForBind();

            }
        }

        private void DataForBind()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    try
                    {
                        DateTime DS = DateTime.Now;
                        DateTime DE = DateTime.Now;
                        int UserID = Convert.ToInt32(ddlUserName.SelectedValue.ToString());
                        string D_S = !string.IsNullOrEmpty(WucDateRange1.CNDateRange) ? WucDateRange1.CNDateRange : "";
                        if (D_S != "")
                        {
                            DS = Convert.ToDateTime(WucDateRange1.CNDateRange);
                        }
                        string D_E = !string.IsNullOrEmpty(WucDateRange2.CNDateRange) ? WucDateRange2.CNDateRange : "";
                        if (D_E != "")
                        {
                            DE = Convert.ToDateTime(WucDateRange2.CNDateRange).AddDays(+1);
                        }

                        var UserList = (from s in db.T_SysLog
                                        join u in db.T_SysUser on s.userid equals u.ID.ToString() into tempu
                                        from u in tempu.DefaultIfEmpty()
                                        join d in db.T_SysDept on u.DeptID equals d.sn into tempd
                                        from d in tempd.DefaultIfEmpty()


                                        where ((ddlUserDept.SelectedValue.ToString() != "0") ? d.sn.ToString() == ddlUserDept.SelectedValue.ToString() : true) &&
                                              ((ddlUserName.SelectedValue.ToString() != "0") ? u.ID == UserID : true) &&
                                              ((ddlAction.SelectedValue.ToString() != "0") ? s.action == ddlAction.SelectedValue.ToString() : true) &&
                                              ((ddlLog.SelectedValue.ToString() == "1") ? s.stacktrace != string.Empty : true) &&
                                                 ((ddlLog.SelectedValue.ToString() == "2") ? s.stacktrace == string.Empty : true) &&
                                              ((!string.IsNullOrEmpty(WucDateRange1.CNDateRange) ? s.time_stamp >= DS : true) &&
                                              (!string.IsNullOrEmpty(WucDateRange2.CNDateRange) ? s.time_stamp <= DE : true))


                                        select new
                                        {
                                            Addr = s.addr,
                                            UserName = u.UserName,
                                            time_stamp = s.time_stamp,
                                            Host = s.host,
                                            Type = s.type,
                                            Message = s.message,
                                            Action = s.action,
                                            Stacktrace = s.stacktrace,
                                            Detail = s.detail,


                                        }).ToList();

                        AspNetPager1.AlwaysShow = true;
                        AspNetPager1.RecordCount = UserList.Count();

                        repUserView.DataSource = UserList.OrderBy(o => o.time_stamp).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize)
                                                 .Take(AspNetPager1.PageSize);
                        repUserView.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void ExcelBind()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    try
                    {
                        DateTime DS = DateTime.Now;
                        DateTime DE = DateTime.Now;
                        int UserID = Convert.ToInt32(ddlUserName.SelectedValue.ToString());
                        string D_S = !string.IsNullOrEmpty(WucDateRange1.CNDateRange) ? WucDateRange1.CNDateRange : "";
                        if (D_S != "")
                        {
                            DS = Convert.ToDateTime(WucDateRange1.CNDateRange);
                        }
                        string D_E = !string.IsNullOrEmpty(WucDateRange2.CNDateRange) ? WucDateRange2.CNDateRange : "";
                        if (D_E != "")
                        {
                            DE = Convert.ToDateTime(WucDateRange2.CNDateRange).AddDays(+1);
                        }

                        var UserList = (from s in db.T_SysLog
                                        join u in db.T_SysUser on s.userid equals u.ID.ToString() into tempu
                                        from u in tempu.DefaultIfEmpty()
                                        join d in db.T_SysDept on u.DeptID equals d.sn into tempd
                                        from d in tempd.DefaultIfEmpty()


                                        where ((ddlUserDept.SelectedValue.ToString() != "0") ? d.sn.ToString() == ddlUserDept.SelectedValue.ToString() : true) &&
                                              ((ddlUserName.SelectedValue.ToString() != "0") ? u.ID == UserID : true) &&
                                              ((ddlAction.SelectedValue.ToString() != "0") ? s.action == ddlAction.SelectedValue.ToString() : true) &&
                                            ((ddlLog.SelectedValue.ToString() == "1") ? s.stacktrace != string.Empty : true) &&
                                                 ((ddlLog.SelectedValue.ToString() == "2") ? s.stacktrace == string.Empty : true) &&
                                              ((!string.IsNullOrEmpty(WucDateRange1.CNDateRange) ? s.time_stamp >= DS : true) &&
                                              (!string.IsNullOrEmpty(WucDateRange2.CNDateRange) ? s.time_stamp <= DE : true))


                                        select new
                                        {
                                            Addr = s.addr,
                                            UserName = u.UserName,
                                            time_stamp = s.time_stamp,
                                            Host = s.host,
                                            Type = s.type,
                                            Message = s.message,
                                            Action = s.action,
                                            Stacktrace = s.stacktrace,
                                            Detail = s.detail,


                                        }).ToList();

                        AspNetPager1.AlwaysShow = true;
                        AspNetPager1.RecordCount = UserList.Count();

                        Repeater1.DataSource = UserList.OrderBy(o => o.time_stamp);
                        Repeater1.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {

            }

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Download.xls");
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "UTF-8";
            Response.ContentType = "application/vnd.ms-excel.12";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Panel2.RenderControl(hw);
            string output = "<table>" + sw.ToString() + "</table>";
            Response.Output.Write(output);
            Response.Flush();
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataForBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataForBind();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            ExcelBind();
        }

        protected void cmdadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AccountDetail");
        }

        /// <summary>
        /// 下拉選單單位資料綁定_使用者部門
        /// </summary>
        public void BindUserDept()
        {
            try
            {
                using (AFRCDutyEntities db = new AFRCDutyEntities())
                {
                    var Dept = (from D in db.T_SysDept
                                orderby D.sort
                                where D.del == 1 && D.sn != 0
                                select D).ToList();

                    ddlUserDept.DataSource = Dept;
                    ddlUserDept.DataTextField = "caption_s";
                    ddlUserDept.DataValueField = "sn";
                    ddlUserDept.DataBind();

                    ddlUserDept.Items.Insert(0, new ListItem("請選擇單位", "0"));
                }
            }
            catch (Exception ex)
            {

            }
        }


        /// <summary>
        /// 下拉選單使用者名稱資料綁定
        /// </summary>
        public void BindUserName()
        {
            using (AFRCDutyEntities db = new AFRCDutyEntities())
            {
                try
                {

                    var User = (from U in db.T_SysUser
                                where U.DeptID != 0
                                select U).ToList();

                    ddlUserName.DataSource = User;
                    ddlUserName.DataTextField = "UserName";
                    ddlUserName.DataValueField = "ID";
                    ddlUserName.DataBind();

                    ddlUserName.Items.Insert(0, new ListItem("請選擇使用者", "0"));
                }
                catch (Exception ex)
                {

                }
            }
        }

        /// <summary>
        /// 下拉選單單位資料綁定_使用者名稱
        /// </summary>
        public void BindUN(int strDeptID)
        {
            using (AFRCDutyEntities db = new AFRCDutyEntities())
            {
                try
                {

                    var User = (from U in db.T_SysUser
                                where U.DeptID == strDeptID
                                select U).ToList();

                    ddlUserName.DataSource = User;
                    ddlUserName.DataTextField = "UserName";
                    ddlUserName.DataValueField = "ID";
                    ddlUserName.DataBind();

                    ddlUserName.Items.Insert(0, new ListItem("請選擇使用者", "0"));
                }
                catch (Exception ex)
                {

                }
            }
        }



        protected void ddlUserDept_SelectedIndexChanged1(object sender, EventArgs e)
        {
            int SelectedValue = Convert.ToInt32(ddlUserDept.SelectedValue.ToString());
            BindUN(SelectedValue);
        }


    }
}