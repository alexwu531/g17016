﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SysLog.aspx.cs" Inherits="AfrcSvcStation.Function.SysMgt.SysLog" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../../WebUserControl/WucDate.ascx" TagName="WucDate" TagPrefix="uc1" %>
<%@ Register Src="~/WebUserControl/WucDate.ascx" TagPrefix="uc2" TagName="WucDate" %>
<%@ Register Src="~/WebUserControl/WucDateRange.ascx" TagPrefix="uc1" TagName="WucDateRange" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            height: 20px;
        }
        .table{
              word-break: break-all 
        }
  
    </style>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>

    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="page-header">
            <h1>稽核紀錄
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    LOG查詢
                                </small>
            </h1>
        </div>

        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <!-- search-area  BEGINS -->
            <div class="widget-box">
                <div class="widget-header widget-header-small">
                    <h5 class="widget-title lighter">查詢條件</h5>
                </div>

                <div class="widget-body">
                    <div class="widget-main">

                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-lg-6 ">

                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="input-group">
                                            <span class="input-group-addon">單位
                                            </span>
                                            <asp:DropDownList ID="ddlUserDept" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlUserDept_SelectedIndexChanged1">
                                                <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                                                <asp:ListItem Value="1">使用中</asp:ListItem>
                                                <asp:ListItem Value="2">停用</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="hr"></div>

                                        <div class="input-group">
                                            <span class="input-group-addon">使用者名稱
                                            </span>
                                            <asp:DropDownList ID="ddlUserName" runat="server" CssClass="form-control">
                                                <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                                                <asp:ListItem Value="1">使用中</asp:ListItem>
                                                <asp:ListItem Value="2">停用</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlUserDept" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <div class="hr"></div>

                                <div class="input-group">
                                    <span class="input-group-addon">動作
                                    </span>
                                    <asp:DropDownList ID="ddlAction" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0">請選擇</asp:ListItem>
                                        <asp:ListItem Value="Login">登入</asp:ListItem>
                                        <asp:ListItem Value="Logout">登出</asp:ListItem>
                                        <asp:ListItem Value="Insert">新增</asp:ListItem>
                                        <asp:ListItem Value="Update">修改</asp:ListItem>
                                        <asp:ListItem Value="Delete">刪除</asp:ListItem>
                                        <asp:ListItem Value="Query">查詢</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="hr"></div>

                                <div class="input-group">
                                    <span class="input-group-addon">Log類別
                                    </span>
                                    <asp:DropDownList ID="ddlLog" runat="server" CssClass="form-control">
                                    
                                        <asp:ListItem Value="1">錯誤Log</asp:ListItem>
                                        <asp:ListItem Value="2" Selected="True">操作Log</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="hr"></div>

                                <div class="input-group">
                                    <span class="input-group-addon">時間範圍
                                    </span>
                                    <%--<uc2:WucDate runat="server" ID="WucDate1" placeholder="請選擇開始時間" />--%>
                                    <uc1:WucDateRange runat="server" ID="WucDateRange1" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-exchange"></i>
                                    </span>
                                    <%--<uc2:WucDate runat="server" ID="WucDate2" placeholder="請選擇結束時間" />--%>
                                    <uc1:WucDateRange runat="server" ID="WucDateRange2" />
                                </div>
                                <div class="hr"></div>
                                <asp:Button ID="btnSearch" runat="server" Text="查詢" CausesValidation="False" CommandName="Search" CssClass="btn btn-purple btn-sm" OnClick="btnSearch_Click" />
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CausesValidation="False" CommandName="Download" CssClass="btn btn-success btn-sm" OnClick="btnExcel_Click" />
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            <div class="hr hr-18 dotted hr-double"></div>
              <div style="display: none">
                                <asp:Panel ID="Panel2" runat="server">
                                    <table id="simpletable" class="table  table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="">ip位置</th>
                                            <th style="">使用者</th>
                                            <th style="">紀錄時間</th>
                                            <th style="">host</th>
                                            <th style="">類型</th>
                                            <th style="">訊息</th>
                                            <th style="">動作</th>
                                            <th style="">stacktrace</th>
                                            <th style="">detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="Repeater1" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="center"><%#Eval("addr")%></td>
                                                    <td align="center"><%#Eval("UserName")%></td>
                                                    <td align="center"><%#Eval("time_stamp")%></td>
                                                    <td align="center"><%#Eval("host")%></td>
                                                    <td align="center"><%#Eval("type")%></td>
                                                    <td align="center"><%#Eval("message")%></td>
                                                    <td align="center"><%#Eval("action")%></td>
                                                    <td align="center"><%#Eval("stacktrace")%></td>
                                                    <td align="center"><%#Eval("detail")%></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </tbody>
                                </table>
                                </asp:Panel>
                            </div>

               <asp:UpdatePanel ID="up1" runat="server">
                    <ContentTemplate>
          


             <div class="col-xs-12">
                     <div class="dataTables_wrapper form-inline no-footer">
                          
              
                                <table id="simpletable2" class="table  table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                               <th style="">ip位置</th>
                                            <th style="">使用者</th>
                                            <th style="">紀錄時間</th>
                                            <th style="">host</th>
                                            <th style="">類型</th>
                                            <th style="width:30%;">訊息</th>
                                          <th style="width:10%;">動作</th>
                                     <%--     <th style="width:50px;">stacktrace</th>
                                             <th style="width:50px;">detail</th>--%>
                                        </tr>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="repUserView" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="center"><%#Eval("addr")%></td>
                                                    <td align="center"><%#Eval("UserName")%></td>
                                                    <td align="center"><%#Eval("time_stamp")%></td>
                                                    <td align="center"><%#Eval("host")%></td>
                                                    <td align="center"><%#Eval("type")%></td>
                                                    <td align="center"><%#Eval("message")%></td>
                                                    <td align="center"><%#Eval("action")%></td>
                                            <%--        <td align="center"><%#Eval("stacktrace")%></td>
                                                    <td align="center"><%#Eval("detail")%></td>--%>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </tbody>
                                </table>
                    
                            <div class="row">

                                <div class="col-xs-12">
                                    <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                                            ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                                        </webdiyer:AspNetPager>

                                    </div>
                                </div>
                            </div>

                        </div>
             </div>

                    


                        <!-- PAGE CONTENT ENDS -->
           
    <!-- /.col -->




                    </ContentTemplate>

                </asp:UpdatePanel>
            </div>
    
         </div>
    <%--<!-- #dialog-message -->--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
</asp:Content>
