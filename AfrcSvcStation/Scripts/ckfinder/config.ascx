<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="false" Inherits="CKFinder.Settings.ConfigFile" %>
<%@ Import Namespace="CKFinder.Settings" %>

<script runat="server">

    /**
	 * This function must check the user session to be sure that he/she is
	 * authorized to upload and access files using CKFinder.
	 */
    public override bool CheckAuthentication()
    {
        // WARNING : DO NOT simply return "true". By doing so, you are allowing
        // "anyone" to upload and list the files in your server. You must implement
        // some kind of session validation here. Even something very simple as...
        //
        //		return ( Session[ "IsAuthorized" ] != null && (bool)Session[ "IsAuthorized" ] == true );
        //
        // ... where Session[ "IsAuthorized" ] is set to "true" as soon as the
        // user logs on your system.

        //return false;

        //暫時不驗證
        return true;
    }

    /**
	 * All configuration settings must be defined here.
	 */
    public override void SetConfig()
    {
        // Paste your license name and key here. If left blank, CKFinder will
        // be fully functional, in Demo Mode.
        LicenseName = "";
        LicenseKey = "";

        // The base URL used to reach files in CKFinder through the browser.
        string webid = Session["WEBID"].ToString();


        switch (webid)
        {
            case "1":
                //指揮部
                BaseUrl = "http://afrc.mnd.gov.tw/AFRCWEB/Uploads/";
                break;
            case "2":
                //高雄市
                BaseUrl = "http://afrc.mnd.gov.tw/KaohSiung/Uploads/";
                break;
            case "3":
                //基隆市
                BaseUrl = "http://afrc.mnd.gov.tw/KeeLung/Uploads/";
                break;
            case "4":
                //宜蘭縣
                BaseUrl = "http://afrc.mnd.gov.tw/Yilan/Uploads/";
                break;
            case "5":
                //桃園市
                BaseUrl = "http://afrc.mnd.gov.tw/Taoyuan/Uploads/";
                break;
            case "6":
                //台北市
                BaseUrl = "http://afrc.mnd.gov.tw/Taipei/Uploads/";
                break;
            case "7":
                //新北市
                BaseUrl = "http://afrc.mnd.gov.tw/TaipeiCity/Uploads/";
                break;
            case "8":
                //新竹
                BaseUrl = "http://afrc.mnd.gov.tw/Hsinchu/Uploads/";
                break;
            case "9":
                //花蓮
                BaseUrl = "http://afrc.mnd.gov.tw/HunLien/Uploads/";
                break;
            case "10":
                //苗栗
                BaseUrl = "http://afrc.mnd.gov.tw/Miaoli/Uploads/";
                break;
            case "11":
                //台中市
                BaseUrl = "http://afrc.mnd.gov.tw/TaichungCity/Uploads/";
                break;
            case "12":
                //南投縣
                BaseUrl = "http://afrc.mnd.gov.tw/Nantou/Uploads/";
                break;
            case "13":
                //彰化縣
                BaseUrl = "http://afrc.mnd.gov.tw/Changhua/Uploads/";
                break;
            case "14":
                //雲林縣
                BaseUrl = "http://afrc.mnd.gov.tw/Yunlin/Uploads/";
                break;
            case "15":
                //嘉義
                BaseUrl = "http://afrc.mnd.gov.tw/Chiayi/Uploads/";
                break;
            case "16":
                //台南市
                BaseUrl = "http://afrc.mnd.gov.tw/Tainancity/Uploads/";
                break;
            case "17":
                //屏東
                BaseUrl = "http://afrc.mnd.gov.tw/PingTung/Uploads/";
                break;
            case "18":
                //台東縣
                BaseUrl = "http://afrc.mnd.gov.tw/TaiTung/Uploads/";
                break;
            case "19":
                //澎湖縣
                BaseUrl = "http://afrc.mnd.gov.tw/Penghu/Uploads/";
                break;
            case "20":
                //忠烈祠
                BaseUrl = "http://afrc.mnd.gov.tw/faith_martyr/Uploads/";
                break;
            case "21":
                //示範公墓
                BaseUrl = "http://afrc.mnd.gov.tw/Cemetery/Uploads/";
                break;
            case "22":
                //忠烈英文
                BaseUrl = "http://afrc.mnd.gov.tw/faith_martyr/Uploads/";
                break;
            case "23":
                //忠烈日文
                BaseUrl = "http://afrc.mnd.gov.tw/faith_martyr/Uploads/";
                break;
            case "24":
                //忠烈韓文
                BaseUrl = "http://afrc.mnd.gov.tw/faith_martyr/Uploads/";
                break;
            case "25":
                //公墓英文
                BaseUrl = "http://afrc.mnd.gov.tw/Cemetery/Uploads/";
                break;
            case "26":
                //主網英文
                BaseUrl = "http://afrc.mnd.gov.tw/AFRCWEB/Uploads/";
                break;
            case "27":
                //桃園管理組
                BaseUrl = "http://afrc.mnd.gov.tw/AFRCTaoyuanMgt/Uploads/";
                break;
        }



        // The phisical directory in the server where the file will end up. If
        // blank, CKFinder attempts to resolve BaseUrl.

        switch (webid)
        {
            case "1":
                //指揮部
                BaseDir = @"E:\WEB\AFRCWeb\Uploads\";
                break;
            case "2":
                //高雄市
                BaseDir = @"E:\WEB\KaohSiung\Uploads\";
                break;
            case "3":
                //基隆市
                BaseDir = @"E:\WEB\KeeLung\Uploads\";
                break;
            case "4":
                //宜蘭縣
                BaseDir = @"E:\WEB\Yilan\Uploads\";
                break;
            case "5":
                //桃園市
                BaseDir = @"E:\WEB\Taoyuan\Uploads\";
                break;
            case "6":
                //台北市
                BaseDir = @"E:\WEB\Taipei\Uploads\";
                break;
            case "7":
                //新北市
                BaseDir = @"E:\WEB\TaipeiCity\Uploads\";
                break;
            case "8":
                //新竹
                BaseDir = @"E:\WEB\Hsinchu\Uploads\";
                break;
            case "9":
                //花蓮
                BaseDir = @"E:\WEB\HunLien\Uploads\";
                break;
            case "10":
                //苗栗
                BaseDir = @"E:\WEB\Miaoli\Uploads\";
                break;
            case "11":
                //台中市
                BaseDir = @"E:\WEB\TaichungCity\Uploads\";
                break;
            case "12":
                //南投縣
                BaseDir = @"E:\WEB\Nantou\Uploads\";
                break;
            case "13":
                //彰化縣
                BaseDir = @"E:\WEB\Changhua\Uploads\";
                break;
            case "14":
                //雲林縣
                BaseDir = @"E:\WEB\Yunlin\Uploads\";
                break;
            case "15":
                //嘉義
                BaseDir = @"E:\WEB\Chiayi\Uploads\";
                break;
            case "16":
                //台南市
                BaseDir = @"E:\WEB\Tainancity\Uploads\";
                break;
            case "17":
                //屏東
                BaseDir = @"E:\WEB\PingTung\Uploads\";
                break;
            case "18":
                //台東縣
                BaseDir = @"E:\WEB\TaiTung\Uploads\";
                break;
            case "19":
                //澎湖縣
                BaseDir = @"E:\WEB\Penghu\Uploads\";
                break;
            case "20":
                //忠烈祠
                BaseDir = @"E:\WEB\faith_martyr\Uploads\";
                break;
            case "21":
                //示範公墓
                BaseDir = @"E:\WEB\Cemetery\Uploads\";
                break;
            case "22":
                //忠烈英文
                BaseDir = @"E:\WEB\faith_martyr\Uploads\";
                break;
            case "23":
                //忠烈日文
                BaseDir = @"E:\WEB\faith_martyr\Uploads\";
                break;
            case "24":
                //忠烈韓文
                BaseDir = @"E:\WEB\faith_martyr\Uploads\";
                break;
            case "25":
                //公墓英文
                BaseDir = @"E:\WEB\Cemetery\Uploads\";
                break;
            case "26":
                //主網英文
                BaseDir = @"E:\WEB\AFRCWeb\Uploads\";
                break;
            case "27":
                //桃園管理組
                BaseDir = @"E:\WEB\AFRCTaoyuanMgt\Uploads\";
                break;
        }

        // Optional: enable extra plugins (remember to copy .dll files first).
        Plugins = new string[] {
			// "CKFinder.Plugins.FileEditor, CKFinder_FileEditor",
			// "CKFinder.Plugins.ImageResize, CKFinder_ImageResize",
			// "CKFinder.Plugins.Watermark, CKFinder_Watermark"
		};
        // Settings for extra plugins.
        PluginSettings = new Hashtable();
        PluginSettings.Add("ImageResize_smallThumb", "90x90");
        PluginSettings.Add("ImageResize_mediumThumb", "120x120");
        PluginSettings.Add("ImageResize_largeThumb", "180x180");
        // Name of the watermark image in plugins/watermark folder
        PluginSettings.Add("Watermark_source", "logo.gif");
        PluginSettings.Add("Watermark_marginRight", "5");
        PluginSettings.Add("Watermark_marginBottom", "5");
        PluginSettings.Add("Watermark_quality", "90");
        PluginSettings.Add("Watermark_transparency", "80");

        // Thumbnail settings.
        // "Url" is used to reach the thumbnails with the browser, while "Dir"
        // points to the physical location of the thumbnail files in the server.
        Thumbnails.Url = BaseUrl + "_thumbs/";
        if (BaseDir != "")
        {
            Thumbnails.Dir = BaseDir + "_thumbs/";
        }
        Thumbnails.Enabled = true;
        Thumbnails.DirectAccess = false;
        Thumbnails.MaxWidth = 100;
        Thumbnails.MaxHeight = 100;
        Thumbnails.Quality = 80;

        // Set the maximum size of uploaded images. If an uploaded image is
        // larger, it gets scaled down proportionally. Set to 0 to disable this
        // feature.
        Images.MaxWidth = 1600;
        Images.MaxHeight = 1200;
        Images.Quality = 80;

        // Indicates that the file size (MaxSize) for images must be checked only
        // after scaling them. Otherwise, it is checked right after uploading.
        CheckSizeAfterScaling = true;

        // Increases the security on an IIS web server.
        // If enabled, CKFinder will disallow creating folders and uploading files whose names contain characters
        // that are not safe under an IIS 6.0 web server.
        DisallowUnsafeCharacters = true;

        // If CheckDoubleExtension is enabled, each part of the file name after a dot is
        // checked, not only the last part. In this way, uploading foo.php.rar would be
        // denied, because "php" is on the denied extensions list.
        // This option is used only if ForceSingleExtension is set to false.
        CheckDoubleExtension = true;

        // Due to security issues with Apache modules, it is recommended to leave the
        // following setting enabled. It can be safely disabled on IIS.
        ForceSingleExtension = true;

        // For security, HTML is allowed in the first Kb of data for files having the
        // following extensions only.
        HtmlExtensions = new string[] { "html", "htm", "xml", "js" };

        // Folders to not display in CKFinder, no matter their location. No
        // paths are accepted, only the folder name.
        // The * and ? wildcards are accepted.
        // By default folders starting with a dot character are disallowed.
        HideFolders = new string[] { ".*", "CVS" };

        // Files to not display in CKFinder, no matter their location. No
        // paths are accepted, only the file name, including extension.
        // The * and ? wildcards are accepted.
        HideFiles = new string[] { ".*" };

        // Perform additional checks for image files.
        SecureImageUploads = true;

        // Enables protection in the connector.
        // The default CSRF protection mechanism is based on double submit cookies, where
        // connector checks if the request contains a valid token that matches the token
        // sent in the cookie
        //
        // https://www.owasp.org/index.php/Cross-Site_Request_Forgery_%28CSRF%29_Prevention_Cheat_Sheet#Double_Submit_Cookies
        EnableCsrfProtection = true;

        // The session variable name that CKFinder must use to retrieve the
        // "role" of the current user. The "role" is optional and can be used
        // in the "AccessControl" settings (bellow in this file).
        RoleSessionVar = "CKFinder_UserRole";

        // ACL (Access Control) settings. Used to restrict access or features
        // to specific folders.
        // Several "AccessControl.Add()" calls can be made, which return a
        // single ACL setting object to be configured. All properties settings
        // are optional in that object.
        // Subfolders inherit their default settings from their parents' definitions.
        //
        //	- The "Role" property accepts the special "*" value, which means
        //	  "everybody".
        //	- The "ResourceType" attribute accepts the special value "*", which
        //	  means "all resource types".
        AccessControl acl = AccessControl.Add();
        acl.Role = "*";
        acl.ResourceType = "*";
        acl.Folder = "/";

        acl.FolderView = true;
        acl.FolderCreate = true;
        acl.FolderRename = true;
        acl.FolderDelete = true;

        acl.FileView = true;
        acl.FileUpload = true;
        acl.FileRename = true;
        acl.FileDelete = true;

        // Resource Type settings.
        // A resource type is nothing more than a way to group files under
        // different paths, each one having different configuration settings.
        // Each resource type name must be unique.
        // When loading CKFinder, the "type" querystring parameter can be used
        // to display a specific type only. If "type" is omitted in the URL,
        // the "DefaultResourceTypes" settings is used (may contain the
        // resource type names separated by a comma). If left empty, all types
        // are loaded.

        // ==============================================================================
        // ATTENTION: Flash files with `swf' extension, just like HTML files, can be used
        // to execute JavaScript code and to e.g. perform an XSS attack. Grant permission
        // to upload `.swf` files only if you understand and can accept this risk.
        // ==============================================================================

        DefaultResourceTypes = "";

        ResourceType type;

        type = ResourceType.Add("Files");
        type.Url = BaseUrl + "files/";
        type.Dir = BaseDir == "" ? "" : BaseDir + "files/";
        type.MaxSize = 0;
        type.AllowedExtensions = new string[] { "7z", "aiff", "asf", "avi", "bmp", "csv", "doc", "docx", "fla", "flv", "gif", "gz", "gzip", "jpeg", "jpg", "mid", "mov", "mp3", "mp4", "mpc", "mpeg", "mpg", "ods", "odt", "pdf", "png", "ppt", "pptx", "pxd", "qt", "ram", "rar", "rm", "rmi", "rmvb", "rtf", "sdc", "sitd", "swf", "sxc", "sxw", "tar", "tgz", "tif", "tiff", "txt", "vsd", "wav", "wma", "wmv", "xls", "xlsx", "zip" };
        type.DeniedExtensions = new string[] { };

        type = ResourceType.Add("Images");
        type.Url = BaseUrl + "images/";
        type.Dir = BaseDir == "" ? "" : BaseDir + "images/";
        type.MaxSize = 0;
        type.AllowedExtensions = new string[] { "bmp", "gif", "jpeg", "jpg", "png" };
        type.DeniedExtensions = new string[] { };

        type = ResourceType.Add("Flash");
        type.Url = BaseUrl + "flash/";
        type.Dir = BaseDir == "" ? "" : BaseDir + "flash/";
        type.MaxSize = 0;
        type.AllowedExtensions = new string[] { "swf", "flv" };
        type.DeniedExtensions = new string[] { };
    }

</script>
