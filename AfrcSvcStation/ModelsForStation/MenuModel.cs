﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AfrcSvcStation.ModelsForStation
{

    public class MenuModel
    {
        //選單ID
        public string MenuID { get; set; }
        //選單名稱
        public string MenuNane { get; set; }
        //選單階層
        public string MenuLevel { get; set; }

        //選單類別
        public string MenuType { get; set; }

        public string WebID { get; set; }
    }
}