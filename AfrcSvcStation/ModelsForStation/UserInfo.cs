﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AfrcSvcStation.ModelsForStation
{
    [Serializable]
    public class UserInfo
    {
        //管理者姓名
        public string UserName { get; set; }
        //管理者ID
        public string UserID { get; set; }

        public string UserPKID { get; set; }

        public string UserGroup { get; set; }

        public string UserDept { get; set; }

        //登入者IP
        public string IP { get; set; }
    }
}