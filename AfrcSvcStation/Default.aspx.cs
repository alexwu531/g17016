﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Optimization;
using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;
namespace AfrcSvcStation
{
    public partial class Default : System.Web.UI.Page
    {
        protected string webid = HttpContext.Current.Request.QueryString["webid"];
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["WEBID"] = int.Parse(Common.Decrypt(webid));

            Response.Redirect("/Function/Account/AccountMain");
        }
    }
}