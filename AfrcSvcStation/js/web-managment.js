﻿/*選單管理功能*/
var parentrow;
function addnewrow(obj) {
    var menuid = $(obj).attr('id').split("_")[1];
    var formData = JSON.stringify({ mid: menuid });
    parentrow = $(obj).parent().parent();
    $.ajax({
        type: "POST",
        url: 'WebManagment.aspx/AddChild',
        data: formData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: "false",

        success: function (msg) {
            var menu = msg.d[0];
            if (menu.length != 0) {
                $(parentrow).after('<tr>    <td align="center">' + menu.MenuID + '</td>  <td ><a href="#">' + menu.MenuNane + '</a></td>  <td align="center"><span class="label label-sm label-inverse">不刊登</span></td>  <td align="center"></td></tr>');
            }
            else {

            }
            $(".iframe").colorbox({
                iframe: true, width: "100%", height: "100%"
            });


        },
        Error: function (x, e) {
            console.log(x);
        }
    });
}

function addnewrow(obj, tableid) {
    var menuid = $(obj).attr('id').split("_")[1];
    var formData = JSON.stringify({ mid: menuid });

    $.ajax({
        type: "POST",
        url: 'WebManagment.aspx/AddChild',
        data: formData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: "false",

        success: function (msg) {
            var menu = msg.d[0];
            if (menu.length != 0) {
                if ($("#" + tableid + " > tbody > tr:first").length > 0) {
                    $("#" + tableid + " > tbody > tr:first").before('<tr>    <td align="center">' + menu.MenuID + '</td>  <td ><a href="ContentManagment?Menuid=' + menu.MenuID + '&MenuType=' + menu.MenuType + '&WebID=' + menu.WebID + '&ismenu=1">' + menu.MenuNane + '</a></td>  <td align="center"><span class="label label-sm label-inverse">不刊登</span></td>  <td align="center"></td></tr>');

                }
                else {
                    $("#" + tableid + " > tbody").before('<tr>    <td align="center">' + menu.MenuID + '</td>  <td ><a href="ContentManagment?Menuid=' + menu.MenuID + '&MenuType=' + menu.MenuType + '&WebID=' + menu.WebID + '&ismenu=1" >' + menu.MenuNane + '</a></td>  <td align="center"><span class="label label-sm label-inverse">不刊登</span></td>  <td align="center"></td></tr>');
                }

            }
            else {

            }
            $(".iframe").colorbox({
                iframe: true, width: "100%", height: "100%"
            });


        },
        Error: function (x, e) {
            console.log(x);
        }
    });
}



