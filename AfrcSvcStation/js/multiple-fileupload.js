﻿


var MultipleFileUpload = function (container) {
    var _self = this;
    var Uploadcontainer = container;
    var _WidgetBox = '';
    var _DefauleFile = '';
    //初始化
    this.Init = function (DefaultId, BtnAdd, BtnUp, WidgetBox) {
        _self.InstallFileInput(DefaultId);
        _self.BindAddEvent(BtnAdd);
        _self.BindUpLoadEvent(BtnUp);
        _self.BindFiles();
        _WidgetBox = WidgetBox;
        _DefauleFile = DefaultId;
    };

    //新增
    this.AddFile = function () {
        var id = RandomNumber().toString();
        var Delid = RandomNumber().toString();
        var RowContents = ' <div class="col-sm-12"> <div class="col-sm-11"> <input type="file" id="' + id + '"> </div> <div class="col-sm-1"><a id="' + Delid + '"  href="javascript:" ><i class="fa fa-trash fa-lg red ace-icon" ></i></a> </div></div>';
        $("#" + Uploadcontainer).append(RowContents);
        _self.InstallFileInput(id);
        _self.BindDelEvent(Delid);

    };

    this.DelRow = function (Obj) {
        $(Obj).parent().parent().remove();
    };


    this.UploadFiles = function () {

        var data = new FormData();

        var lid = $("#hfListID").val();
        var mid = $("#hfMenuID").val();
        if ($("#hfListID").val())
        {
            mid = lid;
        }

      

        var wid = $("#hfWebID").val();
        var ismenu = $("#hfIsMenu").val();

        $("#" + Uploadcontainer + " input[type=file]").each(function () {
            data.append(this.id, this.files[0]);
        }
        )
        $.ajax({
            url: 'FileUploadService.ashx?webid=' + wid + '&menuid=' + mid + '&ismenu=' + ismenu,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
                if (data.Result == "1") {
                    data.Data.forEach(function (o) {
                        var row = ' <div class="col-sm-12"> <div class="col-sm-11"><a href="' + o.FilePath + '" target="_blank">' + o.FileName + ' </a>    </div><div class="col-sm-1"><a id="' + o.ID + '"  href="javascript:" ><i class="fa fa-trash fa-lg red ace-icon" ></i></a> </div></div>';
                        $("#" + Uploadcontainer).append(row);
                        _self.BindDelFileEvent(o.ID);
                    });
                  
                    $("#" + Uploadcontainer + " input[type=file]").each(function () {
                        if (this.id != "id-input-file-2") {
                            console.log($(this).parent().parent().parent().remove());
                        }
                        else {
                            $(this).ace_file_input('reset_input');
                        }
                    });
                 

                }
                else {
                    //上傳失敗
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {

                console.log('ERRORS: ' + textStatus);

            }
        });
    };


    this.InstallFileInput = function (id) {
        $('#' + id).ace_file_input({
            no_file: '沒有檔案 ...',
            btn_choose: '選擇',
            btn_change: '變更',
            droppable: false,
            onchange: null,
            thumbnail: false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
        });
        $("#" + id).click(function () {
            _self.DelFile(id);
        });
    };

    this.BindFiles = function () {


        var mid = $("#hfMenuID").val();
        var wid = $("#hfWebID").val();
        var ismenu = $("#hfIsMenu").val();

        $.ajax({
            url: 'GetFilesService.ashx?webid=' + wid + '&menuid=' + mid + '&ismenu=' + ismenu,
            type: 'POST',

            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
                if (data.Result == "1") {
                    data.Data.forEach(function (o) {
                        var row = ' <div class="col-sm-12"> <div class="col-sm-11"><a href="' + o.FilePath + '" target="_blank">' + o.FileName + ' </a>    </div><div class="col-sm-1"><a id="' + o.ID + '"  href="javascript:" ><i class="fa fa-trash fa-lg red ace-icon" ></i></a> </div></div>';
                        $("#" + Uploadcontainer).append(row);

                        _self.BindDelFileEvent(o.ID);
                    });
                }
                else {

                }

            },
            error: function (jqXHR, textStatus, errorThrown) {

                console.log('ERRORS: ' + textStatus);

            }
        });

    }


    this.BindAddEvent = function (id) {
        $("#" + id).click(function () {
            _self.AddFile();
        });
    };

    this.BindUpLoadEvent = function (id) {
        $("#" + id).click(function () {
            _self.UploadFiles();
        });
    };
    this.BindDelEvent = function (id) {
        $("#" + id).click(function () {
            _self.DelRow(this);
        });
    }
    this.BindDelFileEvent = function (id) {
        $("#" + id).click(function () {
            DelRow(this.id);
        });
    }
}


//刪除檔案
function DelRow(Obj) {
    $.ajax({
        url: 'FileDelService.ashx?FileID=' + Obj,
        type: 'POST',
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (res) {
            $('#' + Obj).parent().parent().remove();
        },
        error: function (res) {

        }
    });

}




//亂數
function RandomNumber() {
    var array1 = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
    var Str = "";
    for (var i = 1; i <= 10; i++) {
        index = Math.floor(Math.random() * array1.length);
        Str = Str + array1[index];
    }

    return Str;

}