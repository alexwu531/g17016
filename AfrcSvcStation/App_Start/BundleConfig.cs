﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace AfrcSvcStation
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {


            // bundling script.
            #region 登入JS
            bundles.Add(
             new ScriptBundle("~/bundles/loginjs")
             .Include(
                 "~/js/jquery-2.1.4.min.js"

             ));
            #endregion

            #region MasterJS
            bundles.Add(
             new ScriptBundle("~/bundles/masterjs")
             .Include(
                 "~/js/ace-extra.min.js",
                 "~/js/jquery-2.1.4.min.js",
                 "~/js/bootstrap.min.js",
                 "~/js/ace-elements.min.js",
                     "~/js/jquery-ui.min.js",
                 "~/js/ace.min.js"

             ));
            #endregion

            #region WebMgtJS
            bundles.Add(
             new ScriptBundle("~/bundles/webmgtjs")
             .Include(
              "~/js/jquery.colorbox.min.js", "~/js/web-managment.js"

             ));
            #endregion
            // bundling styles.
            #region 登入CSS
            bundles.Add(
           new StyleBundle("~/bundles/loginstyles")
           .Include(
               "~/css/bootstrap.min.css",
               "~/css/font-awesome.min.css",
               "~/css/fonts.googleapis.com.css",
               "~/css/ace.min.css",
                "~/css/ace-rtl.min.css"
           ));
            #endregion

            #region MasterCSS
            bundles.Add(
           new StyleBundle("~/bundles/masterstyles")
           .Include(
               "~/css/bootstrap.min.css",

               "~/css/fonts.googleapis.com.css",
               "~/css/ace.min.css",
               "~/css/ace-skins.min.css",
                   "~/css/jquery-ui.min.css",
               "~/css/ace-rtl.min.css"


           ));
            #endregion

            #region webmgtCSS
            bundles.Add(
           new StyleBundle("~/bundles/webmgtstyles")
           .Include(
          "~/css/colorbox.min.css"
           ));
            #endregion
            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;

            ScriptManager.ScriptResourceMapping.AddDefinition(
                "respond",
                new ScriptResourceDefinition
                {
                    Path = "~/Scripts/respond.min.js",
                    DebugPath = "~/Scripts/respond.js",
                });
        }
    }
}