﻿using AfrcSvcStation.App_Code;
using AfrcSvcStation.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.WebUserControl
{
    public partial class WucListEdit : System.Web.UI.UserControl
    {
        protected string MenuID = HttpContext.Current.Request["MenuID"].ToString();
        protected string MenuType = HttpContext.Current.Request["MenuType"].ToString();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected UserInfo so = new UserInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
           so = (UserInfo)Session["UserData"];
            if (!IsPostBack)
            {
                BindData();
            }
        }

        public void BindData()
        {
            try
            {
                using (AFRCEntities db = new AFRCEntities())
                {
                    int MenuID = Convert.ToInt32(this.MenuID);
                    var lst = db.T_List.ToList();

                    AspNetPager1.AlwaysShow = true;
                    AspNetPager1.RecordCount = lst.Where(c => c.ParentMenuID == MenuID).Count();

                    repListView.DataSource = lst.Where(c => c.ParentMenuID == MenuID).OrderBy(o => o.Sort).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize)
                                              .Take(AspNetPager1.PageSize);
                    repListView.DataBind();
                    // var UserList =
                    //      db.T_SysUser.Where(
                    //          c => txtUserID.Text != "" ? c.UserID.Contains(txtUserID.Text) : true &&
                    //               txtUserName.Text != "" ? c.UserName.Contains(txtUserName.Text) : true &&
                    //               ddlUseState.SelectedValue.ToString() != "0" ? c.UseState == ddlUseState.SelectedValue.ToString() : true &&
                    //               c.UseState == "1"
                    //               ).ToList();
                    // var UserList_Query =
                    //  db.T_SysUser.Where(
                    //      c => txtUserID.Text != "" ? c.UserID.Contains(txtUserID.Text) : true &&
                    //           txtUserName.Text != "" ? c.UserName.Contains(txtUserName.Text) : true &&
                    //           ddlUseState.SelectedValue.ToString() != "0" ? c.UseState == ddlUseState.SelectedValue.ToString() : true &&
                    //           c.UseState == "1"
                    //           ).ToString();
                    // AspNetPager1.AlwaysShow = true;
                    // AspNetPager1.RecordCount = UserList.Count();

                    //repListView.DataSource = UserList.OrderBy(o => o.ID).Skip((AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize)
                    //                          .Take(AspNetPager1.PageSize);
                    // repListView.DataBind();


                    #region ActionLog
                    string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                    logger.Info(string.Format("資料表:T_List"));
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ActionLog
                //string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                ////取得IP
                //if (Request.ServerVariables["HTTP_VIA"] != null)
                //{
                //    tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                //}
                //GlobalDiagnosticsContext.Set("addr", tClientIP);
                //GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                //GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                //logger.Error(ex);
                #endregion
            }
        }



        protected void repListView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            {
                //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                //{
                //    HiddenField HF_RoleID = (HiddenField)e.Item.FindControl("HiddenField_RoleID");
                //    HiddenField HF_UseState = (HiddenField)e.Item.FindControl("HiddenField_UseState");
                //    Label lblRoleID = (Label)e.Item.FindControl("lblRoleID");
                //    Label lblUseState = (Label)e.Item.FindControl("lblUseState");

                //    using (AFRCEntities db = new AFRCEntities())
                //    {
                //        try
                //        {
                //            so = (UserInfo)Session["UserData"];

                //            var Group = (from G in db.T_SysGroup
                //                         where G.GroupID.ToString() == HF_RoleID.Value
                //                         select G).SingleOrDefault();

                //            lblRoleID.Text = Group.GroupName;

                //            #region ActionLog
                //            string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //            //取得IP
                //            if (Request.ServerVariables["HTTP_VIA"] != null)
                //            {
                //                tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                //            }
                //            GlobalDiagnosticsContext.Set("addr", tClientIP);
                //            GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                //            GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                //            logger.Info(Group.ToString());
                //            db.Dispose();
                //            #endregion
                //        }
                //        catch (Exception ex)
                //        {
                //            lblRoleID.Text = "查無對應權限";

                //            #region ActionLog
                //            string tClientIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                //            //取得IP
                //            if (Request.ServerVariables["HTTP_VIA"] != null)
                //            {
                //                tClientIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                //            }
                //            GlobalDiagnosticsContext.Set("addr", tClientIP);
                //            GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                //            GlobalDiagnosticsContext.Set("action", Actios.Query.ToString());
                //            logger.Info(ex);
                //            #endregion
                //        }

                //        lblUseState.Text = HF_UseState.Value == "1" ? "是" : "否";
                //    }
            }
        }



        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int ListID = Convert.ToInt32(e.CommandArgument);
                int Type = 0;

                AFRCEntities db = new AFRCEntities();
                try
                {
                    var ListType = (from L in db.T_List
                                    where L.ListID == ListID
                                    select L).ToList();
                    if (ListType != null)
                    {
                        foreach (var item in ListType)
                        {
                            Type = Convert.ToInt32(item.Type);
                        }
                    }
                  
                }
                catch (Exception ex)
                {
                    db.Dispose();
                }
                finally
                {
                    db.Dispose();
                }

                Response.Redirect("ListContentManagment?MenuID=" + MenuID + "&MenuType=" + MenuType + "&ListID=" + e.CommandArgument + "&Type=" + Type.ToString()+"&ismenu=2");
            }
        }

        protected void cmdadd_Click(object sender, EventArgs e)
        {
            try
            {
                UserInfo so = (UserInfo)HttpContext.Current.Session["UserData"];



                using (AFRCEntities db = new AFRCEntities())
                {
                    int _menuid = int.Parse(MenuID);

                    var tmp = db.T_List.Where(l => l.ParentMenuID == _menuid).Select(s => s.Sort).ToArray();
                    int Minsort = tmp.Any() ? tmp.Min() : 0;
                    int sort = Minsort - 1;
                    T_List lst = new T_List
                    {

                        ParentMenuID = _menuid,
                        Title = "新清單",
                        CreateDate = DateTime.UtcNow.AddHours(8),
                        CreatePersonID = int.Parse(so.UserPKID),
                        EditDate = DateTime.UtcNow.AddHours(8),
                        EditPersonID = int.Parse(so.UserPKID),
                        MetaData = "",
                        DepID = int.Parse(so.UserDept),
                        Type = 1,
                        FreeGOType = 2,
                        Sort = sort,
                        Publish = 2,
                        Audit = 2,
                        WorkYear = DateTime.Now.Year

                    };

                    db.T_List.Add(lst);

                    db.SaveChanges();

                    #region ActionLog
                    string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    //取得IP
                    if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    {
                        tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                    }
                    GlobalDiagnosticsContext.Set("addr", tClientIP);
                    GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                    GlobalDiagnosticsContext.Set("action", Actios.Insert.ToString());
                    logger.Info(string.Format("資料表:T_List;主鍵{0};單元名稱:{1}", lst.ListID.ToString(), lst.Title));
                    #endregion

                    BindData();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

        protected void btnDel_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Del")
            {
                int ListID = Convert.ToInt32(e.CommandArgument);
                try
                {
                    using (AFRCEntities db = new AFRCEntities())
                    {
                        T_List lst = db.T_List.Where(l => l.ListID == ListID).FirstOrDefault();

                        db.T_List.Remove(lst);

                        db.SaveChanges();

                        #region ActionLog
                        string tClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                        //取得IP
                        if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                        {
                            tClientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                        }
                        GlobalDiagnosticsContext.Set("addr", tClientIP);
                        GlobalDiagnosticsContext.Set("userid", so.UserPKID.ToString());
                        GlobalDiagnosticsContext.Set("action", Actios.Delete.ToString());
                        logger.Info(string.Format("資料表:T_List;主鍵{0};單元名稱:{1}", lst.ListID.ToString(), lst.Title));
                        #endregion

                        BindData();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}