﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WucMetaDataControl.ascx.cs" Inherits="AfrcSvcStation.WebUserControl.WucMetaDataControl" %>
<%@ Register Src="~/WebUserControl/WucDate.ascx" TagPrefix="uc1" TagName="WucDate" %>

<style type="text/css">
    .auto-style1 {
        background-color: #E3DFD7;
    }
    .auto-style2 {
        height: 2px;
    }
    .auto-style3 {
        height: 6px;
    }
</style>

<tr>
    <td class="formframe-fieldname" onclick="ChangeDisplay()" style="cursor: pointer">
<%--             分類檢索<br/>
                                            (點擊縮放)--%>
    </td>
    <td colspan="3" class="formframe-fieldvalue" id="MetaData" style="display: none">
        <table bgcolor="#cccccc" border="0" cellpadding="2" cellspacing="1" width="800">
            <tr>
                <td height="26" colspan="2" align="center" valign="middle" bgcolor="#e3dfd7">
                    <div align="center">
                    </div>
                    <div align="center">
                        <strong>分類檢索資料項</strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" bgcolor="#e3dfd7">
                    <span style="color: #ff0000">*</span>標題(必要)</td>
                <td align="center" valign="middle" bgcolor="#f9f9f9">
                    <div align="left">
                        <input id="txtTitle" maxlength="100" name="txtTitle" type="text" class="textbox"
                            runat="server" />&nbsp;<span id="spanTitle" class="alertText" style="display: none">請輸入標題！</span>
                        <asp:CheckBox ID="ckbGetTitle" runat="server" Text="同標題" />
                    </div>
                </td>
            </tr>
            <tr>
                <td height="-1" nowrap align="center" valign="middle" bgcolor="#e3dfd7">
                    <span style="color: #ff0000">*</span>主旨(必要)</td>
                <td align="center" bgcolor="#f9f9f9" height="26" valign="middle">
                    <div align="left">
                        <input id="txtSubject" maxlength="100" name="txtSubject" type="text" class="textbox"
                            runat="server" />&nbsp;<span id="spanSubject" class="alertText" style="display: none">請輸入主題或關鍵字！</span>
                        <asp:CheckBox ID="ckbGetSubject" runat="server" Text="同標題" />
                    </div>
                </td>
            </tr>
            <tr>
                <td height="0" align="center" valign="middle" bgcolor="#e3dfd7">
                    <span style="color: #ff0000">*</span>作者姓名(必要)</td>
                <td align="center" bgcolor="#f9f9f9" height="26" valign="middle">
                    <div align="left">
                        <input id="txtCreater" maxlength="100" name="txtCreater" type="text" class="textbox"
                            runat="server" value="國防部後備指揮部" />
                        &nbsp;
                    </div>
                </td>
            </tr>
            <tr>
                <td height="0" align="center" valign="middle" bgcolor="#e3dfd7">
                    <span style="color: #ff0000">*</span>機關全稱(必要)</td>
                <td align="center" bgcolor="#f9f9f9" height="26" valign="middle"
                    style="text-align: left">
                    <input id="txtPublisher" maxlength="100" name="txtPublisher" type="text" class="textbox"
                        runat="server" value="國防部後備指揮部" />
                    <span id="spanPublisher" class="alertText" style="display: none">請輸入出版者！</span>
                </td>
            </tr>
            <tr>
                <td height="1" align="center" valign="middle" bgcolor="#e3dfd7">
                    <span style="color: #ff0000">*</span>製作日期(必要)</td>
                <td align="center" bgcolor="#f9f9f9" height="26" style="text-align: left"
                    valign="middle">
                    <%--                    <input id="txtCreateDate" maxlength="10" name="txtCreateDate" type="text" class="textbox"
                        runat="server" />--%>
                    <uc1:WucDate runat="server" ID="WucDate" />
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" bgcolor="#e3dfd7" class="auto-style2">
                    <span style="color: #ff0000">*</span>資料類型(必要)</td>
                <td align="center" bgcolor="#f9f9f9" valign="middle" class="auto-style2">
                    <div align="left">
                        <asp:DropDownList ID="ddlType" CssClass="select" runat="server" MemberType>
                            <asp:ListItem Value="文字">文字</asp:ListItem>
                            <asp:ListItem Value="聲音">聲音</asp:ListItem>
                            <asp:ListItem Value="影像">影像</asp:ListItem>
                            <asp:ListItem Value="實體物體">實體物體</asp:ListItem>
                            <asp:ListItem Value="事件">事件</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </td>
            </tr>
            <tr>
                <td height="1" align="center" valign="middle" bgcolor="#e3dfd7">
                    <span style="color: #ff0000">*</span>OID(必要)</td>
                <td align="center" bgcolor="#f9f9f9" height="26" style="text-align: left"
                    valign="middle">
                    <input id="txtIdentifier" maxlength="20" name="txtIdentifier" type="text" class="textbox"
                        runat="server" value="2.16.886.101.20003.20003.20010" />
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" bgcolor="#e3dfd7" class="auto-style3">
                    <span style="color: #ff0000">*</span>主題分類(必要)</td>
                <td align="center" bgcolor="#f9f9f9" valign="middle" style="text-align: left" class="auto-style3">
                    <%--                    <asp:DropDownList ID="ddlTheme" CssClass="select" runat="server" MemberType>
                        <asp:ListItem Value="160">消防</asp:ListItem>
                        <asp:ListItem Value="I00">公共資訊</asp:ListItem>
                    </asp:DropDownList>--%>
                    <input type="hidden" id="hidTheme" runat="server" /><span id="spanTheme" class="alertText"
                        style="display: none">請選擇主題分類！</span>
                    <%--<asp:Button ID="btnSearchTheme" runat="server" Text="選取" OnClick="btnSearchTheme_Click" />
                    --%>
                    <asp:HyperLink ID="linkDetail" runat="server" ToolTip="進入預算編列輸入" NavigateUrl="ThemeOpen()">選取</asp:HyperLink>
                    <asp:TextBox ID="txtTheme" runat="server" ClientIDMode="Static">240,440</asp:TextBox>
                    <asp:Label ID="lblThemeText" runat="server" Text="國防(410),文化藝術(440)" ClientIDMode="Static"></asp:Label>
                                                <asp:DropDownList ID="ddlMetaTheme" runat="server" CssClass ="multiselect" multiple="" ClientIDMode="Static" Visible="False"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td height="13" align="center" valign="middle" bgcolor="#e3dfd7">
                    <span style="color: #ff0000">*</span>施政分類(必要)</td>
                <td align="center" bgcolor="#f9f9f9" valign="middle" style="text-align: left">
                    <%--                    <asp:DropDownList ID="ddlCake" CssClass="select" runat="server" MemberType>
                        <asp:ListItem Value="160">災害防救</asp:ListItem>
                        <asp:ListItem Value="150">消防</asp:ListItem>
                        <asp:ListItem Value="151">火災預防</asp:ListItem>
                        <asp:ListItem Value="153">危險物品管理</asp:ListItem>
                        <asp:ListItem Value="154">緊急救護</asp:ListItem>
                        <asp:ListItem Value="157">民力運用</asp:ListItem>
                        <asp:ListItem Value="I5Z">其他</asp:ListItem>
                        <asp:ListItem Value="I64">災害應變</asp:ListItem>
                    </asp:DropDownList>--%>
                    <input type="hidden" id="hidCake" runat="server" /><span id="spanCake" class="alertText"
                        style="display: none">請選擇施政分類！</span>
                                        <asp:HyperLink ID="linkDetail2" runat="server" ToolTip="進入預算編列輸入" NavigateUrl="ThemeOpen()">選取</asp:HyperLink>
                    <asp:TextBox ID="txtCake" runat="server" Text="314" ClientIDMode="Static"></asp:TextBox>
                    <asp:Label ID="lblCakeText" runat="server" Text="後備動員(314)" ClientIDMode="Static"></asp:Label>
                    <asp:DropDownList ID="ddlMetaCake" runat="server" CssClass ="multiselect" multiple="" ClientIDMode="Static" Visible="False"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="center" bgcolor="#e3dfd7" valign="middle">
                    <div align="center">
                        <span style="color: #ff0000">*</span>服務分類(必要)
                    </div>
                </td>
                <td align="center" bgcolor="#f9f9f9" valign="middle" style="text-align: left">
                    <%--<asp:DropDownList ID="ddlService" CssClass="select" runat="server" MemberType>
                        <asp:ListItem Value="I6Z">資訊服務>其他</asp:ListItem>
                    </asp:DropDownList>--%>
                    <input type="hidden" id="hidService" runat="server" /><span id="spanService" class="alertText"
                        style="display: none">請選擇服務分類！</span>
                                                            <asp:HyperLink ID="linkDetail3" runat="server" ToolTip="進入預算編列輸入" NavigateUrl="ThemeOpen()">選取</asp:HyperLink>
                    <asp:TextBox ID="txtService" runat="server" Text="410,420,430,440,450" ClientIDMode="Static"></asp:TextBox>
                    <asp:Label ID="lblServiceText" runat="server" Text="志願役(410),義務役(420),國防役(430),後備軍人(440),退伍軍人(450)" ClientIDMode="Static"></asp:Label>
                    <asp:DropDownList ID="ddlMetaService" runat="server" CssClass ="multiselect" multiple="" ClientIDMode="Static" Visible="False"></asp:DropDownList>
                </td>
            </tr>
            <tr bgcolor="#efefef">
                <td height="26" align="center" valign="middle" bgcolor="#E3DFD7">
                    <div align="center">
                        內容描述(選擇)
                    </div>
                </td>
                <td height="26" align="center" valign="middle">
                    <div align="left">
                        <textarea class="textbox" cols="35" name="txtDesc" rows="3" id="txtDesc" runat="server"></textarea>
                    </div>
                </td>
            </tr>
            <tr bgcolor="#efefef">
                <td height="26" align="center" valign="middle" bgcolor="#E3DFD7">貢獻者(選擇)
                </td>
                <td height="26" align="center" valign="middle">
                    <div align="left">
                        <input id="txtContributor" maxlength="100" name="txtContributor" type="text" class="textbox"
                            runat="server" value="國防部後備指揮部" />
                    </div>
                </td>
            </tr>
            <tr bgcolor="#efefef">
                <td height="26" align="center" valign="middle" bgcolor="#E3DFD7">資料格式(選擇)
                </td>
                <td height="26" align="center" valign="middle">
                    <div align="left">
                        <input id="txtFormat" maxlength="100" name="txtFormat" type="text" class="textbox"
                            runat="server" value="文字" />
                    </div>
                </td>
            </tr>
            <tr bgcolor="#efefef">
                <td height="26" align="center" valign="middle" bgcolor="#E3DFD7">關聯(選擇)
                </td>
                <td height="26" align="center" valign="middle">
                    <div align="left">
                        <input id="txtRelation" maxlength="100" name="txtRelation" type="text" class="textbox"
                            runat="server" value="國防部後備指揮部" />
                    </div>
                </td>
            </tr>
            <tr bgcolor="#efefef">
                <td height="26" align="center" valign="middle" bgcolor="#E3DFD7">來源(選擇)
                </td>
                <td height="26" align="center" valign="middle">
                    <div align="left">
                        <input id="txtSource" maxlength="100" name="txtSource" type="text" class="textbox"
                            runat="server" value="國防部後備指揮部" />
                    </div>
                </td>
            </tr>
            <tr bgcolor="#efefef">
                <td height="26" align="center" valign="middle" bgcolor="#E3DFD7">語<span class="auto-style1">文</span>(建議)
                </td>
                <td height="26" align="center" valign="middle">
                    <div align="left">
                        <asp:DropDownList ID="ddlLanguage" CssClass="select" runat="server" MemberType>
                            <asp:ListItem Value="中文">中文</asp:ListItem>
                            <asp:ListItem Value="英文">英文</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </td>
            </tr>
            <tr bgcolor="#efefef">
                <td height="26" nowrap align="center" valign="middle" bgcolor="#E3DFD7">涵蓋範圍(建議)
                </td>
                <td height="26" align="center" valign="middle">
                    <div align="left">
                        <input id="txtCoverage" maxlength="100" name="txtCoverage" type="text" class="textbox"
                            runat="server" value="民國至迄今" />
                    </div>
                </td>
            </tr>
            <tr bgcolor="#efefef">
                <td height="26" nowrap align="center" valign="middle" bgcolor="#E3DFD7">著作權說明(建議)
                </td>
                <td height="26" align="center" valign="middle">
                    <div align="left">
                        <input id="txtRight" maxlength="100" name="txtRight" type="text" class="textbox"
                            runat="server" value="國防部後備指揮部" />
                    </div>
                </td>
            </tr>
            <tr bgcolor="#efefef">
                <td height="26" nowrap align="center" valign="middle" bgcolor="#E3DFD7">關鍵字(建議)
                </td>
                <td height="26" align="center" valign="middle">
                    <div align="left">
                        <input id="txtKeyWord" maxlength="100" name="txtKeyWord" type="text" class="textbox"
                            runat="server" />
                    </div>
                </td>
            </tr>

        </table>
    </td>
</tr>


<script type="text/javascript">
    function ChangeDisplay() {
        if (document.all("MetaData").style.display == "block") {
            document.all("MetaData").style.display = "none";
        }
        else {
            document.all("MetaData").style.display = "block";
        }
    }
</script>


