﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WucHeader.ascx.cs" Inherits="AfrcSvcStation.WebUserControl.WucHeader" %>
<div class="navbar-container ace-save-state" id="navbar-container">
    <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
        <span class="sr-only">Toggle sidebar</span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>
    </button>

    <div class="navbar-header pull-left">
        <a href="index.html" class="navbar-brand">
            <small>
                <i class="fa fa-cloud"></i>
                後備軍人服務台後端管理平台
            </small>
            
        </a>
    </div>

    <div class="navbar-buttons navbar-header pull-right" role="navigation">
        <ul class="nav ace-nav">
            <!--li class="grey dropdown-modal">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="ace-icon fa fa-tasks"></i>
                    <span class="badge badge-grey">
                        <asp:Literal ID="LitWebCount" runat="server"></asp:Literal>
                    </span>
                </a>

                <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                    <li class="dropdown-header">
                        <i class="ace-icon fa fa-refresh"></i>
                        網站切換
                    </li>

                    <li class="dropdown-content ace-scroll" style="position: relative;">
                        <div class="scroll-track" style="display: none;">
                            <div class="scroll-bar"></div>
                        </div>
                        <div class="scroll-content" style="max-height: 200px;overflow-y:scroll">
                            <ul class="dropdown-menu dropdown-navbar">

                                <asp:Repeater ID="repWebName" runat="server">

                                    <ItemTemplate>
                                        <li>
                                            <a href='<%#ResolveUrl("~/Default") + "?webid=" +Encrypt(Eval("WebID").ToString()) %>'>
                                                <div class="clearfix">
                                                    <span class="pull-left">
                                                        <%#Eval("WebName") %>
                                                   
                                                    </span>

                                                </div>
                                            </a>
                                        </li>


                                    </ItemTemplate>
                                </asp:Repeater>

                            </ul>
                        </div>
                    </li>


                </ul>
            </li-->

  
            <li class="light-blue dropdown-modal">
                <a data-toggle="dropdown" href="#" class="dropdown-toggle">
     
                    <span class="user-info">
                        <small>Welcome,</small>
                        <asp:Literal ID="ltlUserName" runat="server"></asp:Literal>
                    </span>

                    <i class="ace-icon fa fa-caret-down"></i>
                </a>

                <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                 
                    <li>
                        <asp:Literal ID="lblProfileLink" runat="server"></asp:Literal>

                        <a href="/Function/Account/AccountDetail.aspx?StrID=<%=StrID%>">
                            <i class="ace-icon fa fa-user"></i>
                           個人資料
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="<%=ResolveUrl("~/Function/Account/Login") %>">
                            <i class="ace-icon fa fa-power-off"></i>
                           登出
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
