﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AfrcSvcStation.ModelsForStation;

namespace AfrcSvcStation.WebUserControl
{
    public partial class WucLeftMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string LeftMenu()
        {
            string strMenu = string.Empty;
            AFRCDutyEntities db = new AFRCDutyEntities();
            UserInfo so = (UserInfo)Session["UserData"];
            var MenuList = db.P_GetMenu(so.UserID.ToString()).OrderBy(o => o.Sort).ToList();

            try
            {
                if (MenuList.Count() > 0)
                {
                    strMenu += "<ul class='nav nav-list'>";

                    //第一層選單
                    var ParentMenuList = MenuList.Where(p => p.MenuLevel == 1).OrderBy(o => o.Sort);
                    foreach (var dataRow in ParentMenuList)
                    {
                        var Layer2Child = MenuList.Where(c => c.MenuLevel == 2 && c.ParentMenuID == dataRow.MenuID);
                        if (Layer2Child.Count() <= 0)
                        {//無第二層選單
                            strMenu += string.Format(@"
                                <li class='active'>
                                    <a href='{0}'>
                                        <i class='menu-icon fa fa-tachometer'></i>
                                        <span class='menu-text'>
                                        {1}
                                        </span>
                                    </a>
                                         <b class='arrow'></b>
                                </li>
                                                        ",ResolveUrl(dataRow.MenuURL.ToString()), getName(dataRow.MenuName));
                        }
                        else
                        {//有第二層選單
                            strMenu += string.Format(@"
                                <li class=''>
                                    <a href='#' class='dropdown-toggle'>
                                        <i class='menu-icon fa {2}'></i>
                                        <span class='menu-text'>
                                        {1}
                                        </span>
                                           <b class='arrow fa fa-angle-down'></b>
                                    </a>
                                    <b class='arrow'></b>

                                    <ul class='submenu'>
                                                        ", ResolveUrl(dataRow.MenuURL.ToString()), getName(dataRow.MenuName),dataRow.MenuCSS);

                            foreach (var Layer2ChilddataRow in Layer2Child)
                            {
                                strMenu += string.Format(@"
                                    <li class=''>
										<a href='{0}'>
                                            <i class='menu-icon fa fa-caret-right'></i>
											{1}
                                        </a>

										<b class='arrow'></b>
									</li>
                                        ", ResolveUrl(Layer2ChilddataRow.MenuURL.ToString()), getName(Layer2ChilddataRow.MenuName));
                            }
                        }
                        strMenu += @"
                                    </ul>
                                 </li>
                                      ";
                    }
                    strMenu += "</ul>";
                }
            }
            catch (Exception ex)
            {

            }

            return strMenu;
        }

        #region UrlHelper
        /// <summary>
        /// 相對路徑轉絕對路徑
        /// </summary>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        public string ConvertRelativeUrlToAbsoluteUrl(string relativeUrl)
        {
            return string.Format("https{0}://{1}{2}",
                (Request.IsSecureConnection) ? "s" : "",
                Request.Url.Host,
                Page.ResolveUrl(relativeUrl)
            );
        }
        #endregion

        #region Menu父層超過10個字換行
        /// <summary>
        /// Menu父層超過10個字換行
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public string getName(string Name)
        {
            if (Name.Length > 10)
            {
                Name = Name.Substring(0, 10) + "<br>" + Name.Substring(10);
            }
            return Name;
        }
        #endregion
    }
}