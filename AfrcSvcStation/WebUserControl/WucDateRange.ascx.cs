﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.WebUserControl
{
    public partial class WucDateRange : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private string GetCNDateRange(string date)
        {
            string rtValue = "";
            try
            {
                string[] arydate = date.Split('-');
                int y = Convert.ToInt32(arydate[0]);
                int m = Convert.ToInt32(arydate[1]);
                int d = Convert.ToInt32(arydate[2]);

                rtValue = string.Format("{0:000}", y) + "-" + string.Format("{0:00}", m) + "-" + string.Format("{0:00}", d);
            }
            catch (Exception ex)
            {

            }

            return rtValue;
        }

        private string GetTWDateRange(string date)
        {
            string rtValue = "";
            try
            {
                string[] arydate = date.Split('-');
                int y = Convert.ToInt32(arydate[0]);
                int m = Convert.ToInt32(arydate[1]);
                int d = Convert.ToInt32(arydate[2]);

                rtValue = string.Format("{0:000}", y - 1911) + "-" + string.Format("{0:00}", m) + "-" + string.Format("{0:00}", d);
            }
            catch (Exception ex)
            {

            }

            return rtValue;
        }

        public string TWDateRange
        {
            get
            { return GetTWDateRange(datepicker.Text.Trim()); }
            set
            {
                this.datepicker.Text = value;
            }
        }

        public string CNDateRange
        {
            get
            { return GetCNDateRange(datepicker.Text.Trim()); }
            set
            {
                try
                {
                    datepicker.Text = string.Format("{0:yyyy-MM-dd}", (Convert.ToDateTime(value)));
                }
                catch (Exception ex)
                {
                    datepicker.Text = "";
                }
            }
        }
    }
}