﻿using AfrcSvcStation.App_Code;
using AfrcSvcStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.WebUserControl
{
    public partial class WucHyperlnkList : System.Web.UI.UserControl
    {
        WebMessage Msg = new WebMessage();
        protected string MenuID = HttpContext.Current.Request["MenuID"].ToString();
        protected string MenuType = HttpContext.Current.Request["MenuType"].ToString();
        protected string ismenu = HttpContext.Current.Request["ismenu"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }
        public void BindData()
        {
            try
            {
                using (AFRCEntities db = new AFRCEntities())
                {
                    int MenuID = Convert.ToInt32(this.MenuID);
                    int _ismenu = Convert.ToInt32(this.ismenu);
                    var lst = db.T_ContentEXLink.Where(m => m.ParentID == MenuID && m.isMenu == _ismenu && m.Type==1).ToList();
                    replinkView.DataSource = lst;
                    replinkView.DataBind();
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void replinkView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                try
                {
                    UserInfo so = (UserInfo)Session["UserData"];
                    using (AFRCEntities db = new AFRCEntities())
                    {
                        TextBox txtlinkname = e.Item.FindControl("txtlinkname") as TextBox;
                        TextBox txtlnk = e.Item.FindControl("txtlnk") as TextBox;
                        int lid = int.Parse(e.CommandArgument.ToString());
                        //修改
                        T_ContentEXLink lnk = db.T_ContentEXLink.Where(u => u.ID == lid).FirstOrDefault();

                        lnk.Title = txtlinkname.Text;
                        lnk.Link = txtlnk.Text;
                        db.SaveChanges();
                        Msg.MsgBox_In_Ajax("修改完成", this.Page);


                    }
                }
                catch (Exception ex)
                {

                }

            }
            if (e.CommandName == "Del")
            {
                try
                {
                    UserInfo so = (UserInfo)Session["UserData"];
                    using (AFRCEntities db = new AFRCEntities())
                    {
                     
                        int lid = int.Parse(e.CommandArgument.ToString());
                      
                        T_ContentEXLink lnk = db.T_ContentEXLink.Where(u => u.ID == lid).FirstOrDefault();

                 

                        db.T_ContentEXLink.Remove(lnk);

                   
                        db.SaveChanges();



                        BindData();

                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {

            try
            {
                int MenuID = Convert.ToInt32(this.MenuID);
                int _ismenu = Convert.ToInt32(this.ismenu);
                int webid = (int)Session["WEBID"];
                using (AFRCEntities db = new AFRCEntities())
                {
                    int sort = 0;
                    var vsort = db.T_ContentEXLink.Where(o => o.ParentID == MenuID && o.WebID == webid).Select(o => o.Sort).ToArray().Max();
                    if (vsort == null)
                    {
                        sort = 1;
                    }
                    else
                    {
                        sort = Convert.ToInt32(vsort);
                        sort = sort + 1;
                    }



                    T_ContentEXLink lnk = new T_ContentEXLink
                    {
                        isMenu = _ismenu,
                        Link = txtlnk.Text,
                        ParentID = MenuID,
                        Sort = sort,
                        Title = txtlinkname.Text,
                        Type = 1,
                        WebID = webid
                    };
                    db.T_ContentEXLink.Add(lnk);
                    db.SaveChanges();
                    txtlinkname.Text = "";
                    txtlnk.Text = "";
                    BindData();
                }
            }
            catch (Exception ex)
            {

            }






            BindData();
        }
    }
}