﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WucHyperlnkList.ascx.cs" Inherits="AfrcSvcStation.WebUserControl.WucHyperlnkList" %>
<asp:UpdatePanel ID="up1" runat="server">
    <ContentTemplate>
        <span class="input-icon">
            <asp:TextBox ID="txtlinkname" runat="server"></asp:TextBox>
            <i class="ace-icon fa fa-font blue"></i>
        </span>

        <span class="input-icon input-icon-right">
            <asp:TextBox ID="txtlnk" runat="server"></asp:TextBox>
            <i class="ace-icon fa fa-link green"></i>
        </span>
        <asp:LinkButton ID="cmdAdd" runat="server" CssClass="btn  btn-grey btn-xs radius-4" OnClick="cmdAdd_Click">
              <i class="ace-icon fa fa-floppy-o bigger-160"></i>
            新增									
        </asp:LinkButton>

        <br />


        <asp:Repeater ID="replinkView" runat="server" OnItemCommand="replinkView_ItemCommand">
            <ItemTemplate>
                <span class="input-icon">
                    <asp:TextBox ID="txtlinkname" runat="server" Text='<%#Eval("Title") %>'></asp:TextBox>
                    <i class="ace-icon fa fa-font blue"></i>
                </span>

                <span class="input-icon input-icon-right">
                    <asp:TextBox ID="txtlnk" runat="server" Text='<%#Eval("Link") %>'></asp:TextBox>
                    <i class="ace-icon fa fa-link green"></i>
                </span>
                <asp:LinkButton ID="cmdEdit" runat="server" CssClass="btn  btn-grey btn-xs radius-4" CommandName="Edit" CommandArgument='<%#Eval("ID").ToString()%>'>
              <i class="ace-icon fa fa-floppy-o bigger-160"></i>
           編輯								
                </asp:LinkButton>
                   <asp:LinkButton ID="cmdDel" runat="server" CssClass="btn  btn-danger btn-xs radius-4" CommandName="Del" CommandArgument='<%#Eval("ID").ToString()%>'>
              <i class="ace-icon fa fa-trash-o"></i>
           刪除						
                </asp:LinkButton>
                <br />

            </ItemTemplate>

        </asp:Repeater>

    </ContentTemplate>
</asp:UpdatePanel>



