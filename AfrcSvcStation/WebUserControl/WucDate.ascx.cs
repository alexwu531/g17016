﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.WebUserControl
{
    public partial class WucDate : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(datepicker.Text))
            {
                datepicker.Text = string.Format("{0:yyyy-MM-dd}", DateTime.Now.Date);
            }            
        }

        private string GetCNDate(string date)
        {            
            string rtValue = "";
            try
            {
                string[] arydate = date.Split('-');
                int y = Convert.ToInt32(arydate[0]);
                int m = Convert.ToInt32(arydate[1]);
                int d = Convert.ToInt32(arydate[2]);

                rtValue = string.Format("{0:000}", y) + "-" + string.Format("{0:00}", m) + "-" + string.Format("{0:00}", d);
            }
            catch (Exception ex)
            {

            }

            return rtValue;
        }

        private string GetTWDate(string date)
        {
            string rtValue = "";
            try
            {
                string[] arydate = date.Split('-');
                int y = Convert.ToInt32(arydate[0]);
                int m = Convert.ToInt32(arydate[1]);
                int d = Convert.ToInt32(arydate[2]);

                rtValue = string.Format("{0:000}", y - 1911) + "-" + string.Format("{0:00}", m) + "-" + string.Format("{0:00}", d);
            }
            catch (Exception ex)
            {

            }

            return rtValue;
        }

        public string TWDate
        {
            get
            { return GetTWDate(datepicker.Text.Trim()); }
            set
            {
                this.datepicker.Text = value;
            }
        }

        public string CNDate
        {
            get
            { return GetCNDate(datepicker.Text.Trim()); }
            set
            {
                try
                {
                    datepicker.Text =string.Format("{0:yyyy-MM-dd}",(Convert.ToDateTime(value)));
                }
                catch (Exception ex)
                {
                    datepicker.Text = "";                    
                }             
            }
        }
    }
}