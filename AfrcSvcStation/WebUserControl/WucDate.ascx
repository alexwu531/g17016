﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WucDate.ascx.cs" Inherits="AfrcSvcStation.WebUserControl.WucDate" %>


        <div class="input-group input-group-sm">
            <asp:TextBox ID="datepicker" runat="server" CssClass="datepicker form-control"></asp:TextBox>
            <span class="input-group-addon">
                <i class="ace-icon fa fa-calendar"></i>
            </span>
        </div>
