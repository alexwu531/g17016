﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AfrcSvcStation.ModelsForStation;
using AfrcSvcStation.App_Code;
using System.Data;

namespace AfrcSvcStation.WebUserControl
{
    public partial class WucMetaDataControl : System.Web.UI.UserControl
    {
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        string metaDataXml = string.Empty;

        public string metatitle
        {
            get { return txtTitle.Value; }
        }
        public string metasubject
        {
            get { return txtSubject.Value; }
        }
        public string metaCreator
        {
            get { return txtCreater.Value; }
        }
        public string metaPublisher
        {
            get { return txtPublisher.Value; }
        }
        public string metaWucDate
        {
            get { return WucDate.CNDate; }
        }
        public string metaddlType
        {
            get { return ddlType.SelectedValue.ToString(); }
        }
        public string metaIdengifier
        {
            get { return txtIdentifier.Value; }
        }
        public string metatheme { get { return txtTheme.Text; } }
        public string metacake { get { return txtCake.Text; } }
        public string metaservice { get { return txtService.Text; } }
        public string metaKeyWord
        {
            get { return txtKeyWord.Value; }
        }

        public string InputTitle
        {
            get; set;
        }

        public string InputSubject
        {
            get; set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindMetaTheme();
            BindMetaCake();
            BindMetaService();

            linkDetail.Attributes.Add("onclick", "window.open('MetaChoose.aspx?Type=Theme&ValueCID=txtTheme&TextCID=lblThemeText','','height=300, width=700, top=200, left=270, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');return false;");
            linkDetail2.Attributes.Add("onclick", "window.open('MetaChoose.aspx?Type=Cake&ValueCID=txtCake&TextCID=lblCakeText','','height=300, width=700, top=200, left=270, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');return false;");
            linkDetail3.Attributes.Add("onclick", "window.open('MetaChoose.aspx?Type=Service&ValueCID=txtService&TextCID=lblServiceText','','height=300, width=700, top=200, left=270, toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, status=no');return false;");
        }
        private string InputText(string text)
        {
            text = text.Trim();
            if (String.IsNullOrEmpty(text))
                return String.Empty;
            text = text.Replace("<", "＜").Replace(">", "＞");
            text = text.Replace("'", "‘");
            return text;
        }
        public string MetaDataXML
        {
            get
            {
                //string metaDataXml = null;                
                metaDataXml = null;
                if (ckbGetTitle.Checked)
                {
                    this.txtTitle.Value = InputTitle;
                }
                if (ckbGetSubject.Checked)
                {
                    this.txtSubject.Value = InputSubject;
                }

                metaDataXml = "<MetaData>" +
                            "<Title>" + InputText(this.txtTitle.Value) + "</Title>" +
                            "<Subject>" + InputText(this.txtSubject.Value) + "</Subject>" +
                            "<Creator>" + InputText(this.txtCreater.Value) + "</Creator>" +
                            "<Publisher>" + InputText(this.txtPublisher.Value) + "</Publisher>" +
                            "<Date>" + InputText(WucDate.CNDate/*this.txtCreateDate.Value*/) + "</Date>" +
                            "<Type>" + this.ddlType.SelectedValue + "</Type>" +
                            "<Identifier>" + InputText(this.txtIdentifier.Value) + "</Identifier>" +
                            //"<Theme>" +metatheme+ /*+ this.ddlTheme.SelectedValue +*/ "</Theme>" +
                            //"<Cake>" + metacake/*this.ddlCake.SelectedValue*/ + "</Cake>" +
                            //"<Service>" + metaservice/*this.ddlService.SelectedValue*/ + "</Service>" +
                            "<Theme>" + InputText(txtTheme.Text) + "</Theme>" +
                            "<Cake>" + InputText(txtCake.Text) + "</Cake>" +
                            "<Service>" + InputText(txtService.Text) + "</Service>" +
                            "<Description>" + InputText(this.txtDesc.Value) + "</Description>" +
                            "<Contributor>" + InputText(this.txtContributor.Value) + "</Contributor>" +
                            "<Format>" + InputText(this.txtFormat.Value) + "</Format>" +
                            "<Relation>" + InputText(this.txtRelation.Value) + "</Relation>" +
                            "<Source>" + InputText(this.txtSource.Value) + "</Source>" +
                            "<Language>" + this.ddlLanguage.SelectedValue + "</Language>" +
                            "<Coverage>" + InputText(this.txtCoverage.Value) + "</Coverage>" +
                            "<Rights>" + InputText(this.txtRight.Value) + "</Rights>" +
                            "<KeyWord>" + InputText(this.txtKeyWord.Value) + "</KeyWord>" +
                            "</MetaData>";
                return metaDataXml;
            }
            set
            {
                if (value != null)
                {
                    MetaData clsCMetaData = new MetaData();
                    MetaData metaData = clsCMetaData.GetMetaData(value);//解析XML後把XML的值帶入欄位
                    if (metaData != null)
                    {
                        this.txtTitle.Value = metaData.Title;
                        if (string.IsNullOrEmpty(metaData.Title))
                        {
                            if (System.IO.Path.GetFileName(Request.PhysicalPath) == "ContentManagment"/*path == "/WebMgt/ContentManagment"*/)
                            {
                                this.txtTitle.Value = "新選單";
                            }
                            if (System.IO.Path.GetFileName(Request.PhysicalPath) == "EditHomeManagment"/*path == "/WebMgt/EditHomeManagment"*/)
                            {
                                this.txtTitle.Value = "最新消息";
                            }
                        }

                        this.txtSubject.Value = metaData.Subject;
                        if (string.IsNullOrEmpty(metaData.Subject))
                        {
                            if (System.IO.Path.GetFileName(Request.PhysicalPath) == "ContentManagment"/*path == "/WebMgt/ContentManagment"*/)
                            {                                
                                this.txtSubject.Value = "新選單";
                            }
                            if (System.IO.Path.GetFileName(Request.PhysicalPath) == "EditHomeManagment"/*path == "/WebMgt/EditHomeManagment"*/)
                            {
                                this.txtSubject.Value = "最新消息";
                            }
                        }


                        this.txtCreater.Value = metaData.Creator;
                        this.txtPublisher.Value = metaData.Publisher;
                        //this.txtCreateDate.Value = metaData.Date;
                        this.WucDate.CNDate = metaData.Date;
                        ListItem li = this.ddlType.Items.FindByValue(metaData.Type);
                        if (li != null)
                            li.Selected = true;
                        this.txtIdentifier.Value = metaData.Identifier;
                        this.txtDesc.Value = metaData.Description;
                        this.txtContributor.Value = metaData.Contributor;
                        this.txtFormat.Value = metaData.Format;
                        this.txtRelation.Value = metaData.Relation;
                        this.txtSource.Value = metaData.Source;
                        this.txtCoverage.Value = metaData.Coverage;
                        li = this.ddlLanguage.Items.FindByValue(metaData.Language);
                        if (li != null)
                            li.Selected = true;
                        this.txtRight.Value = metaData.Rights;

                        this.txtTheme.Text = metaData.Theme;
                        this.lblThemeText.Text = metaTran("Theme", metaData.Theme);

                        this.txtCake.Text = metaData.Cake;
                        this.lblCakeText.Text = metaTran("Cake", metaData.Cake);

                        this.txtService.Text = metaData.Service;
                        this.lblServiceText.Text = metaTran("Service", metaData.Service);

                        this.txtKeyWord.Value = metaData.KeyWord;
                    }
                }
                else
                {
                    if (System.IO.Path.GetFileName(Request.PhysicalPath) == "ContentManagment"/*path == "/WebMgt/ContentManagment"*/)
                    {
                        this.txtTitle.Value = "新選單";
                        this.txtSubject.Value = "新選單";
                    }
                    if (System.IO.Path.GetFileName(Request.PhysicalPath) == "EditHomeManagment"/*path == "/WebMgt/EditHomeManagment"*/)
                    {
                        this.txtTitle.Value = "最新消息";
                        this.txtSubject.Value = "最新消息";
                    }
                }
            }
        }

        private string metaTran(string strMetaTag, string strMetaContent)
        {
            AFRCDutyEntities db = new AFRCDutyEntities();

            string[] aryMetaContent = strMetaContent.Split(',');

            string rtValue = "";

            try
            {
                for (int i = 0; i < aryMetaContent.Length; i++)
                {
                    string strID = aryMetaContent[i].ToString();

                    var str = (from C in db.T_Classifysearch
                               where C.classify_search_code == strMetaTag && C.id == strID
                               select C).FirstOrDefault();

                    if (str != null)
                    {
                        rtValue += str.caption + "(" + str.id + ")" + ",";
                    }

                }
                db.Dispose();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Dispose();
            }

            return rtValue;
        }

        public void TitleAutoTypeIn(string strInput)
        {
            txtTitle.Value = strInput;
            txtSubject.Value = strInput;
        }

        private void BindMetaTheme()
        {
            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                var Tlist = (from T in db.T_Classifysearch
                             where T.classify_search_code == "Theme" && T.belong_sn == 42
                             select new { id = T.id, caption = T.caption.ToString() + "(" + T.id.ToString() + ")", sort = T.sort }).OrderBy(o => o.sort).ToList();
                //select T).ToList();
                if (Tlist != null)
                {
                    ddlMetaTheme.DataSource = Tlist;
                    ddlMetaTheme.DataValueField = "id";
                    ddlMetaTheme.DataTextField = "caption";
                    ddlMetaTheme.DataBind();

                    for (int i = 0; i < ddlMetaTheme.Items.Count; i++)
                    {
                        if (ddlMetaTheme.Items[i].Value == "240")
                        {
                            ddlMetaTheme.Items[i].Selected = true;
                        }
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                db.Dispose();
            }
            finally
            {
                db.Dispose();
            }
        }

        private void BindMetaCake()
        {
            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                var Tlist = (from C in db.T_Classifysearch
                             where C.classify_search_code == "Cake" && C.belong_sn == 0
                             select new { id = C.id, caption = C.caption.ToString() + "(" + C.id.ToString() + ")", sort = C.sort }).OrderBy(o => o.sort).ToList();
                if (Tlist != null)
                {
                    ddlMetaCake.DataSource = Tlist;
                    ddlMetaCake.DataValueField = "id";
                    ddlMetaCake.DataTextField = "caption";
                    ddlMetaCake.DataBind();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                db.Dispose();
            }
            finally
            {
                db.Dispose();
            }
        }

        private void BindMetaService()
        {
            AFRCDutyEntities db = new AFRCDutyEntities();
            try
            {
                var Tlist = (from S in db.T_Classifysearch
                             where S.classify_search_code == "Service" && S.belong_sn == 0
                             select new { id = S.id, caption = S.caption.ToString() + "(" + S.id.ToString() + ")", sort = S.sort }).OrderBy(o => o.sort).ToList();
                if (Tlist != null)
                {
                    ddlMetaService.DataSource = Tlist;
                    ddlMetaService.DataValueField = "id";
                    ddlMetaService.DataTextField = "caption";
                    ddlMetaService.DataBind();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                db.Dispose();
            }
            finally
            {
                db.Dispose();
            }
        }

        protected void btnSearchTheme_Click(object sender, EventArgs e)
        {

        }
    }
}