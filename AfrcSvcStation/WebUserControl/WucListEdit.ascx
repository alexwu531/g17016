﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WucListEdit.ascx.cs" Inherits="AfrcSvcStation.WebUserControl.WucListEdit" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:UpdatePanel ID="up1" runat="server">
    <ContentTemplate>
        <div class="dataTables_wrapper form-inline no-footer">

            <div class="row">
                <div class="pull-right">
                    <asp:Button ID="cmdadd" runat="server" Text="新增" CssClass="btn btn-info btn-sm" OnClick="cmdadd_Click" />
                </div>
            </div>

            <table id="simple-table" class="table  table-bordered table-hover">

                <thead>
                    <tr>
                        <th align="center">名稱</th>
                         <th align="center">選單號碼</th>
                        <th align="center">更新時間</th>
                        <th align="center">刊登</th>
                        <th align="center">維護</th>
                        <th align="center">刪除</th>
                        <th align="center">排序</th>
                    </tr>
                </thead>

                <tbody>
                    <asp:Repeater ID="repListView" runat="server" OnItemCommand="repListView_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td><%#Eval("Title") %></td>
                                   <td><%#Eval("ListID") %></td>
                                <td><%#Eval("EditDate")%></td>
                                <td align="center">
                                    <i class='ace-icon fa fa-circle <%#Eval("Publish").ToString() != "2" ? "green":"red"%>'></i>
                                </t>

                                <td class="center">
                                    <asp:Button ID="btnEdit" runat="server" Text="編輯" CssClass="btn btn-xs btn-info" CausesValidation="False" CommandName="Edit" CommandArgument='<%#Eval("ListID")%>' OnCommand="btnEdit_Command" /></td>
                                <td class="center">
                                    <asp:Button ID="btnDel" runat="server" Text="刪除" CausesValidation="False" CommandName="Del" CssClass="btn btn-xs btn-danger" CommandArgument='<%#Eval("ListID")%>' OnCommand="btnDel_Command" /></td>
                                <td>

                                </td>
                            </tr>
                        </ItemTemplate>

                    </asp:Repeater>
                </tbody>
            </table>
            <div class="row">

                <div class="col-xs-12">
                    <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Width="100%" UrlPaging="false" CssClass="pagination" NumericButtonCount="5" LayoutType="Div" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" PageSize="5" OnPageChanged="AspNetPager1_PageChanged"
                            ShowCustomInfoSection="Left" CustomInfoSectionWidth="22%" CustomInfoTextAlign="Left" CustomInfoHTML="第<font color='red'><b>%currentPageIndex%</b></font>頁，共%PageCount%頁，每頁%PageSize%筆">
                        </webdiyer:AspNetPager>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
