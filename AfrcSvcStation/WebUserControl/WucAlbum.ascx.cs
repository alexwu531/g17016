﻿using AfrcSvcStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.WebUserControl
{
    public partial class WucAlbum : System.Web.UI.UserControl
    {
        protected UserInfo so = new UserInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindData();
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

        }

        protected void cmdcel_ServerClick(object sender, EventArgs e)
        {

        }

        protected void save_ServerClick(object sender, EventArgs e)
        {
            AFRCEntities db = new AFRCEntities();
            try
            {
                so = (UserInfo)Session["UserData"];

                for (int i = 0; i < Repeater1.Items.Count; i++)
                {
                    TextBox txtsort = (TextBox)Repeater1.Items[i].FindControl("txtsort");
                    HiddenField HidID = (HiddenField)Repeater1.Items[i].FindControl("HidID");
                    int _HidID = int.Parse(HidID.Value.ToString());
                    TextBox txtFileInfo = (TextBox)Repeater1.Items[i].FindControl("txtFileInfo");

                    T_Album T_Album = db.T_Album.SingleOrDefault(g => g.FileID == _HidID);
                    if (T_Album != null)
                    {
                        T_Album.Sort = Convert.ToInt32(txtsort.Text);
                        T_Album.FileInfo = txtFileInfo.Text;
                        T_Album.ModifyUser = int.Parse(so.UserPKID);
                        T_Album.ModifyTime = DateTime.UtcNow.AddHours(8);
                        db.SaveChanges();
                    }
                }

              
                BindData();
            }
            catch (Exception ex)
            {
              
            }
            finally
            {
                db.Dispose();
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BindData();
        }

        public void BindData()
        {
            AFRCEntities db = new AFRCEntities();
            try
            {

                int webid = (int)Session["WEBID"];

                var Files = from g in db.T_Album
                            where g.WebID == webid && g.Del == 1
                            orderby g.Sort
                            select g;

                Repeater1.DataSource = Files.ToList();
                Repeater1.DataBind();

                ScriptManager.RegisterStartupScript(this, GetType(), "spinner", @"Initspinner();", true);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Dispose();
            }
        }
    }
}