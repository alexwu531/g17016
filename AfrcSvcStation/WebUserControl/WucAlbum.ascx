﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WucAlbum.ascx.cs" Inherits="AfrcSvcStation.WebUserControl.WucAlbum" %>

<style>
    .fileUpload {
        position: relative;
        overflow: hidden;
        margin: 10px;
    }

        .fileUpload input.upload {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
</style>
<div class="form-horizontal" role="form">
    <div class="form-group">


        <div class="col-sm-12">
            <!-- PAGE CONTENT BEGINS -->



            <!--多檔選取-->
            <div class="fileUpload btn btn-primary" style="float: left">
                <span>批次上傳選取</span>
                <input type="file" id="attachfile" name="replyFiles" multiple="multiple" class="upload">
            </div>
            <div style="float: right">
                <%-- <a class="btn" href='<%=lastpage %>'>返回</a>--%>
                <input type="button" class="btn btn-danger" value="取消" runat="server" onserverclick="cmdcel_ServerClick" id="cmdcel" />
                <input type="button" class="btn btn-success" value="儲存" onclick="SaveFile();" />
            </div>


            <div id="filelist">
                <table border="1" width="100%">
                    <tbody>
                        <tr>
                            <th style="width: 20%; text-align: center">排序
                            </th>
                            <th style="text-align: center">圖片檔案
                            </th>
                            <th style="text-align: center">圖片說明
                            </th>
                            <th style="text-align: center">刪除
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br />
            <div>
                <table border="1" width="100%">
                    <tbody>
                        <tr>
                            <th style="width: 20%; text-align: center">排序
                            </th>
                            <th style="text-align: center">圖片檔案
                            </th>
                            <th style="text-align: center">圖片說明
                            </th>
                            <th style="text-align: center">刪除
                            </th>
                        </tr>
                        <asp:UpdatePanel ID="Uanel1" runat="server" ChildrenAsTriggers="true">
                            <ContentTemplate>

                                <asp:Repeater ID="Repeater1" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="HidID" runat="server" Value='<%#Eval("FileID") %>' />
                                                <asp:TextBox ID="txtsort" CssClass="txtint" runat="server" Text='<%#Eval("Sort") %>'></asp:TextBox>
                                            </td>
                                            <td>
                                            <%--    <a href="" class="btn-gray" target="_blank">--%>
                                                    <%#Eval("FileName") %><%--</a>--%>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFileInfo" TextMode="MultiLine" runat="server" Text='<%#Eval("FileInfo") %>' CssClass="span12" style="width:500px;"></asp:TextBox>
                                            </td>
                                            <td>
                                                <input type="button" class="btn btn-danger" value="刪除" onclick='<%# "Delserverside(" +Eval("FileID") + " );" %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </tbody>
                </table>
            </div>
            <div style="float: right">
                <%--   <a class="btn" href='<%=lastpage %>'>返回</a>--%>
                <input type="button" class="btn btn-danger" value="取消" runat="server" id="cmdcel2" onserverclick="cmdcel_ServerClick" />
                <input type="button" class="btn btn-success" value="儲存" runat="server" id="save" onserverclick="save_ServerClick" />
            </div>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
            </div>
            <!--Script-->
            <script>
                            var selDiv = "";
                            var storedFiles = []; //store the object of the all files
                            var storedFilesCnt = 0;
                            document.addEventListener("DOMContentLoaded", init, false);

                            function init() {
                                //To add the change listener on over file element
                                document.querySelector('#attachfile').addEventListener('change', handleFileSelect, false);
                                //allocate division where you want to print file name
                                selDiv = $("#filelist");
                            }

                            //function to handle the file select listenere
                            function handleFileSelect(e) {
                                $('#filelist').find('table').show();
                                //to check that even single file is selected or not
                                if (!e.target.files) return;

                                //for clear the value of the selDiv
                                selDiv.innerHTML = "";

                                //get the array of file object in files variable
                                var files = e.target.files;
                                var filesArr = Array.prototype.slice.call(files);



                                filesArr.forEach(function (f) {
                                    //add new selected files into the array list
                                    storedFiles.push(f);
                                    //print new selected files into the given division

                                });

                                //print if any file is selected previosly
                                for (var i = (storedFilesCnt + 1) ; i < (filesArr.length + storedFilesCnt + 1) ; i++) {

                                    $("#filelist").find("table").append(' <tr><input type="hidden" value="' + (i - 1) + '" /><td style="text-align:center"><input id="sort' + (i - 1) + '" name="sort" type="text" class="sort span11" value="' + i + '" /> </td> <td>  <input type="text" class="span9" disabled="disabled" id="fileinfo' + (i - 1) + '"  name="fileinfo"  value="' + storedFiles[i - 1].name + '"    /> <div class="fileUpload btn btn-primary"><input class="upload" type="file" class="span10" onchange="RowfileOnChange(this);" /> <span>瀏覽</span></div></td> <td> <textarea class="span11"    name="fileDes" id="fileDes' + (i - 1) + '"    >' + storedFiles[i - 1].name + '</textarea> </td><td  style="text-align: center"><input type="button" class="btn btn-danger" value="刪除" onclick="Del(this);"   /></td></tr>');
                                    Initspinner();
                                }

                                storedFilesCnt = storedFiles.length;

                                //store the array of file in our element this is send to other page by form submit
                                $("#attachfile").val('');

                            }
                            //個別修改路徑
                            function RowfileOnChange(obj) {
                                $(obj).parent().parent().parent().find('input[name = "fileinfo"]').val(obj.files[0].name);
                                $(obj).parent().parent().parent().find('input[name = "fileDes"]').val(obj.files[0].name);
                                var Objindex = parseInt($(obj).parent().parent().parent().find('input[type="hidden"]').val());
                                storedFiles[Objindex] = obj.files[0];


                            }
                            $(function () {

                            });
                            //數字選擇器
                            function Initspinner() {

                                $('.sort').spinner({
                                    min: 0,
                                    max: 1000000,
                                    create: function( event, ui ) {
						//add custom classes and icons
						$(this)
						.next().addClass('btn btn-success').html('<i class="ace-icon fa fa-plus"></i>')
						.next().addClass('btn btn-danger').html('<i class="ace-icon fa fa-minus"></i>')
						
						//larger buttons on touch devices
						if('touchstart' in document.documentElement) 
							$(this).closest('.ui-spinner').addClass('ui-spinner-touch');
					}
                                });
                                $('.sort').unbind("keypress");
                                $(".sort").bind("keydown", function (event) {
                                    event.preventDefault();
                                });
                                $(".txtint").spinner({
                                    min: 0,
                                    max: 1000000
                                });
                                $(".txtint").unbind("keypress");


                                $(".txtint").bind("keydown", function (event) {
                                    event.preventDefault();
                                });
                            }

                            function SaveFile() {
                                var data = new FormData();
                                for (var i = 0; i < storedFiles.length; i++) {
                                    data.append('file-' + i, storedFiles[i]);
                                    $("#filelist").find("table").find("tr").find('input[type="hidden"]').each(function () {
                                        var obj = $(this);
                                        if (obj.val() == i) {
                                            var sortObj = $(this).parent().find('input[name="sort"]');
                                            data.append('sort-' + i, sortObj.val());
                                            var fileDesObj = $(this).parent().find('textarea[name="fileDes"]');
                                            data.append('fileDes-' + i, fileDesObj.val());

                                        }
                                    })

                                }
                                $.ajax({
         url: 'Bulkfileupload.ashx',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (res) {
                alert(res);
           document.getElementById("<%=Button1.ClientID %>").click();
            },
            error: function (res) {
                alert('上傳失敗');
            }
        });
    }

    function Del(Obj) {
        var Objindex = parseInt($(Obj).parent().parent().find('input[type="hidden"]').val());
        storedFiles.splice(Objindex, 1);
        $("#filelist").find("table").find("tr").find('input[type="hidden"]').each(function () {
            var hidindex = parseInt($(this).val());
            if (hidindex > Objindex) {
                $(this).val(hidindex - 1);
            }
        })

        $(Obj).parent().parent().remove();
    }
    //刪除資料庫檔案
    function Delserverside(Obj) {
        $.ajax({
            type: "POST",
            url: "FileDelService.ashx?fileID="+Obj+"&Isalbum=1",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: "true",
            cache: "false",

            success: function (msg) {
                document.getElementById("<%=Button1.ClientID %>").click();
                alert(msg.MSG);

            },
            Error: function (x, e) {

                document.getElementById("<%=Button1.ClientID %>").click();
            }
        });

        }


        $(document).ready(function () {
            $('#filelist').find('table').hide();
        });
            </script>


            <!-- PAGE CONTENT ENDS -->
        </div>
    </div>

</div>
