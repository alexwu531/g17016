﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControls_WucDate : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public string GetDate()
    {

        return txtDate.Text;
    }
    public string StrDate
    {
        get
        { return this.txtDate.Text.ToString(); }
        set
        { this.txtDate.Text = value; }
    }
}