﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AfrcSvcStation.Default" %>

<%@ Register Src="~/WebUserControl/WucLeftMenu.ascx" TagPrefix="uc1" TagName="WucLeftMenu" %>
<%@ Register Src="~/WebUserControl/WucDate.ascx" TagPrefix="uc1" TagName="WucDate" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- css ---------------------------------------------------------------->
    <%--:Styles.Render("~/bundles/loginstyles") --%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
        <!-- PAGE CONTENT ENDS -->
    </div>
    <!-- /.col -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="InitJS" runat="server">
    <!------------------ BundleJavascript ------------------>
    <%--: Scripts.Render("~/bundles/loginjs") --%>
</asp:Content>
