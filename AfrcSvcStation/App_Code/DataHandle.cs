﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace AfrcSvcStation.App_Code
{
    public class DataHandle
    {
        /// <summary>
        /// Description：普通下拉列表綁定縣市數據
        ///Input：綁定之下拉列表ID
        ///Output：地區
        ///Programmer：Bunny
        /// </summary>
        public DropDownList BindCounty(DropDownList dropDownList, String selectedValue)
        {
            //ProDataClassesDataContext db = new ProDataClassesDataContext();
            //var countys = (from cods in db.T_SysCodd
            //               where (cods.Hidden == "0") && (cods.CodeType == "County")
            //               orderby cods.Sort
            //               select cods).ToList();

            //dropDownList.DataTextField = "CodeName";
            //dropDownList.DataValueField = "CodeName";
            //dropDownList.DataSource = countys;
            //dropDownList.DataBind();
            //dropDownList.Items.Insert(0, new ListItem("請選擇", ""));
            //dropDownList.SelectedValue = selectedValue;
             return dropDownList;
        }
    }
}