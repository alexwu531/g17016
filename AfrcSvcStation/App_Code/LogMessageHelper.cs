﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AfrcSvcStation.App_Code
{
    /// <summary>
    /// Log稽核紀錄文字訊息功能
    /// </summary>
    static class LogMessageHelper
    {
        /// <summary>
        /// 取得LOG字串輸出格式化
        /// </summary>
        /// <param name="template">1: </param>
        /// <returns></returns>
        public static string GetMessageText(int template, string who, string evt, string path)
        {
            string rt = string.Empty;
            try
            {
                switch (template)
                {
                    case 1:
                        rt += "操作人員:" + who + ";";
                        rt += "事件:" + evt + ";";
                        rt += "路徑:" + path + ";";
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                }

            }
            catch (Exception ex)
            {

            }

            return rt.Length > 0 ? rt.Substring(0, rt.Length - 1) : "";
        }
    }
}
