﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AfrcSvcStation.App_Code
{
    public class SettingHelper
    {
        /// <summary>
        /// 取的Webconfig app設定
        /// </summary>
        /// <param name="key">add key</param>
        /// <returns></returns>
        public static string GetAppSetting(string key)
        {
            string strValue = "";

            if (!string.IsNullOrEmpty(key))
            {
                strValue = System.Configuration.ConfigurationManager.AppSettings[key].ToString();
            }

            return strValue;
        }

    }
}