﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace AfrcSvcStation.App_Code
{
    public class WebMessage
    {
        /// <summary>
        /// 前端顯示訊息並轉頁
        /// </summary>
        /// <param name="values">訊息字串</param>
        /// <param name="PageURL">URL</param>
        public void WebMessageBox(string values, string PageURL)
        {
            HttpContext.Current.Response.Write("<script>alert('" + values + "');window.location.href='" + PageURL + "'</script>");
            HttpContext.Current.Response.End();
        }




        /// <summary>
        /// 訊息視窗
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="CallPage"></param>
        public void MsgBox(string Message, Page CallPage)
        {
            string _Script = string.Empty;
            string _Message = string.Empty;
            _Message = Message.Replace("'", "\'");
            _Script = string.Format("alert('{0}')", _Message);
            //ScriptManager.RegisterStartupScript(CallPage, this.GetType(), "alert", _Script, true);
            CallPage.ClientScript.RegisterClientScriptBlock(this.GetType(), "checkinput", @"<script language='javascript'>" + _Script + "</script>");
        }



        /// <summary>
        /// 用於UpdatePanel內
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="CallPage"></param>
        public void MsgBox_In_Ajax(string Message, Page CallPage)
        {
            ScriptManager.RegisterStartupScript(CallPage, CallPage.GetType(), "Info", "alert('" + Message + "');", true);

        }
        /// <summary>
        /// 用於UpdatePanel內
        /// 訊息視窗(並跳轉網頁)
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="CallPage"></param>
        public void MsgBox_In_Ajax(string Message, Page CallPage, string Url)
        {
            string _Script = string.Empty;
            string _Message = string.Empty;
            _Message = Message.Replace("'", "\'");
            _Script = string.Format("alert('{0}');location.href='{1}';", _Message, Url);
            ScriptManager.RegisterStartupScript(CallPage, this.GetType(), "alert", _Script, true);
        }
    }
}