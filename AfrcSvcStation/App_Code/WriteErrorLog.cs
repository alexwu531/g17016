﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace AfrcSvcStation.App_Code
{
    public class WriteErrorLog
    {
        public WriteErrorLog() { }

        public void LogWriter(Exception exc)
        {

            string source = "";
            string logFile = ConfigurationManager.AppSettings.Get("ErrorLogPath").ToString() + DateTime.Now.ToString("yyyyMMdd") + "GlobalLog.txt";
            System.IO.StreamWriter sw = new System.IO.StreamWriter(logFile, true);
            sw.Write("******************** " + DateTime.Now);
            sw.WriteLine(" ********************");
            if (exc.InnerException != null)
            {
                sw.Write("Inner Exception Type: ");
                sw.WriteLine(exc.InnerException.GetType().ToString());
                sw.Write("Inner Exception: ");
                sw.WriteLine(exc.InnerException.Message);
                sw.Write("Inner Source: ");
                sw.WriteLine(exc.InnerException.Source);
                if (exc.InnerException.StackTrace != null)
                    sw.WriteLine("Inner Stack Trace: ");
                sw.WriteLine(exc.InnerException.StackTrace);
            }
            sw.Write("Exception Type: ");
            sw.WriteLine(exc.GetType().ToString());
            sw.WriteLine("Exception: " + exc.Message);
            sw.WriteLine("Source: " + source);
            sw.WriteLine("Stack Trace: ");
            if (exc.StackTrace != null)
                sw.WriteLine(exc.StackTrace);
            sw.WriteLine();
            sw.Close();

        }
    }
}