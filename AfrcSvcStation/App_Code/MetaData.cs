﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace AfrcSvcStation.App_Code
{
    public class MetaData
    {
        private string _Cake;
        private string _Contributor;
        private string _Coverage;
        private string _Creator;
        private string _CurPage;
        private string _Date;
        private string _Description;
        private string _Format;
        private string _Identifier;
        private string _Language;
        private string _Publisher;
        private string _Relation;
        private string _Rights;
        private string _Service;
        private string _Source;
        private string _Subject;
        private string _Theme;
        private string _Title;
        private string _Type;
        private string _KeyWord;


        public MetaData GetMetaData(string metaDataXml)
        {
            MetaData data = new MetaData();
            if (!string.IsNullOrEmpty(metaDataXml.Trim()))
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(metaDataXml);
                string xpath = "//MetaData/Cake";
                XmlNode node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Cake = node.InnerText;
                }
                xpath = "//MetaData/Contributor";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Contributor = node.InnerText;
                }
                xpath = "//MetaData/Coverage";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Coverage = node.InnerText;
                }
                xpath = "//MetaData/Creator";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Creator = node.InnerText;
                }
                xpath = "//MetaData/Date";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Date = node.InnerText;
                }
                xpath = "//MetaData/Description";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Description = node.InnerText;
                }
                xpath = "//MetaData/Identifier";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Identifier = node.InnerText;
                }
                xpath = "//MetaData/Format";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Format = node.InnerText;
                }
                xpath = "//MetaData/Language";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Language = node.InnerText;
                }
                xpath = "//MetaData/Publisher";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Publisher = node.InnerText;
                }
                xpath = "//MetaData/Relation";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Relation = node.InnerText;
                }
                xpath = "//MetaData/Rights";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Rights = node.InnerText;
                }
                xpath = "//MetaData/Service";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Service = node.InnerText;
                }
                xpath = "//MetaData/Source";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Source = node.InnerText;
                }
                xpath = "//MetaData/Subject";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Subject = node.InnerText;
                }
                xpath = "//MetaData/Theme";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Theme = node.InnerText;
                }
                xpath = "//MetaData/Title";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Title = node.InnerText;
                }
                xpath = "//MetaData/Type";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.Type = node.InnerText;
                }
                xpath = "//MetaData/KeyWord";
                node = document.SelectSingleNode(xpath);
                if (node != null)
                {
                    data.KeyWord = node.InnerText;
                }
            }
            return data;
        }
        /// <summary>
        /// Cake-施政分類
        /// </summary>
        public string Cake
        {
            get
            {
                return this._Cake;
            }
            set
            {
                this._Cake = value;
            }
        }

        /// <summary>
        /// Contributor-貢獻者
        /// </summary>
        public string Contributor
        {
            get
            {
                return this._Contributor;
            }
            set
            {
                this._Contributor = value;
            }
        }

        /// <summary>
        /// Coverage-時空涵蓋範圍
        /// </summary>
        public string Coverage
        {
            get
            {
                return this._Coverage;
            }
            set
            {
                this._Coverage = value;
            }
        }

        /// <summary>
        /// Creator-創作者
        /// </summary>
        public string Creator
        {
            get
            {
                return this._Creator;
            }
            set
            {
                this._Creator = value;
            }
        }

        /// <summary>
        /// 當前頁碼
        /// </summary>
        public string CurPage
        {
            get
            {
                return this._CurPage;
            }
            set
            {
                this._CurPage = value;
            }
        }

        /// <summary>
        /// Date-製作日期
        /// </summary>
        public string Date
        {
            get
            {
                return this._Date;
            }
            set
            {
                this._Date = value;
            }
        }

        /// <summary>
        /// Description-簡述
        /// </summary>
        public string Description
        {
            get
            {
                return this._Description;
            }
            set
            {
                this._Description = value;
            }
        }

        /// <summary>
        /// Format-資料格式
        /// </summary>
        public string Format
        {
            get
            {
                return this._Format;
            }
            set
            {
                this._Format = value;
            }
        }

        /// <summary>
        /// 識別資料
        /// </summary>
        public string Identifier
        {
            get
            {
                return this._Identifier;
            }
            set
            {
                this._Identifier = value;
            }
        }

        /// <summary>
        /// Language-語言
        /// </summary>
        public string Language
        {
            get
            {
                return this._Language;
            }
            set
            {
                this._Language = value;
            }
        }

        /// <summary>
        /// Publisher-出版者
        /// </summary>
        public string Publisher
        {
            get
            {
                return this._Publisher;
            }
            set
            {
                this._Publisher = value;
            }
        }

        /// <summary>
        /// Relation-關連
        /// </summary>
        public string Relation
        {
            get
            {
                return this._Relation;
            }
            set
            {
                this._Relation = value;
            }
        }

        /// <summary>
        /// Rights-權限範圍
        /// </summary>
        public string Rights
        {
            get
            {
                return this._Rights;
            }
            set
            {
                this._Rights = value;
            }
        }

        /// <summary>
        /// Service-服務分類
        /// </summary>
        public string Service
        {
            get
            {
                return this._Service;
            }
            set
            {
                this._Service = value;
            }
        }

        /// <summary>
        /// Source-來源
        /// </summary>
        public string Source
        {
            get
            {
                return this._Source;
            }
            set
            {
                this._Source = value;
            }
        }

        /// <summary>
        /// Subject-主題或關鍵詞
        /// </summary>
        public string Subject
        {
            get
            {
                return this._Subject;
            }
            set
            {
                this._Subject = value;
            }
        }

        /// <summary>
        /// Theme-主題分類
        /// </summary>
        public string Theme
        {
            get
            {
                return this._Theme;
            }
            set
            {
                this._Theme = value;
            }
        }

        /// <summary>
        /// Title-標題
        /// </summary>
        public string Title
        {
            get
            {
                return this._Title;
            }
            set
            {
                this._Title = value;
            }
        }

        /// <summary>
        /// Type-資料類型
        /// </summary>
        public string Type
        {
            get
            {
                return this._Type;
            }
            set
            {
                this._Type = value;
            }
        }

        /// <summary>
        /// KeyWord-關鍵字
        /// </summary>
        public string KeyWord
        {
            get
            {
                return this._KeyWord;
            }
            set
            {
                this._KeyWord = value;
            }
        }

        public MetaData()
        {
            //
            // TODO: 在此加入建構函式的程式碼
            //
        }
    }
}