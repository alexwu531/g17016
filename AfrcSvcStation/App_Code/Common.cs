﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

using AfrcSvcStation.ModelsForStation;

namespace AfrcSvcStation.App_Code
{
    public class Common
    {

        public Common() { }
        protected UserInfo so = new UserInfo();

        /// <summary>
        /// 取的Webconfig app設定
        /// </summary>
        /// <param name="key">add key</param>
        /// <returns></returns>
        public static string GetAppSetting(string key)
        {
            string strValue = "";

            if (!string.IsNullOrEmpty(key))
            {
                strValue = System.Configuration.ConfigurationManager.AppSettings[key].ToString();
            }

            return strValue;
        }

        /// <summary>
        /// 解碼url字串
        /// </summary>
        public static string DecodeAllPara(object para)
        {
            string rt = string.Empty;
            if (para != null)
            {

                rt = HttpUtility.UrlDecode(para.ToString());
            }
            else
            {
                rt = "";
            }
            return rt;
        }

        private static string KEY = "12121212";//密Key 
        private static string IV = "21212121";//密IV 

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="encryptString">來源字串</param>
        /// <returns>加密後字串</returns>
        public static string Encrypt(string original)
        {
            try
            {
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                des.Key = Encoding.ASCII.GetBytes(KEY);
                des.IV = Encoding.ASCII.GetBytes(IV);
                byte[] s = Encoding.ASCII.GetBytes(original);
                ICryptoTransform desencrypt = des.CreateEncryptor();
                return BitConverter.ToString(desencrypt.TransformFinalBlock(s, 0, s.Length)).Replace("-", string.Empty);
            }
            catch { return original; }
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="decryptString">來源字串</param>
        /// <returns>解密後字串</returns>
        public static string Decrypt(string hexString)
        {
            try
            {
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                des.Key = Encoding.ASCII.GetBytes(KEY);
                des.IV = Encoding.ASCII.GetBytes(IV);

                byte[] s = new byte[hexString.Length / 2];
                int j = 0;
                for (int i = 0; i < hexString.Length / 2; i++)
                {
                    s[i] = Byte.Parse(hexString[j].ToString() + hexString[j + 1].ToString(), System.Globalization.NumberStyles.HexNumber);
                    j += 2;
                }
                ICryptoTransform desencrypt = des.CreateDecryptor();
                return Encoding.ASCII.GetString(desencrypt.TransformFinalBlock(s, 0, s.Length));
            }
            catch { hexString = ""; return hexString; }
        }

        //取得目前時間
        public  DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }

        //取得使用者名稱
        public string GetUserName(string UserID)
        {
            string rtValue = "";
            AFRCDutyEntities db = new AFRCDutyEntities();
            
            try
            {
                var UserInfo = (from U in db.T_SysUser
                                where U.UserID == UserID
                                select U).FirstOrDefault().UserName;

                if (UserInfo != null)
                {
                    rtValue = UserInfo;
                }
                db.Dispose();
            }
            catch (Exception ex)
            {
                rtValue = "";                    
            }
            finally
            {
                db.Dispose();
            }

            return rtValue;
        }
        /// <summary>
        /// 民國日期轉西元日期
        /// </summary>
        /// <param name="strChinaDate">民國日期 格式：年/月/日</param>
        /// <returns>西元日期 格式：年/月/日</returns>
        public String ChinesetoAD(string strChinaDate)
        {
            string[] ymd = strChinaDate.Split('/');
            if (ymd.Length != 3)
            {
                return "";
            }
            else
            {
                string ADYMD = (int.Parse(ymd[0]) + 1911).ToString() + "/" + ymd[1].PadLeft(2, '0') + "/" + ymd[2].PadLeft(2, '0');
                DateTime tmpDate = DateTime.Now;
                if (DateTime.TryParse(ADYMD, out tmpDate))
                {
                    DateTime DTAD = new DateTime();
                    DTAD = DateTime.Parse(ADYMD);
                    return DTAD.ToString("yyyyMMdd");
                }
                return "";
            }
        }
        /// <summary>
        /// 確認是否數值
        /// </summary>
        /// <param name="X">要確認的數值</param>
        /// <returns></returns>
        public bool chkvalue(string X)
        {
            int result;
            if (int.TryParse(X, out result))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// 字串轉換數值
        /// </summary>
        /// <param name="Str"></param>
        /// <returns></returns>
        public int TranStrToInt(string Str)
        {
            try
            {
                return int.Parse(Str);
            }
            catch
            {
                return 0;
            }
        }
        //取得現在時間
        public string GetNowTime()
        {
            return DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        }

    }
}