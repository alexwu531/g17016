﻿using AfrcSvcStation.ModelsForStation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AfrcSvcStation.App_Code
{
    /*
    public class FileUploadPathHelper
    {
        /// <summary>
        /// 取得主網站或縣市網站檔案上傳路徑
        /// </summary>
        /// <param name="webid">網站ID(T_Web.ID)</param>
        /// <returns></returns>
        public string GetUpoladPathByVer(string webid)
        {
            string rt = string.Empty;

            switch (webid)
            {
                case "1":
                    //主網
                    rt = Common.GetAppSetting("main_FilePath");
                    break;
                case "2":
                    //高雄市
                    rt = Common.GetAppSetting("KaohSiung_FilePath");
                    break;
                case "3":
                    //基隆市
                    rt = Common.GetAppSetting("KeeLung_FilePath");
                    break;
                case "4":
                    //宜蘭縣
                    rt = Common.GetAppSetting("Yilan_FilePath");
                    break;
                case "5":
                    //桃園市
                    rt = Common.GetAppSetting("Taoyuan_FilePath");
                    break;
                case "6":
                    //台北市
                    rt = Common.GetAppSetting("Taipei_FilePath");
                    break;
                case "7":
                    //新北市
                    rt = Common.GetAppSetting("TaipeiCity_FilePath");
                    break;
                case "8":
                    //新竹
                    rt = Common.GetAppSetting("Hsinchu_FilePath");
                    break;
                case "9":
                    //花蓮
                    rt = Common.GetAppSetting("HunLien_FilePath");
                    break;
                case "10":
                    //苗栗
                    rt = Common.GetAppSetting("Miaoli_FilePath");
                    break;
                case "11":
                    //台中市
                    rt = Common.GetAppSetting("TaichungCity_FilePath");
                    break;
                case "12":
                    //南投縣
                    rt = Common.GetAppSetting("Nantou_FilePath");
                    break;
                case "13":
                    //彰化縣
                    rt = Common.GetAppSetting("Changhua_FilePath");
                    break;
                case "14":
                    //雲林縣
                    rt = Common.GetAppSetting("Yunlin_FilePath");
                    break;
                case "15":
                    //嘉義
                    rt = Common.GetAppSetting("Chiayi_FilePath");
                    break;
                case "16":
                    //台南市
                    rt = Common.GetAppSetting("Tainancity_FilePath");
                    break;
                case "17":
                    //屏東
                    rt = Common.GetAppSetting("PingTung_FilePath");
                    break;
                case "18":
                    //台東縣
                    rt = Common.GetAppSetting("TaiTung_FilePath");
                    break;
                case "19":
                    //澎湖縣
                    rt = Common.GetAppSetting("Penghu_FilePath");
                    break;
                case "20":
                    //國民革命忠烈祠
                    rt = Common.GetAppSetting("faith_martyr_FilePath");
                    break;
                case "21":
                    //國軍示範公墓
                    rt = Common.GetAppSetting("Cemetery_FilePath");
                    break;
                case "22":
                    //忠烈英文
                    rt = Common.GetAppSetting("faith_martyr_en_FilePath");
                    break;
                case "23":
                    //忠烈日文
                    rt = Common.GetAppSetting("faith_martyr_jp_FilePath");
                    break;
                case "24":
                    //忠烈韓文
                    rt = Common.GetAppSetting("faith_martyr_ko_FilePath");
                    break;
                case "25":
                    //公墓英文
                    rt = Common.GetAppSetting("Cemetery_en_FilePath");
                    break;
                case "26":
                    //主網英文
                    rt = Common.GetAppSetting("main_FilePath_en");
                    break;
                case "27":
                    //桃管組
                    rt = Common.GetAppSetting("AFRCTaoyuanMgt");
                    break;
            }

            return rt;
        }

        /// <summary>
        /// 取得主網站或縣市網站HTTP DOMAIN
        /// </summary>
        /// <param name="webid"></param>
        /// <returns></returns>
        public string GetFilePathByVer(string webid)
        {
            string rt = string.Empty;
            AFRCEntities db = new AFRCEntities();
            
            try
            {
                int _webid = int.Parse(webid);
                var web = db.T_Web.Where(w=>w.WebID == _webid).FirstOrDefault();
                if(web != null)
                {
                    rt = web.WebUrl;
                }
          

            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Dispose();
            }


            return rt;
        }




        /// <summary>
        /// 取得上傳附件完整路徑，並判斷目錄是否存在並產生
        /// </summary>
        /// <param name="menuid"></param>
        /// <returns></returns>
        public string GetFullPathByMenuID(int menuid, string rootpath)
        {
            string rt = string.Empty;

            AFRCEntities db = new AFRCEntities();

            try
            {
                var MenuList = db.T_Menu.ToList();
                string s = "";
                rt = PathGenerator(menuid, MenuList, s, rootpath);

            }
            catch(Exception ex)
            {

            }
            finally
            {
                db.Dispose();
            }

            return rt;
        }

        private string PathGenerator(int menuid, List<T_Menu> Menulst, string path , string root)
        {
            string _path = "";

            var menu = Menulst.Where(m => m.MenuID == menuid).FirstOrDefault();

            int _ParentMenuID = menu.ParentMenuID.Value;

            _path = menu.MenuName;

            if (_ParentMenuID != 0)
            {
                path = _path + @"\" + path;
                path = PathGenerator(_ParentMenuID, Menulst, path, root);
            }
            else
            {
                path = _path + @"\" + path;
                //已到最上層

                //完整實體路徑
                string fullpath = root + path;

                if (!Directory.Exists(fullpath))
                {
                    //產生
                    Directory.CreateDirectory(fullpath);
                }

            }
            //返回相對路徑
            return path;
        }
    }
*/
}