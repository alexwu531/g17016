﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AfrcSvcStation.App_Code
{
    public static class Global
    {
        /// <summary>
        /// Global variable storing important stuff.
        /// </summary>
        static string _WebID;

        /// <summary>
        /// 取得WEBID
        /// Get or set the static important data.
        /// </summary>
        public static string WebID
        {
            get
            {
                if(_WebID ==null)
                {
                    _WebID = System.Configuration.ConfigurationManager.AppSettings["WebID"].ToString();
                }
                return _WebID;
            }
            set
            {
                _WebID = value;
            }
        }
    }
}